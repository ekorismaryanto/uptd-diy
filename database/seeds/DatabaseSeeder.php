<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(SeedDataKabupatenDiy::class);
        // $this->call(SeedDataKecamatan::class);
        $this->call(PermissionSeeder::class);
    }
}
