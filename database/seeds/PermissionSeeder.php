<?php

use App\Repositories\Entities\District;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::firstOrCreate(['name' => 'user-management.user']);
        Permission::firstOrCreate(['name' => 'user-management.role']);
        Permission::firstOrCreate(['name' => 'datamaster.komoditas']);
        Permission::firstOrCreate(['name' => 'datamaster.opt']);
        Permission::firstOrCreate(['name' => 'datamaster.news']);
        Permission::firstOrCreate(['name' => 'data.tanaman-pangan']);
        Permission::firstOrCreate(['name' => 'data.holtikultura']);
        Permission::firstOrCreate(['name' => 'data.perkebunan']);
        Permission::firstOrCreate(['name' => 'input-data']);
        Permission::firstOrCreate(['name' => 'input-data.edit']);
        Permission::firstOrCreate(['name' => 'input-data.delete']);
        Permission::firstOrCreate(['name' => 'overview']);
        Permission::firstOrCreate(['name' => 'statistik']);
        Permission::firstOrCreate(['name' => 'report']);
    }
}
