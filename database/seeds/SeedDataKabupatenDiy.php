<?php

use App\Repositories\Entities\District;
use Illuminate\Database\Seeder;

class SeedDataKabupatenDiy extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        District::firstOrCreate(['name' => 'KAB. KULON PROGO']);
        District::firstOrCreate(['name' => 'KAB. BANTUL']);
        District::firstOrCreate(['name' => 'KAB. GUNUNG KIDUL']);
        District::firstOrCreate(['name' => 'KAB. SLEMAN']);
        District::firstOrCreate(['name' => 'KOTA YOGYAKARTA']);
    }
}
