<?php

use App\Repositories\Entities\District;
use App\Repositories\Entities\SubDistrict;
use Illuminate\Database\Seeder;

class SeedDataKecamatan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Temon']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Wates']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Panjatan']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Galur']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Lendah']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Sentolo']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Pengasih']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Kokap']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Girimulyo']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Nanggulan']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Samigaluh']);
        SubDistrict::firstOrCreate(['district_id' => 1,'name' => 'Kalibawang']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Srandakan']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Sanden']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Kretek']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Pundong']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Bambang Lipuro']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Pandak']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Pajangan']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Bantul']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Jetis']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Imogiri']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Dlingo']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Banguntapan']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Pleret']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Piyungan']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Sewon']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Kasihan']);
        SubDistrict::firstOrCreate(['district_id' => 2,'name' => 'Sedayu']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Wonosari']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Nglipar']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Playen']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Patuk']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Paliyan']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Panggang']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Tepus']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Semanu']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Karangmojo']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Ponjong']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Rongkop']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Semin']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Ngawen']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Gedangsari']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Saptosari']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Girisubo']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Tanjungsari']);
        SubDistrict::firstOrCreate(['district_id' => 3,'name' => 'Purwosari']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Gamping']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Godean']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Moyudan']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Minggir']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Seyegan']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Mlati']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Berbah']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Depok']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Prambanan']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Kalasan']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Ngemplak']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Ngaglik']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Sleman']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Tempel']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Turi']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Pakem']);
        SubDistrict::firstOrCreate(['district_id' => 4,'name' => 'Cangkringan']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Tegalrejo']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Jetis']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Gondokusuman']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Danurejan']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Gedongtengen']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Ngampilan']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Wirobrajan']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Mantrijeron']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Kraton']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Gondomanan']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Pakualaman']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Mergangsan']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Umbulharjo']);
        SubDistrict::firstOrCreate(['district_id' => 5,'name' => 'Kotagede']);
    }
}
