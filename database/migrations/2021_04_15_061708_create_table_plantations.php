<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePlantations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_crops', function (Blueprint $table) {
            $table->id();
            $table->date('periode')->nullable();
            $table->integer('sub_district_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('village_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->string('varieties')->nullable();
            $table->string('age')->nullable();
            $table->integer('planting_area')->nullable();
            $table->integer('opt_id')->nullable();
            $table->integer('criteria_opt')->nullable();
            $table->date('month')->nullable();
            $table->string('periode_tb')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_crops');
    }
}
