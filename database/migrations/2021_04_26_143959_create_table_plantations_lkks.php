<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePlantationsLkks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_plantation_lkks', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->date('periode')->nullable();
            $table->integer('sub_district_id')->nullable();
            $table->integer('village_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->double('planting_area')->nullable();
            $table->string('age')->nullable();
            $table->string('age_unit')->nullable();
            $table->double('attact_area')->nullable();
            $table->string('deskripsi_opt')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_plantation_lkks');
    }
}
