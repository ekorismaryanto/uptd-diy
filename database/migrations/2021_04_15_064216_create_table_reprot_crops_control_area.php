<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReprotCropsControlArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_crops_control_area', function (Blueprint $table) {
            $table->id();
            $table->integer('report_crop_id')->nullable();
            $table->double('kimia')->nullable();
            $table->double('hayati')->nullable();
            $table->double('eradikasi')->nullable();
            $table->double('others')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_crops_control_area');
    }
}
