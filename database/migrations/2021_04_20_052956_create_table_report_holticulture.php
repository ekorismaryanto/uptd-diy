<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReportHolticulture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_holticultures', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->date('periode')->nullable();
            $table->integer('sub_district_id')->nullable();
            $table->integer('village_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->string('periode_tb')->nullable();
            $table->double('planting_area')->nullable();
            $table->double('attact_area_r')->nullable();
            $table->double('attact_area_s')->nullable();
            $table->double('attact_area_b')->nullable();
            $table->double('attact_area_p')->nullable();
            $table->double('attact_area_j')->nullable();
            $table->double('under_control')->nullable();
            $table->double('added_area_r')->nullable();
            $table->double('added_area_s')->nullable();
            $table->double('added_area_b')->nullable();
            $table->double('added_area_p')->nullable();
            $table->double('added_area_j')->nullable();
            $table->double('state_area_r')->nullable();
            $table->double('state_area_s')->nullable();
            $table->double('state_area_b')->nullable();
            $table->double('state_area_p')->nullable();
            $table->double('state_area_j')->nullable();
            $table->double('control_area_pem')->nullable();
            $table->double('control_area_pest')->nullable();
            $table->double('control_area_cl')->nullable();
            $table->double('control_area_aph')->nullable();
            $table->double('control_area_j')->nullable();
            $table->double('threatened_plant')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_holticultures');
    }
}
