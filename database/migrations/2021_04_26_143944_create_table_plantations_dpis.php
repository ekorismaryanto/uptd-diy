<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePlantationsDpis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_plantation_dpis', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->date('periode')->nullable();
            $table->integer('sub_district_id')->nullable();
            $table->integer('village_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->double('planting_area')->nullable();
            $table->string('age')->nullable();
            $table->string('age_unit')->nullable();
            $table->double('dry_exposed')->nullable();
            $table->double('dry_lightly')->nullable();
            $table->string('dry_handling')->nullable();
            $table->double('flood_hit')->nullable();
            $table->double('light_flood')->nullable();
            $table->string('flood_handling')->nullable();
            $table->double('fire_hit')->nullable();
            $table->double('light_fire')->nullable();
            $table->string('fire_handling')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_plantation_dpis');
    }
}
