<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('nip')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('eselon')->nullable();
            $table->string('type')->nullable();
            $table->date('tmt_type')->nullable();
            $table->string('position')->nullable();
            $table->string('work_location')->nullable();
            $table->integer('status_employee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
