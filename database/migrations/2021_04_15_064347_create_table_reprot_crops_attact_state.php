<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReprotCropsAttactState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_crops_attact_state', function (Blueprint $table) {
            $table->id();
            $table->integer('report_crop_id')->nullable();
            $table->double('ringan')->nullable();
            $table->double('sedang')->nullable();
            $table->double('berat')->nullable();
            $table->double('puso')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_crops_attact_state');
    }
}
