<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_reports', function (Blueprint $table) {
            $table->double('itensity_attack_easy')->nullable();
            $table->double('itensity_attack_hard')->nullable();
            $table->double('control_area_apbn')->nullable();
            $table->double('control_area_apbd_1')->nullable();
            $table->double('control_area_apbd_2')->nullable();
            $table->double('control_area_apbd_public')->nullable();
            $table->double('price_average')->nullable();
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
