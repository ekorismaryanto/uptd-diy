<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportCropsDrought extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_crops_droughts', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->date('periode')->nullable();
            $table->integer('sub_district_id')->nullable();
            $table->integer('village_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->string('varieties')->nullable();
            $table->string('age')->nullable();
            $table->double('planting_area')->nullable();
            $table->double('broadly_alert')->nullable();
            $table->string('periode_tb')->nullable();
            $table->double('previous_periode_low')->nullable();
            $table->double('previous_periode_mid')->nullable();
            $table->double('previous_periode_hight')->nullable();
            $table->double('previous_periode_puso')->nullable();
            $table->double('area_added_periode_low')->nullable();
            $table->double('area_added_periode_mid')->nullable();
            $table->double('area_added_periode_hight')->nullable();
            $table->double('area_added_periode_puso')->nullable();
            $table->double('area_added_periode_j')->nullable();
            $table->double('area_state_periode_low')->nullable();
            $table->double('area_state_periode_mid')->nullable();
            $table->double('area_state_periode_hight')->nullable();
            $table->double('area_state_periode_puso')->nullable();
            $table->string('handling_effort')->nullable();
            $table->double('handling_effort_total')->nullable();
            $table->string('coordinate')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_crops_droughts');
    }
}
