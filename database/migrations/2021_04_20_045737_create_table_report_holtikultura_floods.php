<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReportHoltikulturaFloods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_holticulture_floods', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->date('periode')->nullable();
            $table->integer('sub_district_id')->nullable();
            $table->integer('village_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->string('varieties')->nullable();
            $table->string('age')->nullable();
            $table->double('planting_area')->nullable();
            $table->double('broadly_alert')->nullable();
            $table->string('periode_tb')->nullable();
            $table->double('previous_month_rest_receding_area')->nullable();
            $table->double('previous_month_rest_receding_description')->nullable();
            $table->double('previous_month_rest_puso_area')->nullable();
            $table->double('previous_month_rest_puso_description')->nullable();
            $table->double('more_area_added')->nullable();
            $table->double('puso_area_added')->nullable();
            $table->double('more_area_state_period')->nullable();
            $table->double('puso_area_state_period')->nullable();
            $table->string('handling_effort')->nullable();
            $table->double('total_handling_effort')->nullable();
            $table->string('coordinate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_holticulture_floods');
    }
}
