<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDataReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_reports', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('commodity_id')->nullable();
            $table->integer('type_commodity')->nullable();
            $table->integer('sub_district_id')->nullable();
            $table->integer('category_damage')->nullable();
            $table->double('surface_area')->nullable();
            $table->double('area_of_damage')->nullable();
            $table->double('control_area')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_reports');
    }
}
