<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/keadaan-opt', 'HomeController@keadaanOpt')->name('keadaan-opt');
Route::get('/data-petugas', 'HomeController@dataPetugas')->name('data-petugas');
Route::get('/news', 'HomeController@news')->name('news');
Route::get('/news-detail/{url}', 'HomeController@newsDetail')->name('news-detail');


Route::group(['as' =>'admin.', 'namespace' => '\App\Http\Controllers\Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('', 'DashboardController@index')->name('index');
    Route::get('profile', 'ProfileController@profile')->name('profile');
    Route::post('profile/update', 'ProfileController@update')->name('profile.update');

    Route::get('sub-district', 'JsonController@subDistrict')->name('json.sub-district');
    Route::get('opt', 'JsonController@opt')->name('json.opt');
    Route::get('commodity', 'JsonController@commodity')->name('json.commodity');
    Route::get('village', 'JsonController@village')->name('json.village');

    Route::group([
        'namespace' => '\App\Http\Controllers\Admin\UserManagement', 
        'as' =>'user-management.', 'prefix' => 'user-management'], function () {
            
            Route::group(['as' =>'user.', 'prefix' => 'user'], function () {
                Route::get('', 'UserController@index')->name('index');       
                Route::get('create', 'UserController@create')->name('create');
                Route::post('store', 'UserController@store')->name('store');
                Route::get('{id}/edit', 'UserController@edit')->name('edit');
                Route::post('{id}/update', 'UserController@update')->name('update');
                Route::get('{id}/delete', 'UserController@delete')->name('delete');
            });
            Route::group(['as' =>'permission.', 'prefix' => 'permission'], function () {
                Route::get('', 'PermissionController@index')->name('index');       
                Route::get('create', 'PermissionController@create')->name('create');
                Route::post('store', 'PermissionController@store')->name('store');
                Route::get('{id}/edit', 'PermissionController@edit')->name('edit');
                Route::post('{id}/update', 'PermissionController@update')->name('update');
                Route::get('{id}/delete', 'PermissionController@delete')->name('delete');
            });

            Route::group(['as' =>'role.', 'prefix' => 'role'], function () {
                Route::get('', 'RoleController@index')->name('index');       
                Route::get('create', 'RoleController@create')->name('create');
                Route::post('store', 'RoleController@store')->name('store');
                Route::get('{id}/edit', 'RoleController@edit')->name('edit');
                Route::post('{id}/update', 'RoleController@update')->name('update');
                Route::get('{id}/delete', 'RoleController@delete')->name('delete');
            });
    });

    Route::group([
        'namespace' => '\App\Http\Controllers\Admin\DataMaster', 
        'as' =>'datamaster.', 'prefix' => 'data-master'], function () {

            Route::group(['as' =>'commodity.', 'prefix' => 'commodity'], function () {
                Route::get('', 'CommodityController@index')->name('index');       
                Route::get('create', 'CommodityController@create')->name('create');
                Route::post('store', 'CommodityController@store')->name('store');
                Route::get('{id}/edit', 'CommodityController@edit')->name('edit');
                Route::post('{id}/update', 'CommodityController@update')->name('update');
                Route::get('{id}/delete', 'CommodityController@delete')->name('delete');
                Route::post('import', 'CommodityController@import')->name('import');
            });

            Route::group(['as' =>'distraction.', 'prefix' => 'distraction'], function () {
                Route::get('', 'DistractionController@index')->name('index');       
                Route::get('create', 'DistractionController@create')->name('create');
                Route::post('store', 'DistractionController@store')->name('store');
                Route::get('{id}/edit', 'DistractionController@edit')->name('edit');
                Route::post('{id}/update', 'DistractionController@update')->name('update');
                Route::get('{id}/delete', 'DistractionController@delete')->name('delete');
                Route::post('import', 'DistractionController@import')->name('import');

            });

            Route::group(['as' =>'sub-district.', 'prefix' => 'sub-district'], function () {
                Route::get('', 'SubdistrictController@index')->name('index');       
                Route::get('create', 'SubdistrictController@create')->name('create');
                Route::post('store', 'SubdistrictController@store')->name('store');
                Route::get('{id}/edit', 'SubdistrictController@edit')->name('edit');
                Route::post('{id}/update', 'SubdistrictController@update')->name('update');
                Route::get('{id}/delete', 'SubdistrictController@delete')->name('delete');
            });

            Route::group(['as' =>'news.', 'prefix' => 'news'], function () {
                Route::get('', 'NewsController@index')->name('index');       
                Route::get('create', 'NewsController@create')->name('create');
                Route::post('store', 'NewsController@store')->name('store');
                Route::get('{id}/edit', 'NewsController@edit')->name('edit');
                Route::post('{id}/update', 'NewsController@update')->name('update');
                Route::get('{id}/delete', 'NewsController@delete')->name('delete');
                Route::post('import', 'NewsController@import')->name('import');

            });
    });

    Route::group(['as' =>'import.', 'prefix' => 'import'], function () {
        Route::get('perkebunan', 'ImportController@importPerkebunan')->name('importPerkebunan'); 
        Route::post('import-perkebunan', 'ImportController@storeImportPerkebunan')->name('store-import-perkebunan');
        Route::get('tanaman-pangan', 'ImportController@importTanamanPangan')->name('tanaman-pangan'); 
        Route::post('import-tanaman-pangan', 'ImportController@storeImportTanamanPangan')->name('store-import-tanaman-pangan');
        Route::get('holtikultura', 'ImportController@importHoltikultura')->name('holtikultura'); 
        Route::post('import-holtikultura', 'ImportController@storeImportHoltikultura')->name('store-import-holtikultura');

    });

    Route::group([
        'namespace' => '\App\Http\Controllers\Admin\Data', 
        'as' =>'data.', 'prefix' => 'data'], function () {

            Route::group(['as' =>'data-report.', 'prefix' => 'data-report'], function () {
                Route::get('', 'DataReportController@index')->name('index');       
                Route::get('create', 'DataReportController@create')->name('create');
                Route::post('create-1', 'DataReportController@createStep1')->name('create-step-1');
                Route::post('create-final', 'DataReportController@preview')->name('create-final');
                Route::get('store', 'DataReportController@store')->name('store');
            });
    });

    Route::group([
        'namespace' => '\App\Http\Controllers\Admin'], function () {

            Route::group(['as' =>'statistic.', 'prefix' => 'statistic'], function () {
                Route::get('', 'StatisticController@index')->name('index');       
                Route::get('{id}/detail', 'StatisticController@detail')->name('detail');       
            });

            Route::group(['as' =>'report.', 'prefix' => 'report'], function () {

                Route::group(['as' =>'perkebunan.', 'prefix' => 'perkebunan'], function () {
                    Route::get('', 'ReportPlantationController@index')->name('index');       
                    Route::get('{id}/detail', 'ReportPlantationController@detail')->name('detail');
                    Route::get('{id}/edit', 'ReportPlantationController@edit')->name('edit');
                    Route::get('{id}/delete', 'ReportPlantationController@delete')->name('delete');
                    Route::post('{id}/update', 'ReportPlantationController@update')->name('update');
                    Route::get('{id}/detail-print', 'ReportPlantationController@detailPrint')->name('detail-print');
                    Route::get('{id}/detail-print-pdf', 'ReportPlantationController@detailPrintPdf')->name('detail-print-pdf');
                    Route::get('print', 'ReportPlantationController@print')->name('print');
                    Route::get('xls', 'ReportPlantationController@xls')->name('xls');
                    Route::get('csv', 'ReportPlantationController@csv')->name('csv');
                    Route::get('pdf', 'ReportPlantationController@pdf')->name('pdf');

                    Route::group(['as' =>'dpi.', 'prefix' => 'dpi'], function () {
                        Route::get('', 'ReportPlantationDpiController@index')->name('index');       
                        Route::get('{id}/detail', 'ReportPlantationDpiController@detail')->name('detail');
                        Route::get('{id}/edit', 'ReportPlantationDpiController@edit')->name('edit');
                        Route::get('{id}/delete', 'ReportPlantationDpiController@delete')->name('delete');
                        Route::post('{id}/update', 'ReportPlantationDpiController@update')->name('update');
                        Route::get('{id}/detail-print', 'ReportPlantationDpiController@detailPrint')->name('detail-print');
                        Route::get('{id}/detail-print-pdf', 'ReportPlantationDpiController@detailPrintPdf')->name('detail-print-pdf');
                        Route::get('print', 'ReportPlantationDpiController@print')->name('print');
                        Route::get('xls', 'ReportPlantationDpiController@xls')->name('xls');
                        Route::get('csv', 'ReportPlantationDpiController@csv')->name('csv');
                        Route::get('pdf', 'ReportPlantationDpiController@pdf')->name('pdf');
                    });

                    Route::group(['as' =>'lkk.', 'prefix' => 'lkk'], function () {
                        Route::get('', 'ReportPlantationLkkController@index')->name('index');       
                        Route::get('{id}/detail', 'ReportPlantationLkkController@detail')->name('detail');
                        Route::get('{id}/edit', 'ReportPlantationLkkController@edit')->name('edit');
                        Route::get('{id}/delete', 'ReportPlantationLkkController@delete')->name('delete');
                        Route::post('{id}/update', 'ReportPlantationLkkController@update')->name('update');
                        Route::get('{id}/detail-print', 'ReportPlantationLkkController@detailPrint')->name('detail-print');
                        Route::get('{id}/detail-print-pdf', 'ReportPlantationLkkController@detailPrintPdf')->name('detail-print-pdf');
                        Route::get('print', 'ReportPlantationLkkController@print')->name('print');
                        Route::get('xls', 'ReportPlantationLkkController@xls')->name('xls');
                        Route::get('csv', 'ReportPlantationLkkController@csv')->name('csv');
                        Route::get('pdf', 'ReportPlantationLkkController@pdf')->name('pdf');
                    });
                });

                Route::group(['as' =>'tanaman-pangan.', 'prefix' => 'tanaman-pangan'], function () {
                    Route::get('', 'ReportTanamanPanganController@index')->name('index');
                    Route::get('{id}/edit', 'ReportTanamanPanganController@edit')->name('edit');
                    Route::get('{id}/delete', 'ReportTanamanPanganController@delete')->name('delete');
                    Route::post('{id}/update', 'ReportTanamanPanganController@update')->name('update');
                    Route::get('{id}/detail', 'ReportTanamanPanganController@detail')->name('detail');
                    Route::get('{id}/detail-print', 'ReportTanamanPanganController@detailPrint')->name('detail-print');
                    Route::get('{id}/detail-print-pdf', 'ReportTanamanPanganController@detailPrintPdf')->name('detail-print-pdf');
                    Route::get('print', 'ReportTanamanPanganController@print')->name('print');
                    Route::get('xls', 'ReportTanamanPanganController@xls')->name('xls');
                    Route::get('csv', 'ReportTanamanPanganController@csv')->name('csv');
                    Route::get('pdf', 'ReportTanamanPanganController@pdf')->name('pdf'); 

                    Route::group(['as' =>'banjir.', 'prefix' => 'banjir'], function () {
                        Route::get('', 'ReportTanamanPanganBanjirController@index')->name('index');
                        Route::get('{id}/edit', 'ReportTanamanPanganBanjirController@edit')->name('edit');
                        Route::get('{id}/delete', 'ReportTanamanPanganBanjirController@delete')->name('delete');
                        Route::post('{id}/update', 'ReportTanamanPanganBanjirController@update')->name('update');
                        Route::get('{id}/detail', 'ReportTanamanPanganBanjirController@detail')->name('detail');
                        Route::get('{id}/detail-print', 'ReportTanamanPanganBanjirController@detailPrint')->name('detail-print');
                        Route::get('{id}/detail-print-pdf', 'ReportTanamanPanganBanjirController@detailPrintPdf')->name('detail-print-pdf');
                        Route::get('print', 'ReportTanamanPanganBanjirController@print')->name('print');
                        Route::get('xls', 'ReportTanamanPanganBanjirController@xls')->name('xls');
                        Route::get('csv', 'ReportTanamanPanganBanjirController@csv')->name('csv');
                        Route::get('pdf', 'ReportTanamanPanganBanjirController@pdf')->name('pdf'); 
                    });


                    Route::group(['as' =>'kekeringan.', 'prefix' => 'kekeringan'], function () {
                        Route::get('', 'ReportTanamanPanganKekeringanController@index')->name('index');
                        Route::get('{id}/edit', 'ReportTanamanPanganKekeringanController@edit')->name('edit');
                        Route::get('{id}/delete', 'ReportTanamanPanganKekeringanController@delete')->name('delete');
                        Route::post('{id}/update', 'ReportTanamanPanganKekeringanController@update')->name('update');
                        Route::get('{id}/detail', 'ReportTanamanPanganKekeringanController@detail')->name('detail');
                        Route::get('{id}/detail-print', 'ReportTanamanPanganKekeringanController@detailPrint')->name('detail-print');
                        Route::get('{id}/detail-print-pdf', 'ReportTanamanPanganKekeringanController@detailPrintPdf')->name('detail-print-pdf');
                        Route::get('print', 'ReportTanamanPanganKekeringanController@print')->name('print');
                        Route::get('xls', 'ReportTanamanPanganKekeringanController@xls')->name('xls');
                        Route::get('csv', 'ReportTanamanPanganKekeringanController@csv')->name('csv');
                        Route::get('pdf', 'ReportTanamanPanganKekeringanController@pdf')->name('pdf'); 
                    });

                });

                Route::group(['as' =>'holtikultura.', 'prefix' => 'holtikultura'], function () {
                    Route::get('', 'ReportHoltikulturaController@index')->name('index');
                    Route::get('{id}/edit', 'ReportHoltikulturaController@edit')->name('edit');
                    Route::get('{id}/delete', 'ReportHoltikulturaController@delete')->name('delete');
                    Route::post('{id}/update', 'ReportHoltikulturaController@update')->name('update');
                    Route::get('{id}/detail', 'ReportHoltikulturaController@detail')->name('detail');
                    Route::get('{id}/detail-print', 'ReportHoltikulturaController@detailPrint')->name('detail-print');
                    Route::get('{id}/detail-print-pdf', 'ReportHoltikulturaController@detailPrintPdf')->name('detail-print-pdf');
                    Route::get('print', 'ReportHoltikulturaController@print')->name('print');
                    Route::get('xls', 'ReportHoltikulturaController@xls')->name('xls');
                    Route::get('csv', 'ReportHoltikulturaController@csv')->name('csv');
                    Route::get('pdf', 'ReportHoltikulturaController@pdf')->name('pdf'); 

                    Route::group(['as' =>'banjir.', 'prefix' => 'banjir'], function () {
                        Route::get('', 'ReportHoltikulturaBanjirController@index')->name('index');
                        Route::get('{id}/edit', 'ReportHoltikulturaBanjirController@edit')->name('edit');
                        Route::get('{id}/delete', 'ReportHoltikulturaBanjirController@delete')->name('delete');
                        Route::post('{id}/update', 'ReportHoltikulturaBanjirController@update')->name('update');
                        Route::get('{id}/detail', 'ReportHoltikulturaBanjirController@detail')->name('detail');
                        Route::get('{id}/detail-print', 'ReportHoltikulturaBanjirController@detailPrint')->name('detail-print');
                        Route::get('{id}/detail-print-pdf', 'ReportHoltikulturaBanjirController@detailPrintPdf')->name('detail-print-pdf');
                        Route::get('print', 'ReportHoltikulturaBanjirController@print')->name('print');
                        Route::get('xls', 'ReportHoltikulturaBanjirController@xls')->name('xls');
                        Route::get('csv', 'ReportHoltikulturaBanjirController@csv')->name('csv');
                        Route::get('pdf', 'ReportHoltikulturaBanjirController@pdf')->name('pdf'); 
                    });


                    Route::group(['as' =>'kekeringan.', 'prefix' => 'kekeringan'], function () {
                        Route::get('', 'ReportHoltikulturaKekeringanController@index')->name('index');
                        Route::get('{id}/edit', 'ReportHoltikulturaKekeringanController@edit')->name('edit');
                        Route::get('{id}/delete', 'ReportHoltikulturaKekeringanController@delete')->name('delete');
                        Route::post('{id}/update', 'ReportHoltikulturaKekeringanController@update')->name('update');
                        Route::get('{id}/detail', 'ReportHoltikulturaKekeringanController@detail')->name('detail');
                        Route::get('{id}/detail-print', 'ReportHoltikulturaKekeringanController@detailPrint')->name('detail-print');
                        Route::get('{id}/detail-print-pdf', 'ReportHoltikulturaKekeringanController@detailPrintPdf')->name('detail-print-pdf');
                        Route::get('print', 'ReportHoltikulturaKekeringanController@print')->name('print');
                        Route::get('xls', 'ReportHoltikulturaKekeringanController@xls')->name('xls');
                        Route::get('csv', 'ReportHoltikulturaKekeringanController@csv')->name('csv');
                        Route::get('pdf', 'ReportHoltikulturaKekeringanController@pdf')->name('pdf'); 
                    });

                });

            });
    });

});


Route::group(['namespace' => '\App\Http\Controllers\Auth'], function() {
    Route::group(['as' =>'auth.login.', 'prefix' => 'auth/login'], function () {
        Route::get('', 'LoginController@index')->name('index')->middleware('guest');
        Route::post('do-login', 'LoginController@doLogin')->name('do-login');
        Route::get('sign-out', 'LoginController@logout')->name('sign-out');
    });
});

