/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
    config.height = 500;
    config.toolbarGroups = [
        // { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        // { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        // {
        //     name: "editing",
        //     groups: ["find", "selection", "spellchecker", "editing"],
        // },
        // { name: "forms", groups: ["forms"] },
        "/",
        {
            name: "paragraph",
            groups: [
                "basicstyles",
                "list",
                "align",
                "paragraph",
                "styles",
                "colors",
            ],
        },
        // { name: "links", groups: ["links"] },
        // { name: 'insert', groups: [ 'insert' ] },
        // { name: "tools", groups: ["tools"] },
        // { name: "others", groups: ["others"] },
        // { name: "about", groups: ["about"] },
    ];

    config.enterMode = CKEDITOR.ENTER_BR;
    //config.basicEntities = false;
    config.htmlEncodeOutput = false;
    //config.entities = false;

    //config.removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,RemoveFormat,Indent,Outdent,CreateDiv,JustifyCenter,JustifyRight,JustifyBlock,JustifyLeft,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Format,Font,FontSize,TextColor,BGColor,ShowBlocks,Strike';
};
