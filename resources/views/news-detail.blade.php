<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ URL::asset('assets/css/bootstrap-homepage.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/css/homepage-style.css')}}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico')}}">
    <link rel="stylesheet" type="text/css" href="http://www.prepbootstrap.com/Content/shieldui-lite/dist/css/light/all.min.css" />
    <link href="{{ URL::asset('assets/plugins/c3/c3.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.min.css')}}" />
    <title>UPTD BPTP DIY</title>
</head>

<body class="d-flex text-white">
    <div class="mysidebar">
        <div id="overlay"></div>
        <div class="sidebar-custom" id="sidebar">
            <div class="list-group pt-5">
                @include('layouts.home-header')
            </div>
        </div>
    </div>
    <div class="cover-container d-flex w-100 h-100 mx-auto flex-column">
        <header class="home-aboutus mb-auto" style="border-bottom: 4px solid #1dc293;">
            <div class="d-flex justify-content-center">
                <div class="navigasi container d-flex align-items-center justify-content-between">
                    <div class="float-md-start mb-0" style="color: black;">
                        <div class="d-flex align-items-center logo">
                            <img src="{{ URL::asset('assets/images/logobrand.png')}}">
                            <div class="brand">
                                <p>UPTD BPTP</p>
                                <p>Dinas Pertanian dan Ketahanan Pangan</p>
                                <p>D.I Yogyakarta</p>
                            </div>
                        </div>
                    </div>
                    <a href="#" id="menu-button-sidebar"><img src="{{ URL::asset('assets/images/menu.png')}}"
                            height="20px" alt="" srcset=""></a>
                    <nav class="nav nav-masthead justify-content-center float-md-end">
                        @include('layouts.mobile-header')
                    </nav>
                </div>
            </div>
            <div class="about-us-title">
                <p>Artikel Dan Berita</p>
            </div>
        </header>

        <section class="bg-light">
            <div class="container">
                <div class="row my-3">
                    <div class="col-lg-12">
                        
                        <div class="blog-post" style="color: #58798C">
                            <h2 class="blog-post-title">{{ $news->title }}</h2>
                            <p class="blog-post-meta">{{ date('d F Y', strtotime($news->created_at)) }} by <a href="#">{{ $news->user->name }}</a></p>
                            <hr>
                            {!! $news->text !!}
                          </div>
                        
                    </div>
                </div>
            </div>
        </section>

        {{-- <section style="background-image: url('{{ URL::asset('assets/images/plant_lab_black.jpg')}}'); background-size: cover; background-position: 50%; background-repeat: no-repeat; position: relative;">
            <div class="position-relative" style="background-color: black;"></div>
            <div class="container">
                <div class="row p-5 my-5 d-flex align-items-stratch ">
                    <div class="col-lg-4 text-center my-3">
                        <h3 class="animate__animated animate__fadeInUp">Lab Pengendalian</h3>
                    </div>
                    <div class="col-lg-4 text-center my-3">
                        <h3 class="animate__animated animate__fadeInUp">Hama Penyakit</h3>
                    </div>
                    <div class="col-lg-4 text-center my-3">
                        <h3 class="animate__animated animate__fadeInUp">Tanaman (LPHPT)</h3>
                    </div>

                </div>
            </div>
        </section> --}}
        <footer class="mt-auto text-center" style="background: none; color: black;">
            <p>© {{date('Y')}} All rights reserved. BPTP DIY</p>
        </footer>
    </div>
    <script src="{{ URL::asset('assets/js/popper.min.js')}}"
        integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous">
    </script>
    <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"
        integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous">
    </script>

    <script>
        var sidebar = document.getElementById("sidebar");
        var overlay = document.getElementById("overlay");
        document.getElementById("menu-button-sidebar").addEventListener("click", function () {
            sidebar.classList.toggle("active");
            overlay.classList.toggle("active");
        });

        overlay.addEventListener("click", function () {
            sidebar.classList.toggle("active");
            overlay.classList.toggle("active");
        });

    </script>
</body>

</html>
