<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
    <link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
    <style>
        .page-break {
            page-break-after: always;
        }
        
        align-middle {
            vertical-align:middle!important
        }

        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;

        }

        @page { size: landscape; }
    </style>

    @php
        use \App\Http\Constants\TypeCommodity;
        use \App\Http\Constants\ControlArea;
    @endphp
<div class="row">
    <div class="col-md-12">
        <div class="form-group print-area">
            <table class="table table-bordered table-striped mb-0">
                <tbody>
                    <tr>
                        <td width="100%" class="">
                            {!! $report->description !!}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="form-group print-area">
            <table class="table table-bordered table-striped mb-0">
                <tbody>
                    <tr>
                        <td width="50%">Nama</td>
                        <td>{{ ($report->user)->name }}</td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td>Yogyakarta</td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td>{{ optional($report->subDistrict)->name }}</td>
                    </tr>
                    <tr>
                        <td>Periode</td>
                        <td>{{ date_view($report->date) }}</td>
                    </tr>
                    <tr>
                        <td>Kategori</td>
                        <td>{{ TypeCommodity::label($report->type_commodity) }}</td>
                    </tr>
                    <tr>
                        <td>Komoditas</td>
                        <td>{{ $report->commodity->name }}</td>
                    </tr>
                    <tr>
                        @php
                        $opt = optional($report->optDatas)->map(function($q){
                                                    return $q->opt->name;
                                                })->toArray();
                        @endphp
                        <td>OPT</td>
                        <td>{{ implode(', ', $opt) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Intensitas Serangan</label>
                <div class="form-group">
                    <table class="table table-bordered table-striped mb-0">
                        <tbody>
                            <tr>
                                <td width="50%">Ringan</td>
                                <td>{{ $report->itensity_attack_easy }}</td>
                            </tr>
                            <tr>
                                <td>Berat</td>
                                <td>{{ $report->itensity_attack_hard }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12">
                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Pengendalian</label>
                <div class="form-group">
                    <table class="table table-bordered table-striped mb-0">
                        <tbody>
                            <tr>
                                <td width="50%">ABPN</td>
                                <td>{{ $report->control_area_apbn }}</td>
                            </tr>
                            <tr>
                                <td width="50%">APBD I</td>
                                <td>{{ $report->control_area_apbd_1 }}</td>
                            </tr>
                            <tr>
                                <td width="50%">APBD II</td>
                                <td>{{ $report->control_area_apbd_2 }}</td>
                            </tr>
                            <tr>
                                <td width="50%">Masyarakat</td>
                                <td>{{ $report->control_area_apbd_public }}</td>
                            </tr>
        
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12">
                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Cara Pengendalian</label>
                <div class="form-group">
                    <table class="table table-bordered table-striped mb-0">
                        <tbody>
                            <tr>
                                <td width="50%">
                                    @php
                                            $pengendalian = optional($report->controlAreas)->map(function($q){
                                                return ControlArea::label($q->control_area);
                                            })->toArray();
                                    @endphp
                                    {{ implode(', ', $pengendalian) }}
                                </td>
                                <td>{{ $report->control_area_other ?? '-' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12">
                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Harga rata-rata / ton</label>
                <div class="form-group">
                    <table class="table table-bordered table-striped mb-0">
                        <tbody>
                            <tr>
                                <td>Rp. {{ $report->price_average }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    window.print();
</script>

</html>