<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
</head>
<body>

    
    <style>
    .page-break {
        page-break-after: always;
    }
    </style>
    <style>
        @page { 
            size: legal landscape; 
        }
        align-middle{vertical-align:middle!important}

        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;

        }
        </style>
        <style type="text/css" media="print">
            @page { size: landscape; }
        </style>
          
    @php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp
    <table id="tech-companies-1" class="table table-striped">
        <thead>
            <tr>
                <th class="align-middle" rowspan="2">Periode</th>
                <th class="align-middle" rowspan="2">User</th>
                <th class="align-middle" rowspan="2">Kabupaten</th>
                <th class="align-middle" rowspan="2">Kecamatan</th>
                <th class="align-middle" rowspan="2">Kategori</th>
                <th class="align-middle" rowspan="2">Tanaman</th>
                <th class="align-middle" rowspan="2">OPT</th>
                <th class="align-middle" rowspan="2" style="min-width: 170px;">Luas
                    Pertanaman (ha)</th>
                <th class="align-middle text-center" style="min-width: 170px;"
                    colspan="2">Intensitas Serangan</th>
                <th class="align-middle" rowspan="2">Jumlah</th>
                <th class="align-middle text-center" colspan="4">Intensitas Serangan
                </th>
                <th class="align-middle" rowspan="2">Jumlah</th>
                <th class="align-middle" rowspan="2">Cara Pengendalian</th>
                <th class="align-middle" rowspan="2" style="min-width: 200px;">Harga rata - rata/Ton (Rp)</th>
            </tr>
            <tr>
                <th>Ringan</th>
                <th>Berat</th>
                <th>Ringan</th>
                <th>Berat</th>
                <th>Ringan</th>
                <th>Berat</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($reports as $item)
            <tr>
                <td>{{ date_view($item->date) }}</td>
                <td>{{ optional($item->user)->name ?? '-' }}</td>
                <td>{{ optional($item->subDistrict)->district->name }}</td>
                <td>{{ optional($item->subDistrict)->name }}</td>
                <td>{{ TypeCommodity::label($item->type_commodity) }}</td>
                <td>{{ optional($item->commodity)->name }}</td>
                <td>
                    @php
                        $opt = optional($item->optDatas)->map(function($q){
                            return $q->opt->name;
                        })->toArray();
                    @endphp
                    
                    {{ $opt ? implode(', ', $opt) : '-' }}
                </td>
                <td>{{ $item->planting_area }}</td>
                <td>{{ $item->itensity_attack_easy }}</td>
                <td>{{ $item->itensity_attack_hard }}</td>
                <td>{{  $item->itensity_attack_easy +  $item->itensity_attack_hard }}</td>
                <td>{{ $item->control_area_apbn }}</td>
                <td>{{ $item->control_area_apbd_1 }}</td>
                <td>{{ $item->control_area_apbd_2 }}</td>
                <td>{{ $item->control_area_apbd_public }}</td>
                <td>
                    {{
                        $item->control_area_apbn 
                        +
                        $item->control_area_apbd_1
                        +
                        $item->control_area_apbd_2
                        +
                        $item->control_area_apbd_public
                    }}
                </td>
                <td>
                    @php
                        $pengendalian = optional($item->controlAreas)->map(function($q){
                            return ControlArea::label($q->control_area);
                        })->toArray();
                    @endphp
                    
                    {{ $pengendalian ? implode(', ', $pengendalian) : '-' }}
                </td>
                <td>{{ $item->price_average }}</td>
            <tr>
            @endforeach
        </tbody>
    </table> 
</body>

@if (Request::routeIs('admin.report.print'))
    <script>
        window.print();
    </script>
@endif
</html>