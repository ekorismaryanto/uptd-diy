<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ URL::asset('assets/css/bootstrap-homepage.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/css/homepage-style.css')}}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico')}}">
    <link rel="stylesheet" type="text/css" href="http://www.prepbootstrap.com/Content/shieldui-lite/dist/css/light/all.min.css" />
    <link href="{{ URL::asset('assets/plugins/c3/c3.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.min.css')}}" />
    <title>UPTD BPTP DIY</title>
    <style>
        p img{
            width: 10px;
        }
    </style>
</head>

<body class="d-flex text-white">
    <div class="mysidebar">
        <div id="overlay"></div>
        <div class="sidebar-custom" id="sidebar">
            <div class="list-group pt-5">
                @include('layouts.home-header')
            </div>
        </div>
    </div>
    <div class="cover-container d-flex w-100 h-100 mx-auto flex-column">
        <header class="home-aboutus mb-auto" style="border-bottom: 4px solid #1dc293;">
            <div class="d-flex justify-content-center">
                <div class="navigasi container d-flex align-items-center justify-content-between">
                    <div class="float-md-start mb-0" style="color: black;">
                        <div class="d-flex align-items-center logo">
                            <img src="{{ URL::asset('assets/images/logobrand.png')}}">
                            <div class="brand">
                                <p>UPTD BPTP</p>
                                <p>Dinas Pertanian dan Ketahanan Pangan</p>
                                <p>D.I Yogyakarta</p>
                            </div>
                        </div>
                    </div>
                    <a href="#" id="menu-button-sidebar"><img src="{{ URL::asset('assets/images/menu.png')}}"
                            height="20px" alt="" srcset=""></a>
                    <nav class="nav nav-masthead justify-content-center float-md-end">
                        @include('layouts.mobile-header')
                    </nav>
                </div>
            </div>
            <div class="about-us-title">
                <p>Artikel Dan Berita</p>
            </div>
        </header>

        <section class="bg-light">
            <div class="container">
                <div class="row my-3">
                    <div class="col-lg-12">
                        
                        <div class="row mb-2">
                            @foreach ($news as $item)
                                <div class="col-md-6">
                                    <div class="card flex-md-row mb-4 box-shadow h-md-250">
                                        <div class="card-body d-flex flex-column align-items-start">
                                            <h3 class="mb-0">
                                                <a class="text-dark" href="{{ route('news-detail', $item->slug) }}">{{ $item->title }}</a>
                                            </h3>
                                            <div class="mb-1 text-muted">{{ date('d F Y', strtotime($item->created_at)) }}</div>
                                            <p class="card-text mb-auto" style="color: #58798C">{!! substr($item->text, 0, 150); !!}....</p>
                                        </div>
                                        @if ($item->image)
                                            <img class="card-img-right flex-auto d-none d-md-block" theme=thumb" alt="Thumbnail [200x250]" style="width: 100px;" src="{{ url('uploads/news/'. $item->image)}}" />
                                        @else
                                            <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" style="width: 200px; height: 250px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_179110eb899%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_179110eb899%22%3E%3Crect%20width%3D%22200%22%20height%3D%22250%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2256.1953125%22%20y%3D%22131%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>

        {{-- <section style="background-image: url('{{ URL::asset('assets/images/plant_lab_black.jpg')}}'); background-size: cover; background-position: 50%; background-repeat: no-repeat; position: relative;">
            <div class="position-relative" style="background-color: black;"></div>
            <div class="container">
                <div class="row p-5 my-5 d-flex align-items-stratch ">
                    <div class="col-lg-4 text-center my-3">
                        <h3 class="animate__animated animate__fadeInUp">Lab Pengendalian</h3>
                    </div>
                    <div class="col-lg-4 text-center my-3">
                        <h3 class="animate__animated animate__fadeInUp">Hama Penyakit</h3>
                    </div>
                    <div class="col-lg-4 text-center my-3">
                        <h3 class="animate__animated animate__fadeInUp">Tanaman (LPHPT)</h3>
                    </div>

                </div>
            </div>
        </section> --}}
        <footer class="mt-auto text-center" style="background: none; color: black;">
            <p>© {{date('Y')}} All rights reserved. BPTP DIY</p>
        </footer>
    </div>
    <script src="{{ URL::asset('assets/js/popper.min.js')}}"
        integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous">
    </script>
    <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"
        integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous">
    </script>

    <script>
        var sidebar = document.getElementById("sidebar");
        var overlay = document.getElementById("overlay");
        document.getElementById("menu-button-sidebar").addEventListener("click", function () {
            sidebar.classList.toggle("active");
            overlay.classList.toggle("active");
        });

        overlay.addEventListener("click", function () {
            sidebar.classList.toggle("active");
            overlay.classList.toggle("active");
        });

    </script>
</body>

</html>