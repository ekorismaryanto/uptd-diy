@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Role
                    </li>
                </ol>
                <h4>Input Data</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.user-management.role.store') }}" method="POST">
                        @csrf
                        <div class="col-12 form-group">
                            <div class="form-group">
                                <label class="control-label font-bold label-green">Nama Role</label>
                                <input type="text" name="name" class="input-sm form-control" placeholder="Input Nama Role" required>
                                @error('name')
                                    <div style="color: red">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-box">
                                <span><input type="checkbox" id="checkAll"> Permission</span>
                                <br>
                                @foreach($permissions as $permission)
                                    <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"> <span>{{ $permission->name }}</span><br>
                                @endforeach
                                @if($errors->has('permissions'))
                                    <div class="text-danger">{{ $errors->first('permissions') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <a href="{{ route('admin.user-management.role.index') }}" class="btn btn-danger">Batal</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
</script>
@endsection
