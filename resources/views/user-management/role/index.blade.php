@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Overview
                    </li>
                </ol>
                <h4>Role</h4>
                <h6>User Management</h6>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 my-2">
                            <form action="">
                                <div class="row align-items-center">
                                    <div class="col-lg-9">
                                        <div class="col-lg-3 my-2">
                                            <div class="form-group">
                                                <a href="{{ route('admin.user-management.role.create') }}"  class="btn btn-outline-primary btn-md-block btn-block" style="height: 38px;">Tambah Data</a>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 pull-right">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control" placeholder="Cari">
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-1 my-2">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-outline-primary btn-md-block btn-block" style="height: 38px;"><i  class="mx-2 mdi mdi-magnify"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 my-3">
                            <div class="form-group">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle"">No</th>
                                                    <th class="align-middle"">Nama</th>
                                                    <th class="align-middle"">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($roles as $key => $item)
                                                    <tr>
                                                        <td>{{ $key+=1 }}</td>
                                                        <td>{{ $item->name }}</td>
                                                        <td class="text-center">
                                                            <a href="{{ route('admin.user-management.role.edit', $item->id) }}">
                                                                <button class="btn btn-xs btn-primary"> Edit</button>
                                                            </a>
                                                            <a href="{{ route('admin.user-management.role.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                                <button class="btn btn-xs btn-danger"> Hapus</button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="2">Data Empty</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $roles->links('layouts.pagination')  !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

</script>
@endsection
