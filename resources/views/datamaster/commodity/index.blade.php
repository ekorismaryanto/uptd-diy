@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use Illuminate\Support\Facades\Storage;
@endphp

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Data Master
                    </li>
                </ol>
                <h4>Komoditas</h4>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="">
                        <div class="row justify-content-between">
                            <div class="col-md-4 col-xs-12">
                                <div class="row">
                                    <div class="col col-xs-12">
                                        <div class="form-group">
                                            <a href="{{ route('admin.datamaster.commodity.create') }}"  class="btn btn-outline-primary btn-md-block btn-block">Tambah Data</a>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal">
                                                Import Data
                                              </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12 pull-right">
                                <div class="float-right">
                                    <div class="row">
                                        <div class="col col-xs-12">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control"  name="q" value="{{ Request::get('q') }}" placeholder="Cari" aria-label="Cari" aria-describedby="basic-addon2">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-outline-secondary" type="button">
                                                        <span class="fa fa-search"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-12 my-3">
                            <div class="form-group">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th style="width: 50%">Nama</th>
                                                    <th style="width: 0%">Type</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                                @forelse ($commodities as $item)
                                                    <tr>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ TypeCommodity::label($item->type) }}</td>
                                                        <td class="text-center">
                                                            <a href="{{ route('admin.datamaster.commodity.edit', $item->id) }}" >
                                                                <button class="btn btn-primary"> Edit</button>
                                                            </a>
                                                            <a href="{{ route('admin.datamaster.commodity.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                                <button class="btn btn-danger"> Hapus</button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="2" class="text-center">Data Empty</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $commodities->links('layouts.pagination')  !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.datamaster.commodity.import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Data Komoditas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input" id="inputGroupFile01">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                    <small>* Inputkan File CSV dan Excel</small>
                    <br>
                    <small>* Mohon inputkan data sesuai dengan yang sudah ditentukan. contoh format file bisa di download dibawah ini</small>
                    <br>
                    <a href="{{ url('storage/sample/sample-komoditas.xlsx') }}" class="btn btn-sm btn-outline-primary" download="">
                        Sample File Import
                    </a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

</script>
@endsection
