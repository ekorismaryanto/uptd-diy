@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;    
@endphp

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Data Master
                    </li>
                </ol>
                <h4>Input Data</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.datamaster.news.update', $news->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Judul</label>
                                    <input type="text" name="name" class="form-control" placeholder="Input Judul" required value="{{ $news->title }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Konten</label>
                                    <textarea name="text" id="editor1" rows="10" cols="100" required>
                                        {{ $news->text }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <img src="{{ $news->image ? url('uploads/news/'. $news->image) : 'https://via.placeholder.com/350x150' }}" id="preview" style="height:180px" class="img-thumbnail">
                                <input type="file" name="img" class="file" accept="image/*">
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <a href="{{ route('admin.datamaster.news.index') }}" class="btn btn-danger">Batal</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="https://cdn.ckeditor.com/4.16.0/standard-all/ckeditor.js"></script>
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>	
	$(document).on("click", ".browse", function() {
		var file = $(this).parents().find(".file");
		file.trigger("click");
	});

	$('input[type="file"]').change(function(e) {
		var fileName = e.target.files[0].name;

		$("#file").val(fileName);

		var reader = new FileReader();
		reader.onload = function(e) {
			document.getElementById("preview").src = e.target.result;
		};

		reader.readAsDataURL(this.files[0]);
	});
	
	$(document).ready(function() {
		$("#checkAll").click(function(){
			$('input:checkbox').not(this).prop('checked', this.checked);
		});
	});
</script>

<script>
	CKEDITOR.addCss('.cke_editable { font-size: 15px; padding: 2em; }');
	CKEDITOR.replace('editor1', {
		toolbar: [
            {
                name: 'document',
                items: ['Print']
            },
            {
                name: 'clipboard',
                items: ['Undo', 'Redo']
            },
            {
                name: 'styles',
                items: ['Format', 'Font', 'FontSize']
            },
            {
                name: 'colors',
                items: ['TextColor', 'BGColor']
            },
            {
                name: 'align',
                items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
            },
            '/',
            {
                name: 'basicstyles',
                items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']
            },
            {
                name: 'links',
                items: ['Link', 'Unlink']
            },
            {
                name: 'paragraph',
                items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
            },
            {
                name: 'insert',
                items: ['Image', 'Table']
            },
            {
                name: 'tools',
                items: ['Maximize']
            },
            {
                name: 'editing',
                items: ['Scayt']
            }
		],
		
		extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',
		filebrowserImageBrowseUrl: '{{ ENV('APP_URL') }}/laravel-filemanager?type=Images',
		filebrowserImageUploadUrl: '{{ ENV('APP_URL') }}/laravel-filemanager/upload?type=Images&_token=',
		filebrowserBrowseUrl: '{{ ENV('APP_URL') }}/laravel-filemanager?type=Files',
		filebrowserUploadUrl: '{{ ENV('APP_URL') }}/laravel-filemanager/upload?type=Files&_token=',
		height: 560,
		removeDialogTabs: 'image:advanced;link:advanced'
	});
</script>
@endsection
