            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Main Navigation</li>
                            @can('overview')
                            <li>
                                <a href="{{ route('admin.index') }}" class="waves-effect {{ Request::routeIs('admin.index') ? 'active' : '' }}">
                                    <i class="mdi mdi-view-dashboard"></i><span> Overview </span>
                                </a>
                            </li>
                            @endcan
                            @can('input-data')
                                <li class="menu-title">Input Data</li>
                                <li>
                                    <a href="{{ route('admin.data.data-report.create') }}" class="waves-effect {{ Request::routeIs('admin.data.data-report.*') ? 'active' : '' }}">
                                        <i class="ti ti-write"></i><span> Input Data OPT</span>
                                    </a>
                                </li>
                            @endcan
                            @can('statistik')
                                <li>
                                    <a href="{{ route('admin.statistic.index') }}" class="waves-effect {{ Request::routeIs('admin.statistic.*') ? 'active' : '' }}">
                                        <i class="mdi mdi-chart-bar"></i><span> Statistik </span>
                                    </a>
                                </li>
                            @endcan
                            <li class="menu-title">Data</li>
                            <li>
                                <a href="{{ route('admin.report.perkebunan.index') }}" class="waves-effect {{ Request::routeIs('admin.report.perkebunan.*') ? 'active' : '' }}">
                                    <i class="mdi mdi-file-document"></i><span> Data Perkebunan</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.report.tanaman-pangan.index') }}" class="waves-effect {{ Request::routeIs('admin.report.tanaman-pangan.*') ? 'active' : '' }}">
                                    <i class="mdi mdi-file-document"></i><span> Data Tanaman Pangan</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.report.holtikultura.index') }}" class="waves-effect {{ Request::routeIs('admin.report.holtikultura.*') ? 'active' : '' }}">
                                    <i class="mdi mdi-file-document"></i><span> Data Hortikultura</span>
                                </a>
                            </li>
                            <li class="menu-title">Import</li>
                            <li>
                                <a href="{{ route('admin.import.importPerkebunan') }}" class="waves-effect {{ Request::routeIs('admin.import.perkebunan.*') ? 'active' : '' }}">
                                    <i class="mdi mdi-settings"></i><span> Perkebunan </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.import.tanaman-pangan') }}" class="waves-effect {{ Request::routeIs('admin.import.tanaman-pangan.*') ? 'active' : '' }}">
                                    <i class="mdi mdi-settings"></i><span> Tanaman Pangan </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.import.holtikultura') }}" class="waves-effect {{ Request::routeIs('admin.import.hotiluktura.*') ? 'active' : '' }}">
                                    <i class="mdi mdi-settings"></i><span> Hortikultura </span>
                                </a>
                            </li>
                            @can('datamaster.opt','datamaster.komoditas')
                                <li class="menu-title">Data Master</li>
                                @can('datamaster.komoditas')
                                    <li>
                                        <a href="{{ route('admin.datamaster.commodity.index') }}" class="waves-effect {{ Request::routeIs('admin.datamaster.commodity.*') ? 'active' : '' }}">
                                            <i class="mdi mdi-settings"></i><span> Komoditas </span>
                                        </a>
                                    </li>
                                @endcan
                                @can('datamaster.opt')
                                <li>
                                    <a href="{{ route('admin.datamaster.distraction.index') }}" class="waves-effect {{ Request::routeIs('admin.datamaster.distraction.*') ? 'active' : '' }}">
                                        <i class="mdi mdi-settings"></i><span> Opt </span>
                                    </a>
                                </li>
                                @endcan
                                @can('datamaster.news')
                                <li>
                                    <a href="{{ route('admin.datamaster.news.index') }}" class="waves-effect {{ Request::routeIs('admin.datamaster.news.*') ? 'active' : '' }}">
                                        <i class="mdi mdi-settings"></i><span> Berita</span>
                                    </a>
                                </li>
                                @endcan
                            @endcan
                            @can('user-management.user','user-management.role')
                                <li class="menu-title">User Management</li>
                                @can('user-management.user')
                                    <li>
                                        <a href="{{ route('admin.user-management.user.index') }}" class="waves-effect {{ Request::routeIs('admin.user-management.user.*') ? 'active' : '' }}">
                                            <i class="fa fa-users"></i><span> User </span>
                                        </a>
                                    </li>
                                @endcan
                                @can('user-management.role')
                                <li>
                                    <a href="{{ route('admin.user-management.role.index') }}" class="waves-effect {{ Request::routeIs('admin.user-management.role.*') ? 'active' : '' }}">
                                        <i class="fa fa-lock"></i><span> Role </span>
                                    </a>
                                </li>
                                @endcan
                            @endcan
                            <li class="menu-title">Data User</li>
                            <li>
                                <a href="{{ route('admin.profile') }}" class="waves-effect">
                                    <i class="fa fa-user"></i><span> Profile </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('auth.login.sign-out') }}" class="waves-effect text-danger">
                                    <i class="mdi mdi-logout"></i><span> Logout </span>
                                </a>
                            </li>

                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
