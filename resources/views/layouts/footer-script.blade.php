        <!-- jQuery  -->
        <script src="{{ URL::asset('assets/js/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/metisMenu.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{ URL::asset('assets/js/waves.min.js')}}"></script>
        <script src="{{ URL::asset('assets/ckeditor4/ckeditor.js')}}"></script>


        <script src="{{ URL::asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.js')}}"></script>

        <script src="{{ asset('assets/toast/toastr.min.js') }}"></script>
        <script>
        toastr.options = {
                "progressBar": true
        };
        </script>
        @stack('toastr')
        @if(session()->has("notice"))
        @php
                $value = Session::get('notice');
        @endphp
        @if (is_array($value))
                <script>
                @foreach ($value as $data)
                        @if ($data['labelClass'] == 'success')
                        toastr["success"]("{{ $data['content'] }}");
                        @endif
                        @if ($data['labelClass'] == 'error')
                        toastr["error"]("{{ $data['content'] }}");
                        @endif
                @endforeach
                </script>
        @endif
        @php
                Session::forget('notice');
        @endphp
        @endif

        
        @yield('script-bottom')