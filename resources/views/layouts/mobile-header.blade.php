<a class="nav-link {{ Request::path() == '/' ? 'active' : '' }}" aria-current="page" href="/">Beranda</a>
<a class="nav-link {{ Request::path() == 'about' ? 'active' : '' }}" href="/about">Tentang Kami</a>
<a class="nav-link {{ Request::routeIs('keadaan-opt') ? 'active' : '' }}" href="{{ route('keadaan-opt') }}">Keadaan OPT</a>
<a class="nav-link {{ Request::routeIs('data-petugas') ? 'active' : '' }}" href="{{ route('data-petugas') }}">Data Petugas</a>
<a class="nav-link {{ Request::routeIs('news-detail') || Request::routeIs('news') ? 'active' : '' }}" href="{{ route('news') }}">Artikel dan Berita</a>
