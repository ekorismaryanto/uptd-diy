<a href="/" class="list-group-item list-group-item-action list-group-custom {{ Request::path() == '/' ? 'active' : '' }}">Beranda</a>
<a href="/about" class="list-group-item list-group-item-action list-group-custom {{ Request::path() == 'about' ? 'active' : '' }}">Tentang Kami</a>
<a href="{{ route('keadaan-opt') }}" class="list-group-item list-group-item-action list-group-custom {{ Request::routeIs('keadaan-opt') ? 'active' : '' }}">Keadaan OPT</a>
<a href="{{ route('data-petugas') }}" class="list-group-item list-group-item-action list-group-custom {{ Request::routeIs('data-petugas') ? 'active' : '' }}">Data Petugas</a>
<a href="{{ route('news') }}" class="list-group-item list-group-item-action list-group-custom {{ Request::routeIs('news.*') ? 'active' : '' }}">Artikel Dan Berita</a>