@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item">
                        Input Data
                    </li>
                    <li class="breadcrumb-item active">
                        Laporan - tanaman pangan
                    </li>
                </ol>
                <h4>Selamat Datang</h4>
                <h6>Silahkan Melakukan inputan data pada form dibawah</h6>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 mb-4 header-title">Import Data Tanaman Pangan</h4>
                    <form action="{{ route('admin.import.store-import-tanaman-pangan') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Tipe File Import</label>
                                <select name="type" class="form-control" id="type" required>
                                    <option value="">Pilih Tipe File</option>
                                    <option value="1">OPT</option>
                                    <option value="2">OPT Banjir</option>
                                    <option value="3">OPT Kekeringan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="file" name="file" id="file" onchange="ValidateSize(this)" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <small>* Inputkan File CSV dan Excel</small>
                            <br>
                            <small>* Mohon inputkan data sesuai dengan yang sudah ditentukan. contoh format file bisa di download dibawah ini</small>
                            <br>
                            <a href="#" id="sample" class="btn sample btn-sm btn-outline-primary">
                                Sample File Import
                            </a>
                        </div>
                        <div class="mt-4 text-right">
                            <button class="btn btn-primary" type="submit">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container-fluid -->
@endsection

@section('script')
<script type="text/javascript">


    function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MiB
        if (FileSize > 1) {
            alert('Maksimum File Hanya 1 MB');
           $(file).val(''); //for clearing with Jquery
        } else {

        }
    }

    $('.sample').on('click', function (param) { 
        var a = $('#type').val();

        if (a == '') {
            alert('Pilih Tipe sample terlebih dahulu')
        }

    });

    $('#type').on('change',function () {
        var a = $(this).val();
        var oldUrl = $('.sample').attr("href"); // Get current url

        switch (a) {
            case '1':
            var newUrl = "{{ asset('assets/sample/sample-tanaman-pangan.xlsx') }}";
            $(".sample").prop("href", newUrl)
            break;

            case '2':
            var newUrl = "{{ asset('assets/sample/sample-tanaman-pangan-banjir.xlsx') }}";
            $(".sample").prop("href", newUrl)
            break;

            case '3':
            var newUrl = "{{ asset('assets/sample/sample-tanaman-pangan-kekeringan.xlsx') }}";
            $(".sample").prop("href", newUrl)
            break;
        
            default:
                break;
        }
    });


    //upload data
    $("#formExcel").on('submit',function(e){
        e.preventDefault();
        $.ajax({
          url:"{{ route('admin.import.store-import-perkebunan') }}",
          method:"POST",
          data:new FormData(this),
          processData:false,
          contentType:false,
          cache:false,
          beforeSend: function(){
                $('.waiting').show();
            },
          success:function(data){

            $('.waiting').html('success');

            // for (var i = 0; i < data.length; ++i) {
            //     var convert = JSON.parse(data);
            //     $("#contactTable").append(`<tr>
            //         <td>${convert[i].tahun}-${convert[i].bulan}</td>
            //         <td>${convert[i].prov}</td>
            //         <td>${convert[i].kab}</td>
            //         <td>${convert[i].kec}</td>
            //         <td>${convert[i].tanaman}</td>
            //         <td>${convert[i].opt}</td>
            //         <td>${convert[i].luaspertanaman_ha}</td>
            //         <td>${convert[i].ringan}</td>
            //         <td>${convert[i].berat}</td>
            //         <td>${convert[i].jumlah}</td>
            //         <td>${convert[i].apbn}</td>
            //         <td>${convert[i].apbd_i}</td>
            //         <td>${convert[i].apbd_ii}</td>
            //         <td>${convert[i].masya_rakat}</td>
            //         <td>${convert[i].cara_pengendalian}</td>
            //         <td>${convert[i].harga_rata_rata_ton_rp}</td>
            //     </tr>`);
            // }
            // var val = JSON.parse(val);
            // $.each(val, function(idx,data){
            //    alert(data);
            // })

            //   val = JSON.parse(data);
            //   $("#contactTable").append(`<tr>
            //             <td>${val.tahun}</td>
            //             <td>${val.bulan}</td>
            //             <td>${val.prov}</td>
            //         </tr>`);
          }
        })
    })
    // loadData();
    // //ambil data
    // function loadData(){
    //     $.ajax({
    //         url:"/home/loadData",
    //         success:function(val){
    //             val = JSON.parse(val);
    //             $.each(val,function(idx,data){
    //                 $("#contactTable").append(`<tr>
    //                     <td>${data.tahun}</td>
    //                     <td>${data.bulan}</td>
    //                     <td>${data.prov}</td>
    //                 </tr>
    //                     `);
    //             })
    //         }	
    //     })
    // }
</script>
@endsection
