@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item">
                        Input Data
                    </li>
                    <li class="breadcrumb-item active">
                        Preview Data
                    </li>
                </ol>
                <h4>Preview Data</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="form-group">
                        <table class="table table-bordered table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td width="50%">Nama</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>{{ optional($user->district)->name }}</td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td>{{ resolve(\App\Repositories\Entities\SubDistrict::class)->where('id', $data['sub_district_id'])->first()->name ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Periode</td>
                                    <td>{{ date_view($data['date']) }}</td>
                                </tr>
                                <tr>
                                    <td>Kategori</td>
                                    <td>{{ TypeCommodity::label($data['type_commodity']) }}</td>
                                </tr>
                                <tr>
                                    <td>Komoditas</td>
                                    <td>{{ resolve(\App\Repositories\Entities\Commodity::class)->where('id', $data['commodity_id'])->first()->name ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Luas Pertanaman (ha)</td>
                                    <td>{{ $data['planting_area'] }}</td>
                                </tr>
                                <tr>
                                    @php
                                        $opt = resolve(\App\Repositories\Entities\Opt::class)->whereIn('id', ($data['opts'] ?? null))->pluck('name')->toArray() ?? [];
                                    @endphp
                                    <td>OPT</td>
                                    <td>{{ implode(', ', $opt) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Intensitas Serangan</label>
                            <div class="form-group">
                                <table class="table table-bordered table-striped mb-0">
                                    <tbody>
                                        <tr>
                                            <td width="50%">Ringan</td>
                                            <td>{{ $data['itensity_attack_easy'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Berat</td>
                                            <td>{{ $data['itensity_attack_hard'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Pengendalian</label>
                            <div class="form-group">
                                <table class="table table-bordered table-striped mb-0">
                                    <tbody>
                                        <tr>
                                            <td width="50%">ABPN</td>
                                            <td>{{ $data['control_area_apbn'] }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">APBD I</td>
                                            <td>{{ $data['control_area_apbd_1'] }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">APBD II</td>
                                            <td>{{ $data['control_area_apbd_2'] }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">Masyarakat</td>
                                            <td>{{ $data['control_area_apbd_public'] }}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Cara Pengendalian</label>
                            <div class="form-group">
                                <table class="table table-bordered table-striped mb-0">
                                    <tbody>
                                        <tr>
                                            <td width="50%">
                                                @foreach ($data['control_areas'] as $item)
                                                    @php
                                                        $controlArea[] = ControlArea::label($item);
                                                    @endphp
                                                @endforeach
                                                {{ implode(', ', $controlArea) }}
                                            </td>
                                            <td>{{ $data['control_area_other'] ?? '-' }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Harga rata-rata / ton</label>
                            <div class="form-group">
                                <table class="table table-bordered table-striped mb-0">
                                    <tbody>
                                        <tr>
                                            <td>Rp. {{ $data['price_average'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="alert alert-danger l-h-23">
                                * mohon isi data dengan sebenar - benarnya dan cek kembali inputan data anda. Karena <span class="font-bold">data tidak bisa diedit</span> kembali. Terima Kasih.
                            </div>
                        </div>

                        <div class="col-12 text-right">
                            <a href="javascript:history.back()" class="btn btn-md-block btn-danger my-2">Kembali</a>
                            <a href="{{ route('admin.data.data-report.store') }}" onclick="return confirm('Anda Yakin Ingin Menyimpan data, data yang di input tidak bisa di edit kembali ?');"class="btn btn-md-block btn-primary my-2">Simpan Data</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container-fluid -->
@endsection

@push('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();
    jQuery('#datepicker-autoclose').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

</script>
@endpush
