@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item">
                        Input Data
                    </li>
                    <li class="breadcrumb-item active">
                        Laporan
                    </li>
                </ol>
                <h4>Selamat Datang</h4>
                <h6>Silahkan Melakukan inputan data pada form dibawah</h6>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 mb-4 header-title">LAPORAN SERANGAN ORGANISME PENGANGGU TUMBUHAN (OPT)</h4>
                    <form action="{{ route('admin.data.data-report.create-step-1') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label class="control-label font-bold label-green">Periode</label>
                                <div>
                                    <div class="input-group">
                                        <input type="text" class="form-control periode" autocomplete="off" required name="date" placeholder="Periode" id="datepicker-autoclose">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Kategori</label>
                                    <select class="form-control select2 type" required name="type_commodity" data-placeholder="Pilih Data">
                                        <option value=""></option>
                                        @foreach (TypeCommodity::labels() as $key => $item)
                                            <option value="{{ $key }}">{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group" id="tanaman-pangan" style="display: none">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Tipe OPT Tanaman Pangan</label>
                                    <select class="form-control select2" name="type_opt_pangan" data-placeholder="Pilih Data">
                                        <option value=""></option>
                                        <option value="1">Data OPT</option>
                                        <option value="2">Data OPT Banjir</option>
                                        <option value="3">Data Opt Kekeringan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group" id="hortikultura" style="display: none">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Tipe OPT Hortikultura</label>
                                    <select class="form-control select2 type-hortikultura" name="type_opt_hortikultura" data-placeholder="Pilih Data">
                                        <option value=""></option>
                                        <option value="1">Data OPT</option>
                                        <option value="2">Data OPT Banjir</option>
                                        <option value="3">Data Opt Kekeringan</option>
                                    </select>
                                </div>
                            </div>


                        </div>
                        <div id="perkebunan" style="display: none">
                            
                            <div class="form-group">
                                <label class="control-label font-bold label-green">Tipe OPT Perkebunan</label>
                                <select class="form-control select2 type-perkebunan" name="type_opt_perkebunan" data-placeholder="Pilih Data">
                                    <option value=""></option>
                                    <option value="1">Data OPT</option>
                                    <option value="2">Data OPT DPI</option>
                                    <option value="3">Data Opt LKK</option>
                                </select>
                            </div>

                            <textarea name="description" id="laporan_editor" rows="10" cols="100" required>
                                <h3><strong>I. PENDAHULUAN</strong></h3>
    
                                <h3><strong>II. JENIS KEGIATAN</strong></h3>
    
                                <h3><strong>III. SARANA DAN PRASARANA</strong></h3>
    
                                <p>1. Persediaan Pestisida</p>
    
                                <p>2. Alat Pengendalian</p>
    
                                <h3><strong>IV. PENGGUNAAN PESTISIDA KIMIA, NABATI DAN APH</strong></h3>
    
                                <p>1. Penggunaan Pestisida Kimia</p>
    
                                <p>2. Penggunaan Pestisida Nabati</p>
    
                                <p>3. Penggunaan Agensia Hayati</p>
    
                                <h3><strong>V. PERMASALAHAN DAN PEMECAHANNYA</strong></h3>
    
                                <h3><strong>VI. RENCANA KEGIATAN BULAN SELANJUTNYA</strong></h3>
    
                            </textarea>
                        </div>
                        <div class="mt-4 text-right">
                            <button type="submit" class="btn btn-primary">Simpan & Lanjutkan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<!--Morris Chart-->
<script src="{{ URL::asset('assets/plugins/morris/morris.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/raphael/raphael-min.js')}}"></script>

<script src="{{ URL::asset('assets/pages/dashboard.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(".select2").select2();

    $('.type').on('change', function(){
        var val = $(this).val();

        switch (val) {
            case '1':
                $('#tanaman-pangan').show();
                $('#hortikultura').hide();
                $('#perkebunan').hide();
                $(".type-pangan").prop('required',true);
                $(".type-hortikultura").prop('required',false);
                $(".type-perkebunan").prop('required',false);
                break;

            case '2':
                $('#tanaman-pangan').hide();
                $('#hortikultura').show();
                $('#perkebunan').hide();

                $(".type-pangan").prop('required',false);
                $(".type-perkebunan").prop('required',false);
                $(".type-hortikultura").prop('required',true);
                break;

            case '3':
                $('#tanaman-pangan').hide();
                $('#hortikultura').hide();
                $('#perkebunan').show();

                $(".type-perkebunan").prop('required',true);

                $(".type-pangan").prop('required',false);
                $(".type-hortikultura").prop('required',false);

                break;
        
            default:
                break;
        }

    });

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    CKEDITOR.replace( 'Resolution', {
        height: 400
    } );

    // Replace the <textarea id="editor1"> with a CKEditor 4
    // instance, using default configuration.
    CKEDITOR.replace('laporan_editor');

    CKEDITOR.instances.editor1.on('change', function () {
        var data = CKEDITOR.instances.laporan_editor.getData();
    })

    document.querySelector('.custom-file-input').addEventListener('change', function (e) {
        var fileName = document.getElementById("myInput").files[0].name;
        $('.custom-file-label').text(fileName);
    })

</script>
@endpush
