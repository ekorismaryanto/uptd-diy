@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Input Data
                    </li>
                </ol>
                <h4>Input Data</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="form-group">
                        <table class="table table-bordered table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>{{ optional($user->district)->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @for ($i = 0; $i < count($data['report_data']['sub_district_id']); $i++)
                        <div class="form-box issue-repeatable">
                            <div class="append-data">

                                <div class="issue-container issue-form-length">
                                    <div class="card border">
                                        <div class="card-header">
                                        Data OPT
                                        </div>
                                        <div class="card-body row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold label-green">Kecamatan</label>
                                                        <select class="form-control" required name="sub_district_id[]" data-placeholder="Pilih Kecamatan" disabled>
                                                            <option value=""></option>
                                                            @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $user->work_location)->get() as $item)
                                                                <option value="{{ $item->id }}" {{ $data['report_data']['sub_district_id'][$i] == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Komoditas</label>
                                                    
                                                    <select class="form-control select2 commodity" required name="commodity_id[]" data-category="adjustment" data-placeholder="Pilih Komoditas" disabled>
                                                        @php
                                                            $commodity = resolve(\App\Repositories\Entities\Commodity::class)->where('id', $data['report_data']['commodity_id'][$i])->first();
                                                        @endphp
                                                        <option value="{{ $commodity->id }}" selected>{{ $commodity->name }}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Periode TB</label>
                                                    <input type="text" class="form-control" required name="periode_tb[]" placeholder="Inputkan Umur (HST)" disabled value="{{ $data['report_data']['periode_tb'][$i] }}" >
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Desa</label>
                                                    <select class="form-control select2 desa" disabled name="village_id[]" data-placeholder="Pilih desa">
                                                        @php
                                                            $desa = resolve(\App\Repositories\Entities\Village::class)->where('id', $data['report_data']['village_id'][$i])->first();
                                                        @endphp
                                                        <option value="{{ $desa->id }}" selected>{{ $desa->name }}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Luas Pertanaman (ha)</label>
                                                    <input type="number" class="form-control" required name="planting_area[]" placeholder="Inputkan luas pertnamana (ha)"  value="{{ $data['report_data']['planting_area'][$i] }}"  disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">OPT</label>
                                                    <select class="form-control" id="opt-default" name="opts[1][]" required data-placeholder="Pilih OPT" disabled>
                                                        @php
                                                            $commodity = resolve(\App\Repositories\Entities\Opt::class)->where('id', $data['report_data']['opts'][$i])->first();
                                                        @endphp
                                                        <option value="{{ $commodity->id }}" selected>{{ $commodity->name }}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Sisa Serangan (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">R</td>
                                                            <td class="text-center">S</td>
                                                            <td class="text-center">B</td>
                                                            <td class="text-center">P</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="attact_area_r[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['attact_area_r'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="attact_area_s[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['attact_area_s'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="attact_area_b[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['attact_area_b'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="attact_area_p[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['attact_area_p'][$i]  }}">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Terkendali (Ha)</label>
                                                    <input class="form-control" required name="under_control[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['under_control'][$i]  }}">
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Tambah Serangan (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">R</td>
                                                            <td class="text-center">S</td>
                                                            <td class="text-center">B</td>
                                                            <td class="text-center">P</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="added_area_r[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['added_area_r'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="added_area_s[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['added_area_s'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="added_area_b[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['added_area_b'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="added_area_p[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['added_area_p'][$i]  }}">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Keadaan Serangan (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">R</td>
                                                            <td class="text-center">S</td>
                                                            <td class="text-center">B</td>
                                                            <td class="text-center">P</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="state_area_r[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['state_area_r'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="state_area_s[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['state_area_s'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="state_area_b[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['state_area_b'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="state_area_p[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['state_area_p'][$i]  }}">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Keadaan Pengendalian (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">Pem</td>
                                                            <td class="text-center">Pest</td>
                                                            <td class="text-center">CL</td>
                                                            <td class="text-center">APH</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="control_area_pem[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['control_area_pem'][$i]  }}" >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="control_area_pest[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['control_area_pest'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="control_area_cl[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['control_area_cl'][$i]  }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="control_area_aph[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['control_area_aph'][$i]  }}">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Tanaman Terancam (Ha)</label>
                                                <input class="form-control" required name="threatened_plant[]" min="0" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['threatened_plant'][$i]  }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                    <div class="col-12">
                        <div class="alert alert-danger l-h-23">
                            * mohon isi data dengan sebenar - benarnya dan cek kembali inputan data anda. Karena <span class="font-bold">data tidak bisa diedit</span> kembali. Terima Kasih.
                        </div>
                    </div>

                    <div class="col-12 text-right">
                        <a href="{{ route('admin.data.data-report.store') }}" onclick="return confirm('Anda Yakin Ingin Menyimpan data, data yang di input tidak bisa di edit kembali ?');"class="btn btn-md-block btn-primary my-2">Simpan Data</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>

    $("input").prop("disabled", true);
    $("select").prop("disabled", true);

    $(".select2").select2();

    $('.pengendalian').select2();

    $('.pengendalian').on('change', function (param) {
        var value = $(this).val();
        var cek  = jQuery.inArray( "5", value );
        if (cek == -1) {
            $('.pengendalian-other').hide();
        }else{
            $('.pengendalian-other').show();
        }
    })

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    $('.commodity').select2({
        placeholder: 'Pilih Komoditas',
        ajax: {
            url: '{{ route('admin.json.commodity') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 3,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                var $$select = $('.commodity');
                var selectedValues = Array.from($$select).map(s => parseInt(s.value));

                var result = {
                    "results" : data['results'].filter(a => !selectedValues.includes(a.id))
                };

                return result;
            },
            cache: true
        }
    })

    $('#opt-default').select2({
        placeholder: 'Pilih OPT',
        ajax: {
            url: '{{ route('admin.json.opt') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    })
</script>
@endpush
