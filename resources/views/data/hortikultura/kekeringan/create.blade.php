@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Input Data
                    </li>
                </ol>
                <h4>Input Data OPT Kekeringan</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="form-group">
                        <table class="table table-bordered table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>{{ optional($user->district)->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <form action="{{ route('admin.data.data-report.create-final') }}" method="POST">
                        @csrf
                        <div class="form-box issue-repeatable">
                            <div class="append-data">

                                <div class="issue-container issue-form-length">
                                    <div class="card border">
                                        <div class="card-header">
                                           Data OPT Kekeringan
                                        </div>
                                        <div class="card-body row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Kecamatan</label>
                                                    <select class="form-control kecamatan" required name="sub_district_id[]" data-placeholder="Pilih Kecamatan">
                                                        <option value=""></option>
                                                        @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $user->work_location)->get() as $item)
                                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Komoditas</label>
                                                    <select class="form-control select2 commodity" required name="commodity_id[]" data-category="adjustment" data-placeholder="Pilih Komoditas">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Umur (HST)</label>
                                                    <input type="number" class="form-control" required name="umur[]" placeholder="Inputkan Umur (HST)">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Periode TB</label>
                                                    <input type="text" class="form-control" required name="periode_tb[]" placeholder="Inputkan Umur (HST)">
                                                </div>
                                                
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Desa</label>
                                                    <select class="form-control select2 desa" required name="village_id[]" data-placeholder="Pilih desa">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Varietas</label>
                                                    <input type="text" class="form-control" required name="varietas[]" placeholder="Inputkan Varietas">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Luas Waspada (ha)</label>
                                                    <input type="number" class="form-control" required name="broadly_alert[]" placeholder="Inputkan luas Waspada (ha)">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Luas Pertanaman (ha)</label>
                                                    <input type="number" class="form-control" required name="planting_area[]" placeholder="Inputkan luas pertnamana (ha)">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Sisa Periode sebelumnya/Perubahan Kriteria</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">Ringan</td>
                                                            <td class="text-center">Sedang</td>
                                                            <td class="text-center">Berat</td>
                                                            <td class="text-center">Puso</td>
                                                            <td class="text-center">Pulih</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="previous_periode_low[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="previous_periode_mid[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="previous_periode_hight[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="previous_periode_puso[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="previous_periode_recover[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Tambah pada Periode Laporan (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">Ringan</td>
                                                            <td class="text-center">Sedang</td>
                                                            <td class="text-center">Berat</td>
                                                            <td class="text-center">Puso</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="area_added_periode_low[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="area_added_periode_mid[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="area_added_periode_hight[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="area_added_periode_puso[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Keadaan pada Periode Laporan (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">Ringan</td>
                                                            <td class="text-center">Sedang</td>
                                                            <td class="text-center">Berat</td>
                                                            <td class="text-center">Puso</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="area_state_periode_low[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="area_state_periode_mid[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="area_state_periode_hight[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="area_state_periode_puso[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Penanganan</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">Upaya</td>
                                                            <td class="text-center">Jumlah</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="handling_effort[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="handling_effort_total[]" min="0" type="number" placeholder="contoh (1,2)">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Titik Koordinat Puso (Desa)</label>
                                                    <input class="form-control" required name="coordinate[]" type="text" placeholder="Input Koordinate">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Keterangan</label>
                                                    <input class="form-control" required name="description[]" type="text" placeholder="Input Deskripsi">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <button type="btn" class="btn btn-primary btn-sm repeat-add">Tambah Pelaporan</button>
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <a href="javascript:history.back()" class="btn btn-danger">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan dan Lanjutkan</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();

    $('.pengendalian').select2();

    $('.pengendalian').on('change', function (param) {
        var value = $(this).val();
        var cek  = jQuery.inArray( "5", value );
        if (cek == -1) {
            $('.pengendalian-other').hide();
        }else{
            $('.pengendalian-other').show();
        }
    })

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    $('.commodity').select2({
        placeholder: 'Pilih Komoditas',
        ajax: {
            url: '{{ route('admin.json.commodity') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 2,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                var $$select = $('.commodity');
                var selectedValues = Array.from($$select).map(s => parseInt(s.value));

                var result = {
                    "results" : data['results'].filter(a => !selectedValues.includes(a.id))
                };

                return result;
            },
            cache: true
        }
    })

    $('#opt-default').select2({
        placeholder: 'Pilih OPT',
        ajax: {
            url: '{{ route('admin.json.opt') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    });

    $('.kecamatan').select2();

    $('.kecamatan').on('change', function (param) { 
        var kec = $(this).val();

        $('.desa').select2({
            placeholder: 'Pilih desa',
            ajax: {
                url: '{{ route('admin.json.village') }}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        sub_district_id: kec,
                    }
                    return query;
                },
                delay: 250,
                processResults: function (data) {
                    return data;
                },
                cache: true
            }
        });
    });

    $('body').on('click', '.repeat-add', function (e) {
            e.preventDefault();
            let formParent = $('.issue-form-length').length;
            var templateIssue = `
            <div class="card border issue-form issue-form-length">
                <div class="card-header">
                    <a href="#" class="repeat-remove">
                        <i class="fa fa-minus-square"></i>
                    </a>
                </div>
                <div class="card-body row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="control-label font-bold label-green">Kecamatan</label>
                            <select class="form-control kecamatan-repeate" required name="sub_district_id[]" data-placeholder="Pilih Kecamatan">
                                <option value=""></option>
                                @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $user->work_location)->get() as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label font-bold label-green">Komoditas</label>
                            <select class="form-control select2 commodity-repeate" required name="commodity_id[]" data-category="adjustment" data-placeholder="Pilih Komoditas">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-uppercase font-bold label-green">Umur (HST)</label>
                            <input type="number" class="form-control" required name="umur[]" placeholder="Inputkan Umur (HST)">
                        </div>
                        <div class="form-group">
                            <label class="control-label text-uppercase font-bold label-green">Periode TB</label>
                            <input type="text" class="form-control" required name="periode_tb[]" placeholder="Inputkan Umur (HST)">
                        </div>
                        
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="control-label font-bold label-green">Desa</label>
                            <select class="form-control select2 desa-repeate" required name="village_id[]" data-placeholder="Pilih desa">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-uppercase font-bold label-green">Varietas</label>
                            <input type="text" class="form-control" required name="varietas[]" placeholder="Inputkan Varietas">
                        </div>
                        <div class="form-group">
                            <label class="control-label text-uppercase font-bold label-green">Luas Waspada (ha)</label>
                            <input type="number" class="form-control" required name="broadly_alert[]" placeholder="Inputkan luas Waspada (ha)">
                        </div>
                        <div class="form-group">
                            <label class="control-label text-uppercase font-bold label-green">Luas Pertanaman (ha)</label>
                            <input type="number" class="form-control" required name="planting_area[]" placeholder="Inputkan luas pertnamana (ha)">
                        </div>
                    </div>
                    <div class="col-lg-12 form-group">
                        <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Sisa Periode sebelumnya/Perubahan Kriteria</label>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr>
                                    <td class="text-center">Ringan</td>
                                    <td class="text-center">Sedang</td>
                                    <td class="text-center">Berat</td>
                                    <td class="text-center">Puso</td>
                                    <td class="text-center">Pulih</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="form-control" required name="previous_periode_low[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="previous_periode_mid[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="previous_periode_hight[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="previous_periode_puso[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="previous_periode_recover[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12 form-group">
                        <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Tambah pada Periode Laporan (Ha)</label>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr>
                                    <td class="text-center">Ringan</td>
                                    <td class="text-center">Sedang</td>
                                    <td class="text-center">Berat</td>
                                    <td class="text-center">Puso</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="form-control" required name="area_added_periode_low[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="area_added_periode_mid[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="area_added_periode_hight[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="area_added_periode_puso[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12 form-group">
                        <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Keadaan pada Periode Laporan (Ha)</label>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr>
                                    <td class="text-center">Ringan</td>
                                    <td class="text-center">Sedang</td>
                                    <td class="text-center">Berat</td>
                                    <td class="text-center">Puso</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="form-control" required name="area_state_periode_low[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="area_state_periode_mid[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="area_state_periode_hight[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="area_state_periode_puso[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12 form-group">
                        <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Penanganan</label>
                        <div class="form-group">
                            <table style="width: 100%">
                                <tr>
                                    <td class="text-center">Upaya</td>
                                    <td class="text-center">Jumlah</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="form-control" required name="handling_effort[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                    <td>
                                        <input class="form-control" required name="handling_effort_total[]" min="0" type="number" placeholder="contoh (1,2)">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Titik Koordinat Puso (Desa)</label>
                            <input class="form-control" required name="coordinate[]" type="text" placeholder="Input Koordinate">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Keterangan</label>
                            <input class="form-control" required name="description[]" type="text" placeholder="Input Deskripsi">
                        </div>
                    </div>
                </div>
            </div>`;
            $(this).closest('.issue-repeatable').find('.append-data').append(templateIssue);
            
            $('.select2-repeate').select2();

            $('.commodity-repeate').select2({
                placeholder: 'Pilih Komoditas',
                ajax: {
                    url: '{{ route('admin.json.commodity') }}',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            q: params.term,
                            type: 2,
                            page: params.page
                        }
                        return query;
                    },
                    delay: 250,
                    processResults: function (data) {
                        var $$select = $('.commodity');
                        var selectedValues = Array.from($$select).map(s => parseInt(s.value));

                        var result = {
                            "results" : data['results'].filter(a => !selectedValues.includes(a.id))
                        };

                        return result;
                    },
                    cache: true
                }
            })

            $('.opt-repeate').select2({
                placeholder: 'Pilih OPT',
                ajax: {
                    url: '{{ route('admin.json.opt') }}',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            q: params.term,
                            page: params.page
                        }
                        return query;
                    },
                    delay: 250,
                    processResults: function (data) {
                        return data;
                    },
                    cache: true
                }
            });

            $('.kecamatan-repeate').select2();

            $('.kecamatan-repeate').on('change', function (param) { 
                var kec = $(this).val();

                $('.desa-repeate').select2({
                    placeholder: 'Pilih desa',
                    ajax: {
                        url: '{{ route('admin.json.village') }}',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                q: params.term,
                                page: params.page,
                                sub_district_id: kec
                            }
                            return query;
                        },
                        delay: 250,
                        processResults: function (data) {
                            return data;
                        },
                        cache: true
                    }
                });
            });


		});

		$('body').on('click', '.repeat-remove', function (e) {
			e.preventDefault();
			$(this).parent().parent('.issue-form').remove();
		});
    
</script>
@endpush
