@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    $user = logged_in_user();
@endphp


@section('content')
{{dd(Session::get('form_1'))}}
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Input Data
                    </li>
                </ol>
                <h4>Input Data</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="form-group">
                        <table class="table table-bordered table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>{{ optional($user->district)->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <form action="{{ route('admin.data.data-report.create-final') }}" method="POST">
                        @csrf
                        <div class="form-box issue-repeatable">
                            <div class="issue-container">
                                <div class="card border">
                                    <div class="card-header">
                                        <a href="#" class="repeat-add issue-add">
                                            <i class="fa fa-plus-square"></i>
                                        </a>
                                    </div>
                                    <div class="card-body row">
                                        <div class="col-lg-6">
                                    
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Kecamatan</label>
                                                    <select class="form-control select2" required name="sub_district_id" data-placeholder="Pilih Kecamatan">
                                                        <option value=""></option>
                                                        @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $user->work_location)->get() as $item)
                                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                    
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Komoditas</label>
                                                    <select class="form-control select2 commodity" required name="commodity_id" data-placeholder="Pilih Komoditas">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Luas Pertanaman (ha)</label>
                                                    <input type="number" class="form-control" required name="planting_area" placeholder="Inputkan luas pertnamana (ha)">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">OPT</label>
                                                    <select class="form-control" id="opt-default" name="opts[]" required multiple data-placeholder="Pilih OPT">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 form-group">
                                            <label
                                                class="control-label text-uppercase font-14 mb-3 font-bold label-green">Intensitas
                                                Serangan</label>
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">Ringan</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" required name="itensity_attack_easy" min="0" type="number"
                                                        placeholder="Inputkan Intensitas Serangan Ringan">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">Berat</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" required name="itensity_attack_hard" min="0" type="number"
                                                        placeholder="Inputkan Intensitas Serangan Berat">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 form-group">
                                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas
                                                Pengendalian</label>
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">APBN</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" required name="control_area_apbn" min="0" type="number" placeholder="Inputkan APBN">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">APBD I</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" required name="control_area_apbd_1" min="0" type="number" placeholder="Inputkan APBD I">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">APBD II</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" required name="control_area_apbd_2" min="0" type="number" placeholder="Inputkan APBD II">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">Masyarakat</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" required min="0" name="control_area_apbd_public" type="number" placeholder="Inputkan Masyarakat">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <div class="form-group">
                                                <label class="control-label text-uppercase font-bold label-green">Cara Pengendalian</label>
                                                <select class="form-control select2 pengendalian" required name="control_areas[]" multiple data-placeholder="Pilih Cara Pengendalian (KT, M, B, K, Lain-lain)">
                                                    <option value=""></option>
                                                    <option value="1">KT</option>
                                                    <option value="2">M</option>
                                                    <option value="3">B</option>
                                                    <option value="4">K</option>
                                                    <option value="5">Lain-lain</option>
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div class="col-lg-6 form-group">
                                            <div class="form-group">
                                                <label class="control-label text-uppercase font-bold label-green">Harga rata-rata /
                                                    ton</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="inputGroup-sizing-sm" style="color: grey">Rp</span>
                                                    </div>
                                                    <input  type="number" class="form-control" name="price_average" placeholder="Inputkan harga rata-rata / ton" aria-label="Inputkan harga rata-rata / ton" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 form-group pengendalian-other" style="display: none">
                                            <div class="form-group">
                                                <label class="control-label text-uppercase font-bold label-green">Inputkan Pengendalian Lain</label>
                                                <input type="text" class="form-control" name="control_area_other" placeholder="Inputkan Pengendalian Lain">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <a href="javascript:history.back()" class="btn btn-danger">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan dan Lanjutkan</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();

    $('.pengendalian').select2();

    $('.pengendalian').on('change', function (param) {
        var value = $(this).val();
        var cek  = jQuery.inArray( "5", value );
        if (cek == -1) {
            $('.pengendalian-other').hide();
        }else{
            $('.pengendalian-other').show();
        }
    })

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    $('.commodity').select2({
        placeholder: 'Pilih Komoditas',
        ajax: {
            url: '{{ route('admin.json.commodity') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 3,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    })

    $('#opt-default').select2({
        placeholder: 'Pilih OPT',
        ajax: {
            url: '{{ route('admin.json.opt') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    })

    $('body').on('click', '.issue-add', function (e) {
            e.preventDefault();
            var templateIssue = `
            <div class="card border">
                <div class="card-header">
                    <a href="#" class="repeat-add issue-add">
                        <i class="fa fa-plus-square"></i>
                    </a>
                </div>
                <div class="card-body row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="control-label font-bold label-green">Kecamatan</label>
                                <select class="form-control select2" required name="sub_district_id" data-placeholder="Pilih Kecamatan">
                                    <option value=""></option>
                                    @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $user->work_location)->get() as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label class="control-label font-bold label-green">Komoditas</label>
                                <select class="form-control select2 commodity" required name="commodity_id" data-placeholder="Pilih Komoditas">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label class="control-label text-uppercase font-bold label-green">Luas Pertanaman (ha)</label>
                                <input type="number" class="form-control" required name="planting_area" placeholder="Inputkan luas pertnamana (ha)">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="control-label font-bold label-green">OPT</label>
                                <select class="form-control opt" name="opts[]" required id="opt" multiple data-placeholder="Pilih OPT">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 form-group">
                        <label
                            class="control-label text-uppercase font-14 mb-3 font-bold label-green">Intensitas
                            Serangan</label>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Ringan</label>
                            <div class="col-sm-10">
                                <input class="form-control" required name="itensity_attack_easy" min="0" type="number"
                                    placeholder="Inputkan Intensitas Serangan Ringan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Berat</label>
                            <div class="col-sm-10">
                                <input class="form-control" required name="itensity_attack_hard" min="0" type="number"
                                    placeholder="Inputkan Intensitas Serangan Berat">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 form-group">
                        <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas
                            Pengendalian</label>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">APBN</label>
                            <div class="col-sm-10">
                                <input class="form-control" required name="control_area_apbn" min="0" type="number" placeholder="Inputkan APBN">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">APBD I</label>
                            <div class="col-sm-10">
                                <input class="form-control" required name="control_area_apbd_1" min="0" type="number" placeholder="Inputkan APBD I">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">APBD II</label>
                            <div class="col-sm-10">
                                <input class="form-control" required name="control_area_apbd_2" min="0" type="number" placeholder="Inputkan APBD II">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Masyarakat</label>
                            <div class="col-sm-10">
                                <input class="form-control" required min="0" name="control_area_apbd_public" type="number" placeholder="Inputkan Masyarakat">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 form-group">
                        <div class="form-group">
                            <label class="control-label text-uppercase font-bold label-green">Cara Pengendalian</label>
                            <select class="form-control select2 pengendalian" required name="control_areas[]" multiple data-placeholder="Pilih Cara Pengendalian (KT, M, B, K, Lain-lain)">
                                <option value=""></option>
                                <option value="1">KT</option>
                                <option value="2">M</option>
                                <option value="3">B</option>
                                <option value="4">K</option>
                                <option value="5">Lain-lain</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-6 form-group">
                        <div class="form-group">
                            <label class="control-label text-uppercase font-bold label-green">Harga rata-rata /
                                ton</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm" style="color: grey">Rp</span>
                                </div>
                                <input  type="number" class="form-control" name="price_average" placeholder="Inputkan harga rata-rata / ton" aria-label="Inputkan harga rata-rata / ton" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 form-group pengendalian-other" style="display: none">
                        <div class="form-group">
                            <label class="control-label text-uppercase font-bold label-green">Inputkan Pengendalian Lain</label>
                            <input type="text" class="form-control" name="control_area_other" placeholder="Inputkan Pengendalian Lain">
                        </div>
                    </div>
                </div>
            </div>`;
			$(this).closest('.issue-container').append(templateIssue);


            $('.type').select2().on('change', function(){
                var type = 3;
                $('.commodity').val('');
                $('.commodity').select2({
                    placeholder: 'Pilih Komoditas',
                    ajax: {
                        url: '{{ route('admin.json.commodity') }}',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                q: params.term,
                                type: type,
                                page: params.page
                            }
                            return query;
                        },
                        delay: 250,
                        processResults: function (data) {
                            return data;
                        },
                        cache: true
                    }
                })
            });

            $('.opt').select2({
                placeholder: 'Pilih OPT',
                ajax: {
                    url: '{{ route('admin.json.opt') }}',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            q: params.term,
                            page: params.page
                        }
                        return query;
                    },
                    delay: 250,
                    processResults: function (data) {
                        return data;
                    },
                    cache: true
                }
            })

		});

		$('body').on('click', '.issue-remove', function (e) {
			e.preventDefault();
			$(this).parent().parent('.issue-form').remove();
			data--;
		});
    
</script>
@endpush
