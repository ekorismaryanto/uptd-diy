@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item">
                        Input Data
                    </li>
                    <li class="breadcrumb-item active">
                        Preview Data
                    </li>
                </ol>
                <h4>Preview Data</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="form-group">
                        <table class="table table-bordered table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td width="50%">Nama</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Periode</td>
                                    <td>{{ date_view($data['date']) }}</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Perkebunan</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>{{ optional($user->district)->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        @php
                            $araes = array_values($data['report_data']['control_areas']);
                        @endphp
                        @for ($i = 0; $i < count($data['report_data']['sub_district_id']); $i++)
                            <div class="col-lg-12">
                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Data Pelaporan - Kecamatan {{ resolve(\App\Repositories\Entities\SubDistrict::class)->where('id', $data['report_data']['sub_district_id'][$i])->first()->name ?? '-' }}</label>
                                <div class="form-group">
                                    <table class="table table-bordered table-striped mb-0">
                                        <tbody>
                                            <tr>
                                                <td width="50%">Komoditas</td>
                                                <td>{{ resolve(\App\Repositories\Entities\Commodity::class)->where('id', $data['report_data']['commodity_id'][$i])->first()->name ?? '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Luas Pertanaman (ha)</td>
                                                <td>{{ $data['report_data']['planting_area'][$i] }}</td>
                                            </tr>
                                            <tr>
                                                @php
                                                    $opt = resolve(\App\Repositories\Entities\Opt::class)->where('id', ($data['report_data']['opts'][$i] ?? null))->first() ?? [];
                                                @endphp
                                                <td>OPT</td>
                                                <td>{{ $opt->name }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><h5>Intensitas Serangan</h5></td>
                                            </tr>
                                            <tr>
                                                <td width="50%">Ringan</td>
                                                <td>{{ $data['report_data']['itensity_attack_easy'][$i] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Berat</td>
                                                <td>{{ $data['report_data']['itensity_attack_hard'][$i] }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><h5>Luas Pengendalian</h5></td>
                                            </tr>
                                            <tr>
                                                <td width="50%">ABPN</td>
                                                <td>{{ $data['report_data']['control_area_apbn'][$i] }}</td>
                                            </tr>
                                            <tr>
                                                <td width="50%">APBD I</td>
                                                <td>{{ $data['report_data']['control_area_apbd_1'][$i] }}</td>
                                            </tr>
                                            <tr>
                                                <td width="50%">APBD II</td>
                                                <td>{{ $data['report_data']['control_area_apbd_2'][$i] }}</td>
                                            </tr>
                                            <tr>
                                                <td width="50%">Masyarakat</td>
                                                <td>{{ $data['report_data']['control_area_apbd_public'][$i] }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><h5>Cara Pengendalian</h5></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" colspan="2">
                                                   
                                                    @foreach ($araes[$i] as $row)
                                                        @php
                                                            $controlArea[] = ControlArea::label($row);
                                                        @endphp
                                                    @endforeach
                                                    {{ implode(', ', $controlArea) }}.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="50%">Harga rata-rata / ton</td>
                                                <td>Rp. {{ $data['report_data']['price_average'][$i] }}</td>
                                            </tr>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endfor
                        <div class="col-12">
                            <div class="alert alert-danger l-h-23">
                                * mohon isi data dengan sebenar - benarnya dan cek kembali inputan data anda. Karena <span class="font-bold">data tidak bisa diedit</span> kembali. Terima Kasih.
                            </div>
                        </div>

                        <div class="col-12 text-right">
                            <a href="{{ route('admin.data.data-report.store') }}" onclick="return confirm('Anda Yakin Ingin Menyimpan data, data yang di input tidak bisa di edit kembali ?');"class="btn btn-md-block btn-primary my-2">Simpan Data</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container-fluid -->
@endsection

@push('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();
    jQuery('#datepicker-autoclose').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

</script>
@endpush
