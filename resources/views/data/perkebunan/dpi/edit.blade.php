@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Repositories\Entities\Commodity;
    use \App\Repositories\Entities\Opt;
    use \App\Repositories\Entities\Village;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Input Data
                    </li>
                </ol>
                <h4>Input Data OPT Banjir</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    @php
                        $data['report_data'] = $opt->toArray();
                    @endphp
                    <form action="{{ route('admin.report.perkebunan.dpi.update', $opt->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <table class="table table-bordered table-striped mb-0">
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>{{ $opt->user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td>{{ $opt->subDistrict->district->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td>
                                            <div>
                                                <div class="input-group">
                                                    <input type="text" class="form-control periode" autocomplete="off" required name="date" placeholder="Periode" id="datepicker-autoclose" value="{{ date('Y-m', strtotime($opt->periode)) }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-box issue-repeatable">
                            <div class="append-data">

                                <div class="issue-container issue-form-length">
                                    <div class="card border">
                                        <div class="card-header">
                                           Data OPT Banjir
                                        </div>
                                        <div class="card-body row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Kecamatan</label>
                                                    <select class="form-control kecamatan" required name="sub_district_id[]" data-placeholder="Pilih Kecamatan">
                                                        <option value=""></option>
                                                        @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $opt->subDistrict->district->id)->get() as $item)
                                                            <option value="{{ $item->id }}" {{ $opt->sub_district_id == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Komoditas</label>
                                                    <select class="form-control select2 commodity" required name="commodity_id[]" data-category="adjustment" data-placeholder="Pilih Komoditas">
                                                        @php
                                                            $commodity = Commodity::find($data['report_data']['commodity_id']);
                                                        @endphp
                                                        <option value="{{ $commodity->id }}" selected>{{ $commodity->name }}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Luas (Ha)</label>
                                                    <input type="number" class="form-control" required name="planting_area[]" placeholder="Inputkan luas (ha)" value="{{ $data['report_data']['planting_area'] }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Umur (HST)</label>
                                                    <input type="number" class="form-control" required name="age[]" placeholder="Inputkan Umur (HST)" value="{{ $data['report_data']['age'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Satuan Umur</label>
                                                    <input type="number" class="form-control" required name="age_unit[]" placeholder="Inputkan satuan Umur" value="{{ $data['report_data']['age_unit'] }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Kekeringan</label>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Kering Terkena (ha)</label>
                                                    <input type="number" class="form-control" required name="dry_exposed[]" placeholder="Inputkan Kering Terkena (ha)" value="{{ $data['report_data']['dry_exposed'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Kering Ringan (ha)</label>
                                                    <input type="number" class="form-control" required name="dry_lightly[]" placeholder="Inputkan Kering Ringan (ha)" value="{{ $data['report_data']['dry_lightly'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Kering Penanganan</label>
                                                    <input type="text" class="form-control" required name="dry_handling[]" placeholder="Inputkan Kering Penanganan" value="{{ $data['report_data']['dry_handling'] }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Banjir</label>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Banjir Terkena (ha)</label>
                                                    <input type="number" class="form-control" required name="flood_hit[]" placeholder="Inputkan Banjir Terkena (ha)" value="{{ $data['report_data']['flood_hit'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Banjir Ringan (ha)</label>
                                                    <input type="number" class="form-control" required name="light_flood[]" placeholder="Inputkan Banjir Ringan (ha)" value="{{ $data['report_data']['light_flood'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Banjir Penanganan</label>
                                                    <input type="text" class="form-control" required name="flood_handling[]" placeholder="Inputkan Banjir Penanganan" value="{{ $data['report_data']['flood_handling'] }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Kebakaran</label>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Kebakaran Terkena (ha)</label>
                                                    <input type="number" class="form-control" required name="fire_hit[]" placeholder="Inputkan Kebakaran Terkena (ha)" value="{{ $data['report_data']['fire_hit'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Kebakaran Ringan (ha)</label>
                                                    <input type="number" class="form-control" required name="light_fire[]" placeholder="Inputkan Kebakaran Ringan (ha)" value="{{ $data['report_data']['light_fire'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Kebakaran Penanganan</label>
                                                    <input type="text" class="form-control" required name="fire_handling[]" placeholder="Inputkan Kebakaran Penanganan" value="{{ $data['report_data']['fire_handling'] }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-primary"> Simpan</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();

    $('.pengendalian').select2();

    $('.pengendalian').on('change', function (param) {
        var value = $(this).val();
        var cek  = jQuery.inArray( "5", value );
        if (cek == -1) {
            $('.pengendalian-other').hide();
        }else{
            $('.pengendalian-other').show();
        }
    })

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    $('.commodity').select2({
        placeholder: 'Pilih Komoditas',
        ajax: {
            url: '{{ route('admin.json.commodity') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 2,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                var $$select = $('.commodity');
                var selectedValues = Array.from($$select).map(s => parseInt(s.value));

                var result = {
                    "results" : data['results'].filter(a => !selectedValues.includes(a.id))
                };

                return result;
            },
            cache: true
        }
    })

    $('#opt-default').select2({
        placeholder: 'Pilih OPT',
        ajax: {
            url: '{{ route('admin.json.opt') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    });

    $('.kecamatan').select2();

    $('.kecamatan').on('change', function (param) { 
        var kec = $(this).val();

        $('.desa').select2({
            placeholder: 'Pilih desa',
            ajax: {
                url: '{{ route('admin.json.village') }}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        sub_district_id: kec,
                    }
                    return query;
                },
                delay: 250,
                processResults: function (data) {
                    return data;
                },
                cache: true
            }
        });
    });
</script>
@endpush
