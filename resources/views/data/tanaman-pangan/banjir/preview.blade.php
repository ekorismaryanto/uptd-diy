@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Repositories\Entities\Commodity;
    use \App\Repositories\Entities\Opt;
    use \App\Repositories\Entities\Village;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Input Data
                    </li>
                </ol>
                <h4>Input Data OPT Banjir</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="form-group">
                        <table class="table table-bordered table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>{{ optional($user->district)->name }}</td>
                                </tr>
                                <tr>
                                    <td>Jenis</td>
                                    <td>Tanaman Pangan - Banjir</td>
                                </tr>
                                <tr>
                                    <td>Periode</td>
                                    <td>{{ $data['date'] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <form action="{{ route('admin.data.data-report.create-final') }}" method="POST">
                        @csrf
                        <div class="form-box issue-repeatable">
                            <div class="append-data">
                                @for ($i = 0; $i < count($data['report_data']['sub_district_id']); $i++)
                                    <div class="issue-container issue-form-length">
                                        <div class="card border">
                                            <div class="card-header">
                                            Data OPT Banjir
                                            </div>
                                            <div class="card-body row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold label-green">Kecamatan</label>
                                                        <select class="form-control kecamatan" required name="sub_district_id[]" data-placeholder="Pilih Kecamatan">
                                                            <option value=""></option>
                                                            @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $user->work_location)->get() as $item)
                                                                <option value="{{ $item->id }}" {{ $data['report_data']['sub_district_id'][$i] == $item->id ? 'selected' : '' }}  >{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label font-bold label-green">Komoditas</label>
                                                        <select class="form-control select2 commodity" required name="commodity_id[]" data-category="adjustment" data-placeholder="Pilih Komoditas">
                                                            @php
                                                                $commodity = Commodity::find($data['report_data']['commodity_id'][$i]);
                                                            @endphp
                                                            <option value="{{ $commodity->id }}" selected>{{ $commodity->name }}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label text-uppercase font-bold label-green">Umur (HST)</label>
                                                        <input type="number" class="form-control" required name="umur[]" placeholder="Inputkan Umur (HST)" value="{{ $data['report_data']['umur'][$i] }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label text-uppercase font-bold label-green">Periode TB</label>
                                                        <input type="text" class="form-control" required name="periode_tb[]" placeholder="Inputkan Umur (HST)" value="{{ $data['report_data']['periode_tb'][$i] }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold label-green">Desa</label>
                                                        <select class="form-control select2 desa" required name="village_id[]" data-placeholder="Pilih desa">
                                                            @php
                                                                $village = Village::find($data['report_data']['village_id'][$i]);
                                                            @endphp
                                                            <option value="{{ $village->id  }}" selected>{{ $village->name }}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label text-uppercase font-bold label-green">Varietas</label>
                                                        <input type="text" class="form-control" required name="varietas[]" placeholder="Inputkan Varietas"  value="{{ $data['report_data']['varietas'][$i] }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label text-uppercase font-bold label-green">Luas Pertanaman (ha)</label>
                                                        <input type="number" class="form-control" required name="planting_area[]" placeholder="Inputkan luas Pertanaman (ha)" value="{{ $data['report_data']['planting_area'][$i] }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label text-uppercase font-bold label-green">Luas Waspada (ha)</label>
                                                        <input type="number" class="form-control" required name="broadly_alert[]" placeholder="Inputkan luas Waspada (ha)" value="{{ $data['report_data']['broadly_alert'][$i] }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 form-group">
                                                    <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Sisa Periode sebelumnya/Perubahan Kriteria</label>
                                                    <div class="form-group">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <th class="text-center" colspan="2">Surut</th>
                                                                <th class="text-center" colspan="2">Puso 3)</th>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-center">Luas (Ha)</td>
                                                                <td class="text-center">Ket (Periode)</td>
                                                                <td class="text-center">Luas (Ha)</td>
                                                                <td class="text-center">Ket (Periode)</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input class="form-control" required name="previous_month_rest_receding_area[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['previous_month_rest_receding_area'][$i] }}">
                                                                </td>
                                                                <td>
                                                                    <input class="form-control" required name="previous_month_rest_receding_description[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['previous_month_rest_receding_description'][$i] }}">
                                                                </td>
                                                                <td>
                                                                    <input class="form-control" required name="previous_month_rest_puso_area[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['previous_month_rest_puso_area'][$i] }}">
                                                                </td>
                                                                <td>
                                                                    <input class="form-control" required name="previous_month_rest_puso_description[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['previous_month_rest_puso_description'][$i] }}">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 form-group">
                                                    <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Tambah pada Periode Laporan (Ha)</label>
                                                    <div class="form-group">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td class="text-center">Terkena</td>
                                                                <td class="text-center">Puso 3)</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input class="form-control" required name="more_area_added[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['more_area_added'][$i] }}">
                                                                </td>
                                                                <td>
                                                                    <input class="form-control" required name="puso_area_added[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['puso_area_added'][$i] }}">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 form-group">
                                                    <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Keadaan pada Periode Laporan (Ha)</label>
                                                    <div class="form-group">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td class="text-center">Terkena</td>
                                                                <td class="text-center">Puso 3)</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input class="form-control" required name="more_area_state_period[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['more_area_state_period'][$i] }}">
                                                                </td>
                                                                <td>
                                                                    <input class="form-control" required name="puso_area_state_period[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['puso_area_state_period'][$i] }}">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 form-group">
                                                    <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Penanganan</label>
                                                    <div class="form-group">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td class="text-center">Upaya</td>
                                                                <td class="text-center">Jumlah</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input class="form-control" required name="handling_effort[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['handling_effort'][$i] }}">
                                                                </td>
                                                                <td>
                                                                    <input class="form-control" required name="total_handling_effort[]" type="number" placeholder="contoh (1,2)" value="{{ $data['report_data']['total_handling_effort'][$i] }}">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Titik Koordinat Puso (Desa)</label>
                                                    <input class="form-control" required name="coordinate[]" type="text" placeholder="Input Koordinate" value="{{ $data['report_data']['coordinate'][$i] }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                @endfor
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="alert alert-danger l-h-23">
                                * mohon isi data dengan sebenar - benarnya dan cek kembali inputan data anda. Karena <span class="font-bold">data tidak bisa diedit</span> kembali. Terima Kasih.
                            </div>
                        </div>
    
                        <div class="col-12 text-right">
                            <a href="{{ route('admin.data.data-report.store') }}" onclick="return confirm('Anda Yakin Ingin Menyimpan data, data yang di input tidak bisa di edit kembali ?');"class="btn btn-md-block btn-primary my-2">Simpan Data</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>

    $("select").prop("disabled", true);
    $("input").prop("disabled", true);

    $(".select2").select2();

    $('.pengendalian').select2();

    $('.pengendalian').on('change', function (param) {
        var value = $(this).val();
        var cek  = jQuery.inArray( "5", value );
        if (cek == -1) {
            $('.pengendalian-other').hide();
        }else{
            $('.pengendalian-other').show();
        }
    })

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    $('.commodity').select2({
        placeholder: 'Pilih Komoditas',
        ajax: {
            url: '{{ route('admin.json.commodity') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 1,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                var $$select = $('.commodity');
                var selectedValues = Array.from($$select).map(s => parseInt(s.value));

                var result = {
                    "results" : data['results'].filter(a => !selectedValues.includes(a.id))
                };

                return result;
            },
            cache: true
        }
    })

    $('#opt-default').select2({
        placeholder: 'Pilih OPT',
        ajax: {
            url: '{{ route('admin.json.opt') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    });

    $('.kecamatan').select2();

    $('.kecamatan').on('change', function (param) { 
        var kec = $(this).val();

        $('.desa').select2({
            placeholder: 'Pilih desa',
            ajax: {
                url: '{{ route('admin.json.village') }}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        sub_district_id: kec,
                    }
                    return query;
                },
                delay: 250,
                processResults: function (data) {
                    return data;
                },
                cache: true
            }
        });
    });
    
</script>
@endpush
