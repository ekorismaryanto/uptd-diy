@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Repositories\Entities\Commodity;
    use \App\Repositories\Entities\Opt;
    use \App\Repositories\Entities\Village;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Input Data
                    </li>
                </ol>
                <h4>Input Data OPT Banjir</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    @php
                        $data = $opt->toArray();
                    @endphp
                    <form action="{{ route('admin.report.tanaman-pangan.banjir.update', $opt->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <table class="table table-bordered table-striped mb-0">
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>{{ $opt->user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td>{{ optional($opt->subDistrict)->district->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td>
                                            <div>
                                                <div class="input-group">
                                                    <input type="text" class="form-control periode" autocomplete="off" required name="date" placeholder="Periode" id="datepicker-autoclose" value="{{ date('Y-m', strtotime($opt->periode)) }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-box issue-repeatable">
                            <div class="append-data">

                                <div class="issue-container issue-form-length">
                                    <div class="card border">
                                        <div class="card-header">
                                           Data OPT Banjir
                                        </div>
                                        <div class="card-body row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Kecamatan</label>
                                                    <select class="form-control kecamatan" required name="sub_district_id[]" data-placeholder="Pilih Kecamatan">
                                                        <option value=""></option>
                                                        @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $opt->subDistrict->district_id)->get() as $item)
                                                            <option value="{{ $item->id }}" {{ $data['sub_district_id'] == $item->id ? 'selected' : '' }}  >{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Komoditas</label>
                                                    <select class="form-control select2 commodity" required name="commodity_id[]" data-category="adjustment" data-placeholder="Pilih Komoditas">
                                                        @php
                                                            $commodity = Commodity::find($data['commodity_id']);
                                                        @endphp
                                                        <option value="{{ $commodity->id }}" selected>{{ $commodity->name }}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Umur (HST)</label>
                                                    <input type="number" class="form-control" required name="umur[]" placeholder="Inputkan Umur (HST)" value="{{ $data['age'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Periode TB</label>
                                                    <input type="text" class="form-control" required name="periode_tb[]" placeholder="Inputkan Umur (HST)" value="{{ $data['periode_tb'] }}">
                                                </div>
                                                
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Desa</label>
                                                    <select class="form-control select2 desa" required name="village_id[]" data-placeholder="Pilih desa">
                                                        @php
                                                            $village = Village::find($data['village_id']);
                                                        @endphp
                                                        <option value="{{ $village->id  }}" selected>{{ $village->name }}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Varietas</label>
                                                    <input type="text" class="form-control" required name="varietas[]" placeholder="Inputkan Varietas"  value="{{ $data['varieties'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Luas Waspada (ha)</label>
                                                    <input type="number" class="form-control" required name="broadly_alert[]" placeholder="Inputkan luas Waspada (ha)" value="{{ $data['broadly_alert'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Luas Pertanaman (ha)</label>
                                                    <input type="number" class="form-control" required name="planting_area[]" placeholder="Inputkan luas Pertanaman (ha)" value="{{ $data['planting_area'] }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Sisa Periode sebelumnya/Perubahan Kriteria</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <th class="text-center" colspan="2">Surut</th>
                                                            <th class="text-center" colspan="2">Puso 3)</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">Luas (Ha)</td>
                                                            <td class="text-center">Ket (Periode)</td>
                                                            <td class="text-center">Luas (Ha)</td>
                                                            <td class="text-center">Ket (Periode)</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="previous_month_rest_receding_area[]" type="number" placeholder="contoh (1,2)" value="{{ $data['previous_month_rest_receding_area'] }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="previous_month_rest_receding_description[]" type="number" placeholder="contoh (1,2)" value="{{ $data['previous_month_rest_receding_description'] }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="previous_month_rest_puso_area[]" type="number" placeholder="contoh (1,2)" value="{{ $data['previous_month_rest_puso_area'] }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="previous_month_rest_puso_description[]" type="number" placeholder="contoh (1,2)" value="{{ $data['previous_month_rest_puso_description'] }}">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Tambah pada Periode Laporan (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">Terkena</td>
                                                            <td class="text-center">Puso 3)</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="more_area_added[]" type="number" placeholder="contoh (1,2)" value="{{ $data['more_area_added'] }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="puso_area_added[]" type="number" placeholder="contoh (1,2)" value="{{ $data['puso_area_added'] }}">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Keadaan pada Periode Laporan (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">Terkena</td>
                                                            <td class="text-center">Puso 3)</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="more_area_state_period[]" type="number" placeholder="contoh (1,2)" value="{{ $data['more_area_state_period'] }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="puso_area_state_period[]" type="number" placeholder="contoh (1,2)" value="{{ $data['puso_area_state_period'] }}">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Penanganan</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">Upaya</td>
                                                            <td class="text-center">Jumlah</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="handling_effort[]" type="number" placeholder="contoh (1,2)" value="{{ $data['handling_effort'] }}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="total_handling_effort[]" type="number" placeholder="contoh (1,2)" value="{{ $data['total_handling_effort'] }}">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Titik Koordinat Puso (Desa)</label>
                                                <input class="form-control" required name="coordinate[]" type="text" placeholder="Input Koordinate" value="{{ $data['coordinate'] }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-primary">Simpan dan Lanjutkan</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();

    $('.pengendalian').select2();

    $('.pengendalian').on('change', function (param) {
        var value = $(this).val();
        var cek  = jQuery.inArray( "5", value );
        if (cek == -1) {
            $('.pengendalian-other').hide();
        }else{
            $('.pengendalian-other').show();
        }
    })

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    $('.commodity').select2({
        placeholder: 'Pilih Komoditas',
        ajax: {
            url: '{{ route('admin.json.commodity') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 1,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                var $$select = $('.commodity');
                var selectedValues = Array.from($$select).map(s => parseInt(s.value));

                var result = {
                    "results" : data['results'].filter(a => !selectedValues.includes(a.id))
                };

                return result;
            },
            cache: true
        }
    })

    $('#opt-default').select2({
        placeholder: 'Pilih OPT',
        ajax: {
            url: '{{ route('admin.json.opt') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    });

    $('.kecamatan').select2();

    $('.kecamatan').on('change', function (param) { 
        var kec = $(this).val();

        $('.desa').select2({
            placeholder: 'Pilih desa',
            ajax: {
                url: '{{ route('admin.json.village') }}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        sub_district_id: kec,
                    }
                    return query;
                },
                delay: 250,
                processResults: function (data) {
                    return data;
                },
                cache: true
            }
        });
    });

    $('.desa').select2({
            placeholder: 'Pilih desa',
            ajax: {
                url: '{{ route('admin.json.village') }}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        sub_district_id: {{ $data['sub_district_id'] }},
                    }
                    return query;
                },
                delay: 250,
                processResults: function (data) {
                    return data;
                },
                cache: true
            }
        });
</script>
@endpush
