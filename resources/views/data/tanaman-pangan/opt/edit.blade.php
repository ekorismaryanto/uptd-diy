@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Input Data
                    </li>
                </ol>
                <h4>Input Data</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.report.tanaman-pangan.update', $opt->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <table class="table table-bordered table-striped mb-0">
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>{{ $opt->user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td>{{ optional($opt->subDistrict)->district->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td>
                                            <div>
                                                <div class="input-group">
                                                    <input type="text" class="form-control periode" autocomplete="off" required name="date" placeholder="Periode" id="datepicker-autoclose" value="{{ date('Y-m', strtotime($opt->periode)) }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    
                        <div class="form-box issue-repeatable">
                            <div class="append-data">
                                <div class="issue-container issue-form-length">
                                    <div class="card border">
                                        <div class="card-header">
                                        Data OPT
                                        </div>
                                        <div class="card-body row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold label-green">Kecamatan</label>
                                                        <select class="form-control kecamatan" required name="sub_district_id[]" data-placeholder="Pilih Kecamatan" >
                                                            <option value=""></option>
                                                            @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->where('district_id', $opt->subDistrict->district->id)->get() as $item)
                                                                <option value="{{ $item->id }}" {{ $opt->sub_district_id == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Komoditas</label>
                                                    
                                                    <select class="form-control select2 commodity" required name="commodity_id[]" data-category="adjustment" data-placeholder="Pilih Komoditas" >
                                                        @php
                                                            $commodity = resolve(\App\Repositories\Entities\Commodity::class)->where('id', $opt->commodity_id)->first();
                                                        @endphp
                                                        <option value="{{ $commodity->id }}" selected>{{ $commodity->name }}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Umur (HST)</label>
                                                    <input type="number" class="form-control" required name="umur[]" placeholder="Inputkan Umur (HST)" value="{{ $opt->age }}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Periode TB</label>
                                                    <input type="text" class="form-control" required name="periode_tb[]" placeholder="Inputkan Umur (HST)"  value="{{ $opt->periode_tb }}" >
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">Desa</label>
                                                    <select class="form-control select2 desa"  name="village_id[]" data-placeholder="Pilih desa">
                                                        @php
                                                            $desa = resolve(\App\Repositories\Entities\Village::class)->where('id', $opt->village_id)->first();
                                                        @endphp
                                                        <option value="{{ $desa->id }}" selected>{{ $desa->name }}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Varietas</label>
                                                    <input type="text" class="form-control" required name="varietas[]" placeholder="Inputkan Varietas" value="{{ $opt->varieties }}"  >
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Luas Pertanaman (ha)</label>
                                                    <input type="number" class="form-control" required name="planting_area[]" placeholder="Inputkan luas pertnamana (ha)"  value="{{ $opt->planting_area }}"  >
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label font-bold label-green">OPT</label>
                                                    <select class="form-control" id="opt-default" name="opts[]" required data-placeholder="Pilih OPT" >
                                                        @php
                                                            $opts = resolve(\App\Repositories\Entities\Opt::class)->where('id', $opt->opt_id)->first();
                                                        @endphp
                                                        <option value="{{ $opts->id }}" selected>{{ $opts->name }}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-uppercase font-bold label-green">Kriteria OPT</label>
                                                    <input type="text" class="form-control" required name="criteria_opt[]" placeholder="Inputkan Varietas" value="{{ $opt->criteria_opt }}"  >
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Sisa Periode Sebelumnya</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <th class="text-center" colspan="6">Sisa serangan/Perubahan <hr></th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">R</td>
                                                            <td class="text-center">S</td>
                                                            <td class="text-center">B</td>
                                                            <td class="text-center">P</td>
                                                            <td class="text-center">Luas Terkendali</td>
                                                            <td class="text-center">Luas Panen</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="sisa_serangan_r[]" type="number" placeholder="contoh (1,2)" value="{{ $opt->sisaPeriode->ringan }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="sisa_serangan_s[]" type="number" placeholder="contoh (1,2)" value="{{ $opt->sisaPeriode->sedang }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="sisa_serangan_b[]" type="number" placeholder="contoh (1,2)" value="{{ $opt->sisaPeriode->berat }}" >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="sisa_serangan_p[]" type="number" placeholder="contoh (1,2)"  value="{{ $opt->sisaPeriode->puso }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="sisa_serangan_terkendali[]" type="number" placeholder="contoh (1,2)"  value="{{ $opt->sisaPeriode->retrained_area }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="sisa_serangan_panen[]" type="number" placeholder="contoh (1,2)" value="{{ $opt->sisaPeriode->harvest_area }}"  >
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Tambah Serangan pada Periode Laporan (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">R</td>
                                                            <td class="text-center">S</td>
                                                            <td class="text-center">B</td>
                                                            <td class="text-center">P</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="tambah_serangan_r[]" type="number" placeholder="contoh (1,2)" value="{{$opt->luasTambahSerangan->ringan }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="tambah_serangan_s[]" type="number" placeholder="contoh (1,2)" value="{{$opt->luasTambahSerangan->sedang }}""  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="tambah_serangan_b[]" type="number" placeholder="contoh (1,2)" value="{{$opt->luasTambahSerangan->berat }}""  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="tambah_serangan_p[]" type="number" placeholder="contoh (1,2)" value="{{$opt->luasTambahSerangan->puso }}""  >
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Pengendalian (Ha)</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <th colspan="2" class="text-center">Pestisida</th>
                                                            <th colspan="2" class="text-center">Non Pestisida</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">Kimia</td>
                                                            <td class="text-center">Hayati</td>
                                                            <td class="text-center">Eradiksi</td>
                                                            <td class="text-center">Cara Lain</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="pestisida_kimia[]" type="number" placeholder="contoh (1,2)" value="{{ $opt->luasPengendalian->kimia }}""  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="pestisida_hayati[]" type="number" placeholder="contoh (1,2)" value="{{ $opt->luasPengendalian->hayati }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="non_pestisida_eradiksi[]" type="number" placeholder="contoh (1,2)" value="{{$opt->luasPengendalian->eradikasi }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="non_pestisida_lain[]" type="number" placeholder="contoh (1,2)" value="{{ $opt->luasPengendalian->others }}"  >
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Keadaan Serangan</label>
                                                <div class="form-group">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="text-center">R</td>
                                                            <td class="text-center">S</td>
                                                            <td class="text-center">B</td>
                                                            <td class="text-center">P</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" required name="keadaa_serangan_r[]" type="number" placeholder="contoh (1,2)" value="{{ $opt->keadaanSerangan->ringan }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="keadaa_serangan_s[]" type="number" placeholder="contoh (1,2)" value="{{$opt->keadaanSerangan->sedang }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="keadaa_serangan_b[]" type="number" placeholder="contoh (1,2)"  value="{{$opt->keadaanSerangan->berat }}"  >
                                                            </td>
                                                            <td>
                                                                <input class="form-control" required name="keadaa_serangan_p[]" type="number" placeholder="contoh (1,2)" value="{{$opt->keadaanSerangan->puso }}"  >
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    
    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    $(".select2").select2();

    $('.pengendalian').select2();

    $('.pengendalian').on('change', function (param) {
        var value = $(this).val();
        var cek  = jQuery.inArray( "5", value );
        if (cek == -1) {
            $('.pengendalian-other').hide();
        }else{
            $('.pengendalian-other').show();
        }
    })

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    $('.commodity').select2({
        placeholder: 'Pilih Komoditas',
        ajax: {
            url: '{{ route('admin.json.commodity') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 1,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                var $$select = $('.commodity');
                var selectedValues = Array.from($$select).map(s => parseInt(s.value));

                var result = {
                    "results" : data['results'].filter(a => !selectedValues.includes(a.id))
                };

                return result;
            },
            cache: true
        }
    })

    $('#opt-default').select2({
        placeholder: 'Pilih OPT',
        ajax: {
            url: '{{ route('admin.json.opt') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    });

    $('.kecamatan').select2();

    $('.kecamatan').on('change', function (param) { 
        var kec = $(this).val();

        $('.desa').select2({
            placeholder: 'Pilih desa',
            ajax: {
                url: '{{ route('admin.json.village') }}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        sub_district_id: kec,
                    }
                    return query;
                },
                delay: 250,
                processResults: function (data) {
                    return data;
                },
                cache: true
            }
        });
    });

    $('.desa').select2({
        placeholder: 'Pilih desa',
        ajax: {
            url: '{{ route('admin.json.village') }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page,
                    sub_district_id: '{{ $opt->sub_district_id }}',
                }
                return query;
            },
            delay: 250,
            processResults: function (data) {
                return data;
            },
            cache: true
        }
    });
</script>
@endpush
