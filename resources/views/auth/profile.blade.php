@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item active">
                        Profile
                    </li>
                </ol>
                <h4>Input Data</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <form action="{{ route('admin.profile.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Nama</label>
                                    <input type="text" name="name" class="form-control" value="{{ $user->name }}"  placeholder="Input Nama" required>
                                    @error('name')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">NIP / Nik</label>
                                    <input type="text" name="nip" class="form-control" value="{{ $user->nip }}" placeholder="Input NIP" required>
                                    @error('nip')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">ESELON</label>
                                    <input type="text" name="eselon" class="form-control" placeholder="Input eselon" value="{{ $user->eselon }}"  required>
                                    @error('eselon')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Golongan</label>
                                    <input type="text" name="type" class="form-control" placeholder="Input Golongan" value="{{ $user->type }}"  required>
                                    @error('type')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">TMT Golongan:</label>
                                    <div>
                                        <div class="input-group">
                                            <input type="text" name="tmt_type" value="{{ date('d/m/Y', strtotime($user->tmt_type)) }}" required class="form-control" placeholder="Periode"
                                                id="datepicker-autoclose">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Jabatan</label>
                                    <input type="text" name="position" value="{{ $user->position }}" class="form-control" placeholder="Input Jabatan" required>
                                    @error('position')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Tempat Kerja/Wilayah Kerja</label>
                                    <select class="form-control select2" name="work_location" disabled data-placeholder="Pilih Wilayah Kerja">
                                        <option value="" selected>Pilih Wilayah Kerja</option>
                                        @foreach (resolve(\App\Repositories\Entities\District::class)->get() as $item)
                                            <option value="{{ $item->id }}" {{ $user->work_location == $item->id ? 'selected' : '' }} >{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Status Karyawan</label>
                                    <select name="status_employee" class="form-control" id="">
                                        <option value="1" {{ $user->status_employee == 1 ? 'selected' : '' }} >PNS</option>
                                        <option value="0" {{ $user->status_employee == 0 ? 'selected' : '' }} >Bukan PNS</option>
                                    </select>
                                    @error('status_employee')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Nomer Hp</label>
                                    <input type="text" name="no_hp" class="form-control"  value="{{ $user->no_hp }}"  placeholder="Input No Hp" required>
                                    @error('no_hp')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12"></div>
                            <div class="col-lg-6 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Email</label>
                                    <input type="email" name="email"  value="{{ $user->email }}" class="form-control" placeholder="Input Email" required>
                                    @error('email')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Input Password">
                                    @error('password')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <img src="{{ $user->photo ? url('storage/profile/'. $user->photo) : 'https://placehold.it/180x180' }}" id="preview" style="height:180px" class="img-thumbnail">
                                    <input type="file" name="img" class="file" accept="image/*">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <a href="{{ route('admin.user-management.role.index') }}" class="btn btn-danger">Batal</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
</script>
<script>
    // Select2
    $(".select2").select2();
    jQuery('#datepicker-autoclose').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    $(document).on("click", ".browse", function() {
  var file = $(this).parents().find(".file");
  file.trigger("click");
});
$('input[type="file"]').change(function(e) {
  var fileName = e.target.files[0].name;
  $("#file").val(fileName);

  var reader = new FileReader();
  reader.onload = function(e) {
    // get loaded data and render thumbnail.
    document.getElementById("preview").src = e.target.result;
  };
  // read the image file as a data URL.
  reader.readAsDataURL(this.files[0]);
});
</script>
@endsection
