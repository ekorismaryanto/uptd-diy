<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
</head>
<body>

    
    <style>
    .page-break {
        page-break-after: always;
    }
    </style>
    <style>
        @page { 
            size: legal landscape; 
        }
        align-middle{vertical-align:middle!important}

        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;

        }
        .text-center{
            text-align: center;
        }
        </style>
        <style type="text/css" media="print">
            @page { size: landscape; }
        </style>
          
    @php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp
    <table id="tech-companies-1" class="table table-striped">
        <thead>
            <tr>
                {{-- <th class="align-middle" rowspan="3"></th> --}}
                <th class="align-middle" rowspan="3">Periode</th>
                <th class="align-middle" rowspan="3">User</th>
                <th class="align-middle" rowspan="3">Wilayah</th>
                <th class="align-middle" rowspan="3">Komoditas</th>
                <th class="align-middle" rowspan="3">Varietas</th>
                <th class="align-middle" rowspan="3">Umur (HST)</th>
                <th class="align-middle" rowspan="3">Luas Tanam (HA)</th>
                <th class="align-middle" rowspan="3">Periode TB</th>
                <th class="align-middle" rowspan="3">Luas Waspada</th>
                <th class="align-middle text-center" style="min-width: 170px;" colspan="4">Sisa Periode sebelumnya/Perubahan Kriteria</th>
                <th class="align-middle text-center" style="min-width: 170px;" colspan="2">Luas Tambah pada Periode Laporan (Ha)</th>
                <th class="align-middle text-center" style="min-width: 170px;" colspan="2">Luas Keadaan pada Periode Laporan (Ha)</th>
                <th class="align-middle text-center" style="min-width: 170px;" colspan="2">Penanganan</th>
                <th class="align-middle" rowspan="3" colspan="3">Titik Koordinat Puso (Desa)</th>
            </tr>
            <tr>
                <th colspan="2" class="text-center">Surut</th>
                <th colspan="2" class="text-center">Puso 3)</th>
                <th rowspan="2" class="text-center">Terkena</th>
                <th rowspan="2" class="text-center">Puso 3)</th>
                <th rowspan="2" class="text-center">Terkena</th>
                <th rowspan="2" class="text-center">Puso 3)</th>
                <th rowspan="2" class="text-center">Upaya</th>
                <th rowspan="2" class="text-center">Jumlah</th>
            </tr>
            <tr>
                <th>Luas (Ha)</th>
                <th>Ket (Periode)</th>
                <th>Luas (Ha)</th>
                <th>Ket (Periode)</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($reports as $item)
                <tr>
                    <td class="text-center">{{ date_view($item->periode)}}</td>
                    <td class="text-center">{{ $item->user->name }}</td>
                    <td>Yogyakarta, {{ $item->village->subDistrict->district->name.', '. $item->village->subDistrict->name.', '.$item->village->name }}</td>
                    <td class="text-center">{{ $item->commodity->name }}</td>
                    <td class="text-center">{{ $item->varieties }}</td>
                    <td class="text-center">{{ $item->age }}</td>
                    <td class="text-center">{{ $item->planting_area }}</td>
                    <td class="text-center">{{ $item->periode_tb ?? '-'  }}</td>
                    <td class="text-center">{{ $item->broadly_alert ?? '-'  }}</td>
                    <td class="text-center">{{ $item->previous_month_rest_receding_area ?? '-'  }}</td>
                    <td class="text-center">{{ $item->previous_month_rest_receding_descriptio ?? '-' }}</td>
                    <td class="text-center">{{ $item->previous_month_rest_puso_area ?? '-'  }}</td>
                    <td class="text-center">{{ $item->previous_month_rest_puso_description ?? '-'  }}</td>
                    <td class="text-center">{{ $item->more_area_added ?? '-'  }}</td>
                    <td class="text-center">{{ $item->puso_area_added ?? '-'  }}</td>
                    <td class="text-center">{{ $item->more_area_state_period ?? '-'  }}</td>
                    <td class="text-center">{{ $item->puso_area_state_period ?? '-'  }}</td>
                    <td class="text-center">{{ $item->handling_effort ?? '-'  }}</td>
                    <td class="text-center">{{ $item->total_handling_effort ?? '-'  }}</td>
                    <td class="text-center">{{ $item->coordinate ?? '-'  }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="10">Data Empty</td>
                </tr>
            @endforelse
        </tbody>
    </table> 
</body>

@if (Request::routeIs('admin.report.tanaman-pangan.banjir.print'))
    <style>
        @page {
            size: A4;
            margin: 0;
            size: landscape;
            
            }
            @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            size: landscape;
            }
            /* ... the rest of the rules ... */
        }
    </style>
    <script>
        window.print();
    </script>
@endif
</html>