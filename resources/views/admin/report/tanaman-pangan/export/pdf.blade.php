<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
</head>
<body>

    
    <style>
    .page-break {
        page-break-after: always;
    }
    </style>
    <style>
        @page { 
            size: legal landscape; 
        }
        align-middle{vertical-align:middle!important}

        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;

        }
        .text-center{
            text-align: center;
        }
        </style>
        <style type="text/css" media="print">
            @page { size: landscape; }
        </style>
          
    @php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp
    <table id="tech-companies-1" class="table table-striped">
        <thead>
            <tr>
                <th class="align-middle" rowspan="3">Periode</th>
                <th class="align-middle" rowspan="3">User</th>
                <th class="align-middle" rowspan="3">Wilayah</th>
                <th class="align-middle" rowspan="3">Komoditas</th>
                <th class="align-middle" rowspan="3">Varietas</th>
                <th class="align-middle" rowspan="3">Umur (HST)</th>
                <th class="align-middle" rowspan="3">Luas Tanam (HA)</th>
                <th class="align-middle" rowspan="3">Jenis OPT</th>
                <th class="align-middle" rowspan="3">Kriteria OPT</th>
                <th class="align-middle" rowspan="3">Periode TB</th>
                <th class="align-middle text-center" style="min-width: 170px;" colspan="7">Sisa Periode Sebelumnya</th>
                <th class="align-middle text-center" style="min-width: 200px;" colspan="5">Luas Tambah Serangan pada Periode Laporan (Ha)</th>
                <th class="align-middle text-center" style="min-width: 200px;" colspan="5">Luas Pengendalian (Ha)</th>
                <th class="align-middle text-center" style="min-width: 200px;" colspan="5" >Keadaan Serangan</th>
            </tr>
            <tr>
                <th colspan="5" class="text-center">Sisa serangan/Perubahan</th>
                <th colspan="1" rowspan="2" class="text-center" style="vertical-align: middle;">Luas Terkendali</th>
                <th colspan="1" rowspan="2" class="text-center" style="vertical-align: middle;">Luas Panen</th>
                <th colspan="4"></th>
                <th colspan="1" rowspan="2" class="text-center" style="vertical-align: middle;">Jml</th>
                <th colspan="2"  class="text-center" style="vertical-align: middle;">Pestisida</th>
                <th colspan="2" class="text-center" style="vertical-align: middle;">Non Pestisida</th>
                <th colspan="1" rowspan="2" class="text-center" style="vertical-align: middle;">Jml</th>
                <th colspan="5"></th>
            </tr>
            <tr>
                <th>R</th>
                <th>S</th>
                <th>B</th>
                <th>P</th>
                <th>Jml</th>
                <th>R</th>
                <th>S</th>
                <th>B</th>
                <th>P</th>
                <th>Kimia</th>
                <th>Hayati</th>
                <th>Eradiksi</th>
                <th>Cara Lain</th>
                <th>R</th>
                <th>S</th>
                <th>B</th>
                <th>P</th>
                <th>Jml</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($reports as $item)
            <tr>
                <td class="text-center">{{ date_view($item->periode) }}</td>
                <td class="text-center">{{ $item->user->name  }}</td>
                <td class="text-center">Yogyakarta, {{ $item->village->subDistrict->district->name.', '. $item->village->subDistrict->name.', '.$item->village->name }}</td>
                <td class="text-center">{{ $item->commodity->name }}</td>
                <td class="text-center">{{ $item->varieties }}</td>
                <td class="text-center">{{ $item->age }}</td>
                <td class="text-center">{{ $item->planting_area }}</td>
                <td class="text-center">{{ $item->opt->name }}</td>
                <td class="text-center">{{ $item->criteria_opt }}</td>
                <td class="text-center">{{ $item->periode_tb }}</td>
                <td class="text-center">{{ $item->sisaPeriode->ringan }}</td>
                <td class="text-center">{{ $item->sisaPeriode->berat }}</td>
                <td class="text-center">{{ $item->sisaPeriode->sedang }}</td>
                <td class="text-center">{{ $item->sisaPeriode->puso }}</td>
                <td class="text-center">
                    {{
                        $item->sisaPeriode->ringan 
                        +
                        $item->sisaPeriode->berat
                        +
                        $item->sisaPeriode->sedang
                        +
                        $item->sisaPeriode->puso
                    }}
                </td>
                <td class="text-center">{{ $item->sisaPeriode->retrained_area }}</td>
                <td class="text-center">{{ $item->sisaPeriode->harvest_area }}</td>
                <td class="text-center">{{ $item->luasTambahSerangan->ringan }}</td>
                <td class="text-center">{{ $item->luasTambahSerangan->berat }}</td>
                <td class="text-center">{{ $item->luasTambahSerangan->sedang }}</td>
                <td class="text-center">{{ $item->luasTambahSerangan->puso }}</td>
                <td class="text-center">
                    {{
                        $item->luasTambahSerangan->ringan 
                        +
                        $item->luasTambahSerangan->berat
                        +
                        $item->luasTambahSerangan->sedang
                        +
                        $item->luasTambahSerangan->puso
                    }}
                </td>
                <td class="text-center">{{ $item->luasPengendalian->kimia }}</td>
                <td class="text-center">{{ $item->luasPengendalian->hayati }}</td>
                <td class="text-center">{{ $item->luasPengendalian->eradikasi }}</td>
                <td class="text-center">{{ $item->luasPengendalian->others }}</td>
                <td class="text-center"></td>
                <td class="text-center">{{ $item->keadaanSerangan->ringan }}</td>
                <td class="text-center">{{ $item->keadaanSerangan->berat }}</td>
                <td class="text-center">{{ $item->keadaanSerangan->sedang }}</td>
                <td class="text-center">{{ $item->keadaanSerangan->puso }}</td>
                <td class="text-center">
                    {{
                        $item->keadaanSerangan->ringan 
                        +
                        $item->keadaanSerangan->berat
                        +
                        $item->keadaanSerangan->sedang
                        +
                        $item->keadaanSerangan->puso
                    }}
                </td>
            <tr>
            @empty
            <tr>
                <td colspan="10">Data Empty</td>
            </tr>
            @endforelse
        </tbody>
    </table> 
</body>

@if (Request::routeIs('admin.report.print'))
    <script>
        window.print();
    </script>
@endif
</html>