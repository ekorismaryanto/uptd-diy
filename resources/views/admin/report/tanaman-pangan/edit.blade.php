@extends('layouts.master')

@section('css')
    <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item">
                        Input Data
                    </li>
                    <li class="breadcrumb-item active">
                        Laporan
                    </li>
                </ol>
                <h4>Selamat Datang</h4>
                <h6>Silahkan Melakukan inputan data pada form dibawah</h6>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 mb-4 header-title">LAPORAN SERANGAN ORGANISME PENGANGGU TUMBUHAN (OPT)</h4>
                    <form action="{{ route('admin.data.data-report.create-step-1') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label class="control-label font-bold label-green">Periode</label>
                                <div>
                                    <div class="input-group">
                                        <input type="text" class="form-control periode" autocomplete="off" required name="date" placeholder="Periode" id="datepicker-autoclose">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <div class="form-group">
                                    <label class="control-label font-bold label-green">Kategori</label>
                                    <select class="form-control select2 type" required name="type_commodity" data-placeholder="Pilih Data">
                                        <option value=""></option>
                                        @foreach (TypeCommodity::labels() as $key => $item)
                                            <option value="{{ $key }}" {{ $key != 3 ? 'disabled' : '' }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <textarea name="description" id="laporan_editor" rows="10" cols="100" required>
                            <h3><strong>I. PENDAHULUAN</strong></h3>

                            <h3><strong>II. JENIS KEGIATAN</strong></h3>

                            <h3><strong>III. SARANA DAN PRASARANA</strong></h3>

                            <p>1. Persediaan Pestisida</p>

                            <p>2. Alat Pengendalian</p>

                            <h3><strong>IV. PENGGUNAAN PESTISIDA KIMIA, NABATI DAN APH</strong></h3>

                            <p>1. Penggunaan Pestisida Kimia</p>

                            <p>2. Penggunaan Pestisida Nabati</p>

                            <p>3. Penggunaan Agensia Hayati</p>

                            <h3><strong>V. PERMASALAHAN DAN PEMECAHANNYA</strong></h3>

                            <h3><strong>VI. RENCANA KEGIATAN BULAN SELANJUTNYA</strong></h3>

                        </textarea>
                        <div class="mt-4 text-right">
                            <button type="submit" class="btn btn-primary">Simpan & Lanjutkan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@push('styles')
<style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #ccb9b9 !important;
  opacity: 1; /* Firefox */
}
</style>
@endpush

@push('scripts')
<!--Morris Chart-->
<script src="{{ URL::asset('assets/plugins/morris/morris.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/raphael/raphael-min.js')}}"></script>

<script src="{{ URL::asset('assets/pages/dashboard.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();

    $(".periode").datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

    CKEDITOR.replace( 'Resolution', {
        height: 400
    } );

    // Replace the <textarea id="editor1"> with a CKEditor 4
    // instance, using default configuration.
    CKEDITOR.replace('laporan_editor');

    CKEDITOR.instances.editor1.on('change', function () {
        var data = CKEDITOR.instances.laporan_editor.getData();
    })

    document.querySelector('.custom-file-input').addEventListener('change', function (e) {
        var fileName = document.getElementById("myInput").files[0].name;
        $('.custom-file-label').text(fileName);
    })

</script>
@endpush
