@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            @include('admin.report.tanaman-pangan..__header')
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">                        
                        <div class="col-12 my-2">
                            <form action="">
                                <div class="row">
                                    <div class="col-lg-4 my-1">
                                        <a href="{{ route('admin.report.tanaman-pangan.print', request()->all()) }}" target="_blank" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-print"></span> Print
                                         </a>
                                         <a href="{{ route('admin.report.tanaman-pangan.pdf', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-pdf"></span> Pdf
                                         </a>
                                        <a href="{{ route('admin.report.tanaman-pangan.xls', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-excel"></span> Xls
                                         </a>
                                         <a href="{{ route('admin.report.tanaman-pangan.csv', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-file-excel"></span> CSV
                                        </a>
                                    </div>
                                    <div class="col-lg-3 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker-autoclose" name="date" placeholder="Periode" value="{{ Request::get('date') }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-4 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Cari User">
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-1 my-2">
                                        <div class="form-group">
                                            <button class="btn btn-outline-primary btn-md-block btn-block" style="height: 38px;"><i  class="mx-2 mdi mdi-magnify"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 my-3">
                            <div class="form-group">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle" rowspan="3"></th>
                                                    <th class="align-middle" rowspan="3">Periode</th>
                                                    <th class="align-middle" rowspan="3">User</th>
                                                    <th class="align-middle" rowspan="3">Wilayah</th>
                                                    <th class="align-middle" rowspan="3">Komoditas</th>
                                                    <th class="align-middle" rowspan="3">Varietas</th>
                                                    <th class="align-middle" rowspan="3">Umur (HST)</th>
                                                    <th class="align-middle" rowspan="3">Luas Tanam (HA)</th>
                                                    <th class="align-middle" rowspan="3">Jenis OPT</th>
                                                    <th class="align-middle" rowspan="3">Kriteria OPT</th>
                                                    <th class="align-middle" rowspan="3">Periode TB</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;" colspan="7">Sisa Periode Sebelumnya</th>
                                                    <th class="align-middle text-center" style="min-width: 200px;" colspan="5">Luas Tambah Serangan pada Periode Laporan (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 200px;" colspan="5">Luas Pengendalian (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 200px;" colspan="5" >Keadaan Serangan</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="5" class="text-center">Sisa serangan/Perubahan</th>
                                                    <th colspan="1" rowspan="2" class="text-center" style="vertical-align: middle;">Luas Terkendali</th>
                                                    <th colspan="1" rowspan="2" class="text-center" style="vertical-align: middle;">Luas Panen</th>
                                                    <th colspan="4"></th>
                                                    <th colspan="1" rowspan="2" class="text-center" style="vertical-align: middle;">Jml</th>
                                                    <th colspan="2"  class="text-center" style="vertical-align: middle;">Pestisida</th>
                                                    <th colspan="2" class="text-center" style="vertical-align: middle;">Non Pestisida</th>
                                                    <th colspan="1" rowspan="2" class="text-center" style="vertical-align: middle;">Jml</th>
                                                    <th colspan="5"></th>
                                                </tr>
                                                <tr>
                                                    <th>R</th>
                                                    <th>S</th>
                                                    <th>B</th>
                                                    <th>P</th>
                                                    <th>Jml</th>
                                                    <th>R</th>
                                                    <th>S</th>
                                                    <th>B</th>
                                                    <th>P</th>
                                                    <th>Kimia</th>
                                                    <th>Hayati</th>
                                                    <th>Eradiksi</th>
                                                    <th>Cara Lain</th>
                                                    <th>R</th>
                                                    <th>S</th>
                                                    <th>B</th>
                                                    <th>P</th>
                                                    <th>Jml</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($reports as $item)
                                                <tr>
                                                    <td>
                                                        @can('input-data.edit')

                                                        <a href="{{ route('admin.report.tanaman-pangan.edit', $item->id) }}" class="btn btn-sm btn-primary btn-block">Edit</a>
                                                        @endcan

                                                        <br>
                                                            
                                                        @can('input-data.delete')
                                                            <a href="{{ route('admin.report.tanaman-pangan.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                                <button class="btn btn-danger btn-sm btn-block"> Hapus</button>
                                                            </a>
                                                        @endcan
                                                    </td>
                                                    <td>{{ date_view($item->periode) }}</td>
                                                    <td>{{ $item->user->name  }}</td>
                                                    <td>Yogyakarta, {{ $item->village->subDistrict->district->name.', '. $item->village->subDistrict->name.', '.$item->village->name }}</td>
                                                    <td>{{ $item->commodity->name }}</td>
                                                    <td>{{ $item->varieties }}</td>
                                                    <td>{{ $item->age }}</td>
                                                    <td>{{ $item->planting_area }}</td>
                                                    <td>{{ $item->opt->name }}</td>
                                                    <td>{{ $item->criteria_opt }}</td>
                                                    <td>{{ $item->periode_tb }}</td>
                                                    <td class="text-center">{{ $item->sisaPeriode->ringan }}</td>
                                                    <td class="text-center">{{ $item->sisaPeriode->berat }}</td>
                                                    <td class="text-center">{{ $item->sisaPeriode->sedang }}</td>
                                                    <td class="text-center">{{ $item->sisaPeriode->puso }}</td>
                                                    <td class="text-center">
                                                        {{
                                                            $item->sisaPeriode->ringan 
                                                            +
                                                            $item->sisaPeriode->berat
                                                            +
                                                            $item->sisaPeriode->sedang
                                                            +
                                                            $item->sisaPeriode->puso
                                                        }}
                                                    </td>
                                                    <td class="text-center">{{ $item->sisaPeriode->retrained_area }}</td>
                                                    <td class="text-center">{{ $item->sisaPeriode->harvest_area }}</td>
                                                    <td class="text-center">{{ $item->luasTambahSerangan->ringan }}</td>
                                                    <td class="text-center">{{ $item->luasTambahSerangan->berat }}</td>
                                                    <td class="text-center">{{ $item->luasTambahSerangan->sedang }}</td>
                                                    <td class="text-center">{{ $item->luasTambahSerangan->puso }}</td>
                                                    <td class="text-center">
                                                        {{
                                                            $item->luasTambahSerangan->ringan 
                                                            +
                                                            $item->luasTambahSerangan->berat
                                                            +
                                                            $item->luasTambahSerangan->sedang
                                                            +
                                                            $item->luasTambahSerangan->puso
                                                        }}
                                                    </td>
                                                    <td class="text-center">{{ $item->luasPengendalian->kimia }}</td>
                                                    <td class="text-center">{{ $item->luasPengendalian->hayati }}</td>
                                                    <td class="text-center">{{ $item->luasPengendalian->eradikasi }}</td>
                                                    <td class="text-center">{{ $item->luasPengendalian->others }}</td>
                                                    <td class="text-center"></td>
                                                    <td class="text-center">{{ $item->keadaanSerangan->ringan }}</td>
                                                    <td class="text-center">{{ $item->keadaanSerangan->berat }}</td>
                                                    <td class="text-center">{{ $item->keadaanSerangan->sedang }}</td>
                                                    <td class="text-center">{{ $item->keadaanSerangan->puso }}</td>
                                                    <td class="text-center">
                                                        {{
                                                            $item->keadaanSerangan->ringan 
                                                            +
                                                            $item->keadaanSerangan->berat
                                                            +
                                                            $item->keadaanSerangan->sedang
                                                            +
                                                            $item->keadaanSerangan->puso
                                                        }}
                                                    </td>
                                                <tr>
                                                @empty
                                                <tr>
                                                    <td colspan="10">Data Empty</td>
                                                </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $reports->links('layouts.pagination')  !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2({
        placeholder: "Kategori",
    });
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

</script>
@endsection
