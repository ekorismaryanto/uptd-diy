<div class="page-title-box">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            Home
        </li>
        <li class="breadcrumb-item active">
            Report
        </li>
    </ol>
    <h4>Halaman Laporan Tanaman Pangan</h4>
    <a href="{{ route('admin.report.tanaman-pangan.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.tanaman-pangan.index') ? 'btn-success' : 'btn-primary' }}">Data Opt</button>
    </a>
    <a href="{{ route('admin.report.tanaman-pangan.banjir.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.tanaman-pangan.banjir.*') ? 'btn-success' : 'btn-primary' }}">Data Opt Banjir</button>
    </a>
    <a href="{{ route('admin.report.tanaman-pangan.kekeringan.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.tanaman-pangan.kekeringan.*') ? 'btn-success' : 'btn-primary' }}">Data Opt Kekeringan</button>
    </a>
</div>