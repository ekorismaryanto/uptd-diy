<div class="page-title-box">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            Home
        </li>
        <li class="breadcrumb-item active">
            Report
        </li>
    </ol>
    <h4>Halaman Laporan Hortikultura</h4>
    <a href="{{ route('admin.report.holtikultura.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.holtikultura.index') ? 'btn-success' : 'btn-primary' }}">Data Opt</button>
    </a>
    <a href="{{ route('admin.report.holtikultura.banjir.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.holtikultura.banjir.*') ? 'btn-success' : 'btn-primary' }}">Data Opt Banjir</button>
    </a>
    <a href="{{ route('admin.report.holtikultura.kekeringan.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.holtikultura.kekeringan.*') ? 'btn-success' : 'btn-primary' }}">Data Opt Kekeringan</button>
    </a>
</div>