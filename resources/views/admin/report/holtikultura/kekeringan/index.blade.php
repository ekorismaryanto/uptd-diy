@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            @include('admin.report.holtikultura.__header') 
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">                        
                        <div class="col-12 my-2">
                            <form action="">
                                <div class="row">
                                    <div class="col-lg-4 my-1">
                                        <a href="{{ route('admin.report.holtikultura.kekeringan.print', request()->all()) }}" target="_blank" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-print"></span> Print
                                         </a>
                                         <a href="{{ route('admin.report.holtikultura.kekeringan.pdf', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-pdf"></span> Pdf
                                         </a>
                                        <a href="{{ route('admin.report.holtikultura.kekeringan.xls', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-excel"></span> Xls
                                         </a>
                                         <a href="{{ route('admin.report.holtikultura.kekeringan.csv', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-file-excel"></span> CSV
                                        </a>
                                    </div>
                                    <div class="col-lg-3 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker-autoclose" name="date" placeholder="Periode" value="{{ Request::get('date') }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-4 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Cari User">
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-1 my-2">
                                        <div class="form-group">
                                            <button class="btn btn-outline-primary btn-md-block btn-block" style="height: 38px;"><i  class="mx-2 mdi mdi-magnify"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 my-3">
                            <div class="form-group">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle" rowspan="3"></th>
                                                    <th class="align-middle" rowspan="3">Periode</th>
                                                    <th class="align-middle" rowspan="3">User</th>
                                                    <th class="align-middle" rowspan="3">Wilayah</th>
                                                    <th class="align-middle" rowspan="3">Komoditas</th>
                                                    <th class="align-middle" rowspan="3">Varietas</th>
                                                    <th class="align-middle" rowspan="3">Umur (HST)</th>
                                                    <th class="align-middle" rowspan="3">Luas Tanam (HA)</th>
                                                    <th class="align-middle" rowspan="3">Luas Waspada</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;" colspan="6">Sisa Periode sebelumnya/Perubahan Kriteria</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;" colspan="5">Luas Tambah pada Periode Laporan (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;" colspan="5">Luas Keadaan pada Periode Laporan (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;" colspan="2">Penangangan</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;" rowspan="2">Titik Koordinat Puso (Desa)</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;" rowspan="2">Keterangan</th>
                                                  
                                                </tr>
                                                <tr>
                                                    <th class="text-center">R</th>
                                                    <th class="text-center">S</th>
                                                    <th class="text-center">B</th>
                                                    <th class="text-center">P</th>
                                                    <th class="text-center">Pulih</th>
                                                    <th class="text-center">J</th>
                                                    <th class="text-center">R</th>
                                                    <th class="text-center">S</th>
                                                    <th class="text-center">B</th>
                                                    <th class="text-center">P</th>
                                                    <th class="text-center">J</th>
                                                    <th class="text-center">R</th>
                                                    <th class="text-center">S</th>
                                                    <th class="text-center">B</th>
                                                    <th class="text-center">P</th>
                                                    <th class="text-center">J</th>
                                                    <th class="text-center">Upaya</th>
                                                    <th class="text-center">Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($reports as $item)
                                                    <tr>
                                                        <td>
                                                            @can('input-data.edit')
                                                                <a href="{{ route('admin.report.holtikultura.kekeringan.edit', $item->id) }}" class="btn btn-primary btn-sm btn-block">
                                                                EDIT
                                                                </a>
                                                            @endcan

                                                            <br>
                                                            
                                                            @can('input-data.delete')
                                                                <a href="{{ route('admin.report.holtikultura.kekeringan.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                                    <button class="btn btn-danger btn-sm btn-block"> Hapus</button>
                                                                </a>
                                                            @endcan
                                                        </td>

                                                        <td class="text-center">{{ date_view($item->periode)}}</td>
                                                        <td class="text-center">{{ $item->user->name }}</td>
                                                        <td>Yogyakarta, {{ $item->village->subDistrict->district->name.', '. $item->village->subDistrict->name.', '.$item->village->name }}</td>
                                                        <td class="text-center">{{ $item->commodity->name }}</td>
                                                        <td class="text-center">{{ $item->varieties }}</td>
                                                        <td class="text-center">{{ $item->age }}</td>
                                                        <td class="text-center">{{ $item->planting_area }}</td>
                                                        <td class="text-center">{{ $item->broadly_alert ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->previous_periode_low ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->previous_periode_mid ?? '-' }}</td>
                                                        <td class="text-center">{{ $item->previous_periode_hight ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->previous_periode_puso ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->previous_periode_recover ?? '-'  }}</td>
                                                        <td class="text-center">{{ 
                                                             $item->previous_periode_low  +
                                                             $item->previous_periode_mid  +
                                                             $item->previous_periode_hight  +
                                                             $item->previous_periode_puso  +
                                                             $item->previous_periode_recover  
                                                        }}</td>
                                                        <td class="text-center">{{ $item->area_added_periode_low ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->area_added_periode_mid ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->area_added_periode_hight ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->area_added_periode_puso ?? '-'  }}</td>
                                                        <td class="text-center">{{ 
                                                             $item->area_added_periode_low  +
                                                             $item->area_added_periode_mid  +
                                                             $item->area_added_periode_hight  +
                                                             $item->area_added_periode_puso  

                                                        }}</td>
                                                        <td class="text-center">{{ $item->area_state_periode_low ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->area_state_periode_mid ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->area_state_periode_hight ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->area_state_periode_puso ?? '-'  }}</td>
                                                        <td class="text-center">{{
                                                            $item->area_state_periode_low +
                                                            $item->area_state_periode_mid +
                                                            $item->area_state_periode_hight +
                                                            $item->area_state_periode_puso 
                                                        }}</td>
                                                        <td class="text-center">{{ $item->handling_effort ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->handling_effort_total ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->coordinate ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->description ?? '-'  }}</td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="27">Data Empty</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $reports->links('layouts.pagination')  !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2({
        placeholder: "Kategori",
    });
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

</script>
@endsection
