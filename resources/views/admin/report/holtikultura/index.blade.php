@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            @include('admin.report.holtikultura.__header')
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">                        
                        <div class="col-12 my-2">
                            <form action="">
                                <div class="row">
                                    <div class="col-lg-4 my-1">
                                        <a href="{{ route('admin.report.holtikultura.print', request()->all()) }}" target="_blank" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-print"></span> Print
                                         </a>
                                         <a href="{{ route('admin.report.holtikultura.pdf', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-pdf"></span> Pdf
                                         </a>
                                        <a href="{{ route('admin.report.holtikultura.xls', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-excel"></span> Xls
                                         </a>
                                         <a href="{{ route('admin.report.holtikultura.csv', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-file-excel"></span> CSV
                                        </a>
                                    </div>
                                    <div class="col-lg-3 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker-autoclose" name="date" placeholder="Periode" value="{{ Request::get('date') }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-4 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Cari User">
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-1 my-2">
                                        <div class="form-group">
                                            <button class="btn btn-outline-primary btn-md-block btn-block" style="height: 38px;"><i  class="mx-2 mdi mdi-magnify"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 my-3">
                            <div class="form-group">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle" rowspan="3"></th>
                                                    <th class="align-middle" rowspan="2">Periode</th>
                                                    <th class="align-middle" rowspan="2">User</th>
                                                    <th class="align-middle" rowspan="2">Wilayah</th>
                                                    <th class="align-middle" rowspan="2">Komoditas</th>
                                                    <th class="align-middle" rowspan="2">OPT</th>
                                                    <th class="align-middle" rowspan="2">Periode TB</th>
                                                    <th class="align-middle" rowspan="2">Luas Tanam (HA)</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;" colspan="5">Luas Sisa Serangan (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 200px;" rowspan="2">Luas Terkendali (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 200px;" colspan="5">Luas Tambah Serangan (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 200px;" colspan="5" >Luas Keadaan Serangan (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 200px;" colspan="5" >Luas Keadaan Pengendalian (Ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 200px;" rowspan="2">Tanaman Terancam (Ha)</th>
                                                </tr>
                                                <tr>
                                                    <th>R</th>
                                                    <th>S</th>
                                                    <th>B</th>
                                                    <th>P</th>
                                                    <th>J</th>
                                                    <th>R</th>
                                                    <th>S</th>
                                                    <th>B</th>
                                                    <th>P</th>
                                                    <th>J</th>
                                                    <th>R</th>
                                                    <th>S</th>
                                                    <th>B</th>
                                                    <th>P</th>
                                                    <th>J</th>
                                                    <th>Pem</th>
                                                    <th>Pest</th>
                                                    <th>CL</th>
                                                    <th>APH</th>
                                                    <th>J</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($reports as $item)
                                                <tr>
                                                    <td>
                                                        @can('input-data.edit')
                                                            <a href="{{ route('admin.report.holtikultura.edit', $item->id) }}" class="btn btn-primary btn-sm btn-block">Edit</a>
                                                        @endcan
                                                        
                                                        <br>
                                                        
                                                        @can('input-data.delete')
                                                            <a href="{{ route('admin.report.holtikultura.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                                <button class="btn btn-danger btn-sm btn-block"> Hapus</button>
                                                            </a>
                                                        @endcan
                                                    </td>
                                                    <td>{{ date_view($item->periode) }}</td>
                                                    <td>{{ $item->user->name  }}</td>
                                                    <td>Yogyakarta, {{ $item->village->subDistrict->district->name.', '. $item->village->subDistrict->name.', '.$item->village->name }}</td>
                                                    <td class="text-center">{{ $item->commodity->name }}</td>
                                                    <td class="text-center">{{ optional($item->opt)->name }}</td>
                                                    <td class="text-center">{{ $item->periode_tb ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->planting_area ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->attact_area_r ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->attact_area_s ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->attact_area_b ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->attact_area_p ?? '-' }}</td>
                                                    <td class="text-center">{{
                                                         $item->attact_area_r +
                                                         $item->attact_area_s +
                                                         $item->attact_area_b +
                                                         $item->attact_area_p 
                                                    }}</td>
                                                    <td class="text-center">{{ $item->under_control ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->added_area_r ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->added_area_s ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->added_area_b ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->added_area_p ?? '-' }}</td>
                                                    <td class="text-center">{{
                                                        $item->added_area_r +
                                                        $item->added_area_s +
                                                        $item->added_area_b +
                                                        $item->added_area_p 
                                                    }}</td>
                                                    <td class="text-center">{{ $item->state_area_r ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->state_area_s ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->state_area_b ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->state_area_p ?? '-' }}</td>
                                                    <td class="text-center">{{
                                                        $item->state_area_r +
                                                        $item->state_area_s +
                                                        $item->state_area_b +
                                                        $item->state_area_p 
                                                    
                                                    }}</td>
                                                    <td class="text-center">{{ $item->control_area_pem ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->control_area_pest ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->control_area_cl ?? '-' }}</td>
                                                    <td class="text-center">{{ $item->control_area_aph ?? '-' }}</td>
                                                    <td class="text-center">{{ 
                                                        $item->control_area_pem +
                                                        $item->control_area_pest +
                                                        $item->control_area_cl +
                                                        $item->control_area_aph 
                                                        
                                                    }}</td>
                                                    <td class="text-center">{{ $item->threatened_plant ?? '-' }}</td>
                                                <tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="29">Data Empty</td>
                                                    </tr>
                                                @endforelse
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $reports->links('layouts.pagination')  !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2({
        placeholder: "Kategori",
    });
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

</script>
@endsection
