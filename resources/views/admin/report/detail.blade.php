@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
    $user = logged_in_user();
@endphp


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item">
                        Input Data
                    </li>
                    <li class="breadcrumb-item active">
                        Preview Data
                    </li>
                </ol>
                <h4>Preview Data</h4>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20" id="printMe">
                <div class="card-body">
                    <div class="col-12 text-right">
                        <a href="{{ route('admin.report.detail-print', $report->id) }}" target="_blank" class="btn btn-md-block btn-primary my-2">
                           <span class="fa fa-print"></span> Print
                        </a>
                        <a href="{{ route('admin.report.detail-print-pdf', $report->id) }}" class="btn btn-md-block btn-primary my-2">
                            <span class="fa fa-file-pdf"></span> Download Pdf
                        </a>
                        {{-- <a href="#" class="btn btn-md-block btn-primary my-2">
                            <span class="fa fa-file-excel"></span> Download Xls
                        </a> --}}
                    </div>
                    <div class="form-group print-area">
                        <table class="table table-bordered table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td width="50%">Nama</td>
                                    <td>{{ ($report->user)->name }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>Yogyakarta</td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td>{{ optional($report->subDistrict)->name }}</td>
                                </tr>
                                <tr>
                                    <td>Periode</td>
                                    <td>{{ date_view($report->date) }}</td>
                                </tr>
                                <tr>
                                    <td>Kategori</td>
                                    <td>{{ TypeCommodity::label($report->type_commodity) }}</td>
                                </tr>
                                <tr>
                                    <td>Komoditas</td>
                                    <td>{{ $report->commodity->name }}</td>
                                </tr>
                                <tr>
                                    @php
                                       $opt = optional($report->optDatas)->map(function($q){
                                                                return $q->opt->name;
                                                            })->toArray();
                                    @endphp
                                    <td>OPT</td>
                                    <td>{{ implode(', ', $opt) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Intensitas Serangan</label>
                            <div class="form-group">
                                <table class="table table-bordered table-striped mb-0">
                                    <tbody>
                                        <tr>
                                            <td width="50%">Ringan</td>
                                            <td>{{ $report->itensity_attack_easy }}</td>
                                        </tr>
                                        <tr>
                                            <td>Berat</td>
                                            <td>{{ $report->itensity_attack_hard }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Luas Pengendalian</label>
                            <div class="form-group">
                                <table class="table table-bordered table-striped mb-0">
                                    <tbody>
                                        <tr>
                                            <td width="50%">ABPN</td>
                                            <td>{{ $report->control_area_apbn }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">APBD I</td>
                                            <td>{{ $report->control_area_apbd_1 }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">APBD II</td>
                                            <td>{{ $report->control_area_apbd_2 }}</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">Masyarakat</td>
                                            <td>{{ $report->control_area_apbd_public }}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Cara Pengendalian</label>
                            <div class="form-group">
                                <table class="table table-bordered table-striped mb-0">
                                    <tbody>
                                        <tr>
                                            <td width="50%">
                                                @php
                                                        $pengendalian = optional($report->controlAreas)->map(function($q){
                                                            return ControlArea::label($q->control_area);
                                                        })->toArray();
                                                @endphp
                                                {{ implode(', ', $pengendalian) }}
                                            </td>
                                            <td>{{ $report->control_area_other ?? '-' }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="control-label text-uppercase font-14 mb-3 font-bold label-green">Harga rata-rata / ton</label>
                            <div class="form-group">
                                <table class="table table-bordered table-striped mb-0">
                                    <tbody>
                                        <tr>
                                            <td>Rp. {{ $report->price_average }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script language="javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>


</div> <!-- container-fluid -->
@endsection

@push('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();
    jQuery('#datepicker-autoclose').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

</script>
@endpush
