@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            @include('admin.report.perkebunan.__header')
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">                        
                        <div class="col-12 my-2">
                            <form action="">
                                <div class="row">
                                    <div class="col-lg-4 my-1">
                                        <a href="{{ route('admin.report.perkebunan.print', request()->all()) }}" target="_blank" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-print"></span> Print
                                         </a>
                                         <a href="{{ route('admin.report.perkebunan.pdf', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-pdf"></span> Pdf
                                         </a>
                                        <a href="{{ route('admin.report.perkebunan.xls', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-excel"></span> Xls
                                         </a>
                                         <a href="{{ route('admin.report.perkebunan.csv', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-file-excel"></span> CSV
                                        </a>
                                    </div>
                                    <div class="col-lg-3 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker-autoclose" name="date" placeholder="Periode" value="{{ Request::get('date') }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-4 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Cari User">
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-1 my-2">
                                        <div class="form-group">
                                            <button class="btn btn-outline-primary btn-md-block btn-block" style="height: 38px;"><i  class="mx-2 mdi mdi-magnify"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 my-3">
                            <div class="form-group">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle"  rowspan="2"></th>
                                                    <th class="align-middle" rowspan="2">Periode</th>
                                                    <th class="align-middle" rowspan="2">User</th>
                                                    <th class="align-middle" rowspan="2">Kabupaten</th>
                                                    <th class="align-middle" rowspan="2">Kecamatan</th>
                                                    <th class="align-middle" rowspan="2">Tanaman</th>
                                                    <th class="align-middle" rowspan="2">OPT</th>
                                                    <th class="align-middle" rowspan="2" style="min-width: 170px;">Luas
                                                        Pertanaman (ha)</th>
                                                    <th class="align-middle text-center" style="min-width: 170px;"
                                                        colspan="2">Intensitas Serangan</th>
                                                    <th class="align-middle" rowspan="2">Jumlah</th>
                                                    <th class="align-middle text-center" colspan="4">Luas Pengendalian
                                                    </th>
                                                    <th class="align-middle" rowspan="2">Jumlah</th>
                                                    <th class="align-middle" rowspan="2">Cara Pengendalian</th>
                                                    <th class="align-middle" rowspan="2" style="min-width: 200px;">Harga rata - rata/Ton (Rp)</th>
                                                </tr>
                                                <tr>
                                                    <th>Ringan</th>
                                                    <th>Berat</th>

                                                    <th>apbn</th>
                                                    <th>apbd I</th>
                                                    <th>apbd II</th>
                                                    <th>Masyarakat</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($reports as $item)
                                                <tr>
                                                    <td>
                                                        @can('input-data.edit')
                                                            <a href="{{ route('admin.report.perkebunan.edit', $item->id) }}" class="btn btn-primary btn-sm btn-block">Edit</a>
                                                        @endcan
                                                        <br>
                                                        @can('input-data.delete')
                                                            <a href="{{ route('admin.report.perkebunan.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                                <button class="btn btn-danger btn-sm btn-block"> Hapus</button>
                                                            </a>
                                                        @endcan
                                                    </td>
                                                    <td>{{ date_view($item->date) }}</td>
                                                    <td>{{ optional($item->user)->name ?? '-' }}</td>
                                                    <td>{{ optional($item->subDistrict)->district->name }}</td>
                                                    <td>{{ optional($item->subDistrict)->name }}</td>
                                                    <td>{{ optional($item->commodity)->name }}</td>
                                                    <td>
                                                        @php
                                                            $opt = optional($item->optDatas)->map(function($q){
                                                                return $q->opt->name;
                                                            })->toArray();
                                                        @endphp
                                                        
                                                        {{ $opt ? implode(', ', $opt) : '-' }}
                                                    </td>
                                                    <td>{{ $item->planting_area }}</td>
                                                    <td>{{ $item->itensity_attack_easy }}</td>
                                                    <td>{{ $item->itensity_attack_hard }}</td>
                                                    <td>{{  $item->itensity_attack_easy +  $item->itensity_attack_hard }}</td>
                                                    <td>{{ $item->control_area_apbn }}</td>
                                                    <td>{{ $item->control_area_apbd_1 }}</td>
                                                    <td>{{ $item->control_area_apbd_2 }}</td>
                                                    <td>{{ $item->control_area_apbd_public }}</td>
                                                    <td>
                                                        {{
                                                            $item->control_area_apbn 
                                                            +
                                                            $item->control_area_apbd_1
                                                            +
                                                            $item->control_area_apbd_2
                                                            +
                                                            $item->control_area_apbd_public
                                                        }}
                                                    </td>
                                                    <td>
                                                        @php
                                                            $pengendalian = optional($item->controlAreas)->map(function($q){
                                                                return ControlArea::label($q->control_area);
                                                            })->toArray();
                                                        @endphp
                                                        
                                                        {{ $pengendalian ? implode(', ', $pengendalian) : '-' }}
                                                    </td>
                                                    <td>{{ $item->price_average }}</td>
                                                <tr>
                                                @empty
                                                <tr>
                                                    <td colspan="10">Data Empty</td>
                                                </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $reports->links('layouts.pagination')  !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2({
        placeholder: "Kategori",
    });
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

</script>
@endsection
