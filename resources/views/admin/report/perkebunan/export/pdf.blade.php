<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
</head>
<body>

    
    <style>
    .page-break {
        page-break-after: always;
    }
    </style>
    <style>
        @page { 
            size: legal landscape; 
        }
        align-middle{vertical-align:middle!important}

        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;

        }
        .text-center{
            text-align: center;
        }
        </style>
        <style type="text/css" media="print">
            @page { size: landscape; }
        </style>
          
    @php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp
    <table id="tech-companies-1" class="table table-striped">
        <thead>
            <tr>
                {{-- <th class="align-middle" rowspan="3"></th> --}}
                <th class="align-middle" rowspan="2">Periode</th>
                <th class="align-middle" rowspan="2">User</th>
                <th class="align-middle" rowspan="2">Wilayah</th>
                <th class="align-middle" rowspan="2">Komoditas</th>
                <th class="align-middle" rowspan="2">OPT</th>
                <th class="align-middle" rowspan="2">Periode TB</th>
                <th class="align-middle" rowspan="2">Luas Tanam (HA)</th>
                <th class="align-middle text-center" style="min-width: 170px;" colspan="5">Luas Sisa Serangan (Ha)</th>
                <th class="align-middle text-center" style="min-width: 200px;" rowspan="2">Luas Terkendali (Ha)</th>
                <th class="align-middle text-center" style="min-width: 200px;" colspan="5">Luas Tambah Serangan (Ha)</th>
                <th class="align-middle text-center" style="min-width: 200px;" colspan="5" >Luas Keadaan Serangan (Ha)</th>
                <th class="align-middle text-center" style="min-width: 200px;" colspan="5" >Luas Keadaan Pengendalian (Ha)</th>
                <th class="align-middle text-center" style="min-width: 200px;" rowspan="2">Tanaman Terancam (Ha)</th>
            </tr>
            <tr>
                <th>R</th>
                <th>S</th>
                <th>B</th>
                <th>P</th>
                <th>J</th>
                <th>R</th>
                <th>S</th>
                <th>B</th>
                <th>P</th>
                <th>J</th>
                <th>R</th>
                <th>S</th>
                <th>B</th>
                <th>P</th>
                <th>J</th>
                <th>R</th>
                <th>S</th>
                <th>B</th>
                <th>P</th>
                <th>J</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($reports as $item)
            <tr>
                {{-- <td>
                    <a href="{{ route('admin.report.detail', $item->id) }}">
                        <span class="fa fa-eye"></span>
                    </a>
                </td> --}}
                <td>{{ date_view($item->periode) }}</td>
                <td>{{ $item->user->name  }}</td>
                <td>Yogyakarta, {{ $item->village->subDistrict->district->name.', '. $item->village->subDistrict->name.', '.$item->village->name }}</td>
                <td class="text-center">{{ $item->commodity->name }}</td>
                <td class="text-center">{{ $item->opt->name }}</td>
                <td class="text-center">{{ $item->periode_tb ?? '-' }}</td>
                <td class="text-center">{{ $item->planting_area ?? '-' }}</td>
                <td class="text-center">{{ $item->attact_area_r ?? '-' }}</td>
                <td class="text-center">{{ $item->attact_area_s ?? '-' }}</td>
                <td class="text-center">{{ $item->attact_area_b ?? '-' }}</td>
                <td class="text-center">{{ $item->attact_area_p ?? '-' }}</td>
                <td class="text-center">{{ $item->attact_area_j ?? '-' }}</td>
                <td class="text-center">{{ $item->under_control ?? '-' }}</td>
                <td class="text-center">{{ $item->added_area_r ?? '-' }}</td>
                <td class="text-center">{{ $item->added_area_s ?? '-' }}</td>
                <td class="text-center">{{ $item->added_area_b ?? '-' }}</td>
                <td class="text-center">{{ $item->added_area_p ?? '-' }}</td>
                <td class="text-center">{{ $item->added_area_j ?? '-' }}</td>
                <td class="text-center">{{ $item->state_area_r ?? '-' }}</td>
                <td class="text-center">{{ $item->state_area_s ?? '-' }}</td>
                <td class="text-center">{{ $item->state_area_b ?? '-' }}</td>
                <td class="text-center">{{ $item->state_area_p ?? '-' }}</td>
                <td class="text-center">{{ $item->state_area_j ?? '-' }}</td>
                <td class="text-center">{{ $item->control_area_pem ?? '-' }}</td>
                <td class="text-center">{{ $item->control_area_pest ?? '-' }}</td>
                <td class="text-center">{{ $item->control_area_cl ?? '-' }}</td>
                <td class="text-center">{{ $item->control_area_aph ?? '-' }}</td>
                <td class="text-center">{{ $item->control_area_j ?? '-' }}</td>
                <td class="text-center">{{ $item->threatened_plant ?? '-' }}</td>
            <tr>
            @empty
                <tr>
                    <td colspan="29">Data Empty</td>
                </tr>
            @endforelse
        </tbody>
    </table> 
</body>

@if (Request::routeIs('admin.report.print'))
    <script>
        window.print();
    </script>
@endif
</html>