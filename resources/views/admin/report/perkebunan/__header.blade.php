<div class="page-title-box">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            Home
        </li>
        <li class="breadcrumb-item active">
            Report
        </li>
    </ol>
    <h4>Halaman Laporan Perkebunan</h4>
    <a href="{{ route('admin.report.perkebunan.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.perkebunan.index') ? 'btn-success' : 'btn-primary' }}">Data Opt</button>
    </a>
    <a href="{{ route('admin.report.perkebunan.dpi.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.perkebunan.dpi.*') ? 'btn-success' : 'btn-primary' }}">Data Opt DPI</button>
    </a>
    <a href="{{ route('admin.report.perkebunan.lkk.index') }}">
        <button class="btn {{ Request::routeIs('admin.report.perkebunan.lkk.*') ? 'btn-success' : 'btn-primary' }}">Data Opt LKK</button>
    </a>
</div>