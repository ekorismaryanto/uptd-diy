<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
</head>
<body>

    
    <style>
    .page-break {
        page-break-after: always;
    }
    </style>
    <style>
        @page { 
            size: legal landscape; 
        }
        align-middle{vertical-align:middle!important}

        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;

        }
        .text-center{
            text-align: center;
        }
        </style>
        <style type="text/css" media="print">
            @page { size: landscape; }
        </style>
          
    @php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp
    <table id="tech-companies-1" class="table table-striped">
        <thead>
            <tr>
                <th class="align-middle">Periode</th>
                <th class="align-middle">User</th>
                <th class="align-middle">Wilayah</th>
                <th class="align-middle">Komoditas</th>
                <th class="align-middle">Luas (Ha)</th>
                <th class="align-middle">Umur (HST)</th>
                <th class="align-middle">Satuan Umur</th>
                <th class="align-middle">Luas Serangan (ha)</th>
                <th class="align-middle">Deskripsi OPT</th>
                <th class="align-middle">Foto Comtoh Spesimen OPT</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($reports as $item)
                <tr>
                    <td class="text-center">{{ date_view($item->periode)}}</td>
                    <td class="text-center">{{ $item->user->name }}</td>
                    <td>Yogyakarta, {{ 
                        $item->subDistrict->name.', '. $item->subDistrict->district->name }}</td>
                    <td class="text-center">{{ $item->commodity->name ?? '-' }}</td>
                    <td class="text-center">{{ $item->planting_area }}</td>
                    <td class="text-center">{{ $item->age }}</td>
                    <td class="text-center">{{ $item->age_unit }}</td>
                    <td class="text-center">{{ $item->attact_area ?? '-'  }}</td>
                    <td class="text-center">{{ $item->deskripsi_opt ?? '-'  }}</td>
                    <td class="text-center">{{ $item->image ?? '-'  }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="10">Data Empty</td>
                </tr>
            @endforelse
        </tbody>
    </table> 
</body>

{{-- @if (Request::routeIs('admin.report.print')) --}}
    <script>
        window.print();
    </script>
{{-- @endif --}}
</html>