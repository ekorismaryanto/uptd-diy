<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
</head>
<body>

    
    <style>
    .page-break {
        page-break-after: always;
    }
    </style>
    <style>
        @page { 
            size: legal landscape; 
        }
        align-middle{vertical-align:middle!important}

        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;

        }
        .text-center{
            text-align: center;
        }
        </style>
        <style type="text/css" media="print">
            @page { size: landscape; }
        </style>
          
    @php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp
    <table id="tech-companies-1" class="table table-striped">
        <thead>
            <tr>
                <th class="align-middle">Periode</th>
                <th class="align-middle">User</th>
                <th class="align-middle">Wilayah</th>
                <th class="align-middle">Komoditas</th>
                <th class="align-middle">Luas (Ha)</th>
                <th class="align-middle">Umur (HST)</th>
                <th class="align-middle">Satuan Umur</th>
                <th class="align-middle">Kering Terkena (ha)</th>
                <th class="align-middle">Kering Ringan (ha)</th>
                <th class="align-middle">Kering Penanganan</th>
                <th class="align-middle">Banjir Terkena (ha)</th>
                <th class="align-middle">Banjir Ringan (ha)</th>
                <th class="align-middle">Banjir Penanganan</th>
                <th class="align-middle">Kebakaran Terkena (ha)</th>
                <th class="align-middle">Kebakaran Ringan (ha)</th>
                <th class="align-middle">Kebakaran Penanganan</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($reports as $item)
                <tr>
                    <td class="text-center">{{ date_view($item->periode)}}</td>
                    <td class="text-center">{{ $item->user->name }}</td>
                    <td>Yogyakarta, {{ 
                        $item->subDistrict->name.', '. $item->subDistrict->district->name }}</td>
                    <td class="text-center">{{ $item->commodity->name ?? '-' }}</td>
                    <td class="text-center">{{ $item->planting_area }}</td>
                    <td class="text-center">{{ $item->age }}</td>
                    <td class="text-center">{{ $item->age_unit }}</td>
                    <td class="text-center">{{ $item->dry_exposed ?? '-'  }}</td>
                    <td class="text-center">{{ $item->dry_lightly ?? '-'  }}</td>
                    <td class="text-center">{{ $item->dry_handling ?? '-'  }}</td>
                    <td class="text-center">{{ $item->flood_hit ?? '-' }}</td>
                    <td class="text-center">{{ $item->light_flood ?? '-'  }}</td>
                    <td class="text-center">{{ $item->flood_handling ?? '-'  }}</td>
                    <td class="text-center">{{ $item->fire_hit ?? '-'  }}</td>
                    <td class="text-center">{{ $item->light_fire ?? '-'  }}</td>
                    <td class="text-center">{{ $item->fire_handling ?? '-'  }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="10">Data Empty</td>
                </tr>
            @endforelse
        </tbody>
    </table> 
</body>

@if (Request::routeIs('admin.report.holtikultura.banjir.print'))
    <style>
        @page {
            size: A4;
            margin: 0;
            size: landscape;
            
            }
            @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            size: landscape;
            }
            /* ... the rest of the rules ... */
        }
    </style>
    <script>
        window.print();
    </script>
@endif
</html>