@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@php
    use \App\Http\Constants\TypeCommodity;
    use \App\Http\Constants\ControlArea;
@endphp

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            @include('admin.report.perkebunan.__header') 
        <div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">                        
                        <div class="col-12 my-2">
                            <form action="">
                                <div class="row">
                                    <div class="col-lg-4 my-1">
                                        <a href="{{ route('admin.report.perkebunan.dpi.print', request()->all()) }}" target="_blank" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-print"></span> Print
                                         </a>
                                         <a href="{{ route('admin.report.perkebunan.dpi.pdf', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-pdf"></span> Pdf
                                         </a>
                                        <a href="{{ route('admin.report.perkebunan.dpi.xls', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                             <span class="fa fa-file-excel"></span> Xls
                                         </a>
                                         <a href="{{ route('admin.report.perkebunan.dpi.csv', request()->all()) }}" class="btn btn-md-block btn-primary my-2">
                                            <span class="fa fa-file-excel"></span> CSV
                                        </a>
                                    </div>
                                    <div class="col-lg-3 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker-autoclose" name="date" placeholder="Periode" value="{{ Request::get('date') }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-4 my-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Cari User">
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="col-lg-1 my-2">
                                        <div class="form-group">
                                            <button class="btn btn-outline-primary btn-md-block btn-block" style="height: 38px;"><i  class="mx-2 mdi mdi-magnify"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 my-3">
                            <div class="form-group">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle"></th>
                                                    <th class="align-middle">Periode</th>
                                                    <th class="align-middle">User</th>
                                                    <th class="align-middle">Wilayah</th>
                                                    <th class="align-middle">Komoditas</th>
                                                    <th class="align-middle">Luas (Ha)</th>
                                                    <th class="align-middle">Umur (HST)</th>
                                                    <th class="align-middle">Satuan Umur</th>
                                                    <th class="align-middle">Kering Terkena (ha)</th>
                                                    <th class="align-middle">Kering Ringan (ha)</th>
                                                    <th class="align-middle">Kering Penanganan</th>
                                                    <th class="align-middle">Banjir Terkena (ha)</th>
                                                    <th class="align-middle">Banjir Ringan (ha)</th>
                                                    <th class="align-middle">Banjir Penanganan</th>
                                                    <th class="align-middle">Kebakaran Terkena (ha)</th>
                                                    <th class="align-middle">Kebakaran Ringan (ha)</th>
                                                    <th class="align-middle">Kebakaran Penanganan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($reports as $item)
                                                    <tr>
                                                        <td>
                                                            @can('input-data.edit')
                                                                <a href="{{ route('admin.report.perkebunan.dpi.edit', $item->id) }}" class="btn btn-primary btn-sm btn-block">Edit</a>
                                                            @endcan
   
                                                            <br>
   
                                                            @can('input-data.delete')
                                                                <a href="{{ route('admin.report.perkebunan.dpi.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                                    <button class="btn btn-danger btn-sm btn-block"> Hapus</button>
                                                                </a>
                                                            @endcan
                                                        </td>
                                                        <td class="text-center">{{ date_view($item->periode)}}</td>
                                                        <td class="text-center">{{ $item->user->name }}</td>
                                                        <td>Yogyakarta, {{ 
                                                            $item->subDistrict->name.', '. $item->subDistrict->district->name }}</td>
                                                        <td class="text-center">{{ $item->commodity->name ?? '-' }}</td>
                                                        <td class="text-center">{{ $item->planting_area }}</td>
                                                        <td class="text-center">{{ $item->age }}</td>
                                                        <td class="text-center">{{ $item->age_unit }}</td>
                                                        <td class="text-center">{{ $item->dry_exposed ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->dry_lightly ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->dry_handling ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->flood_hit ?? '-' }}</td>
                                                        <td class="text-center">{{ $item->light_flood ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->flood_handling ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->fire_hit ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->light_fire ?? '-'  }}</td>
                                                        <td class="text-center">{{ $item->fire_handling ?? '-'  }}</td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="10">Data Empty</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $reports->links('layouts.pagination')  !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2({
        placeholder: "Kategori",
    });
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });

</script>
@endsection
