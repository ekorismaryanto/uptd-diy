@extends('layouts.master')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}"
    rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<!-- C3 charts css -->
<link href="{{ URL::asset('assets/plugins/c3/c3.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Home
                    </li>
                    <li class="breadcrumb-item">
                        Dashboard
                    </li>
                    <li class="breadcrumb-item active">
                        Detail
                    </li>
                </ol>
                <h4 class="text-uppercase font-bold">Detail Statistik - {{ $district->name }} </h4>
                <h6 class="text-primary font-bold text-uppercase"></h6>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="card m-b-20">
                <div class="card-body">
                    <h5 class="mt-0 font-bold text-uppercase text-center">Data OPT</h5>
                    <div id="chart-opt"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card m-b-20">
                <div class="card-body">
                    <h5 class="mt-0 font-bold text-uppercase text-center">Data Komoditas</h5>
                    <div id="chart-komoditas"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h5>User Input Data</h5>
                    <div class="row">
                        <div class="col-12 my-3">
                            <div class="form-group">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th class="text-center">Total Input</th>
                                                </tr>
                                                @forelse ($member as $item)
                                                    <tr>
                                                        <td>
                                                            <img class="rounded-circle z-depth-2" width="50px" alt="100x100" src="https://i.pinimg.com/736x/89/90/48/899048ab0cc455154006fdb9676964b3.jpg" data-holder-rendered="true">
                                                            {{ $item->name}}</td>
                                                        <td class="text-center">{{ $item->total}}</td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="2" class="text-center">Data Empty</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection

@section('script')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<!--C3 Chart-->
<script src="{{ URL::asset('assets/plugins/d3/d3.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/c3/c3.min.js')}}"></script>
<script>
    // Select2
    $(".select2").select2();
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        minViewMode: 1,
        format: "yyyy-mm"
    });


    ! function ($) {
        "use strict";

        var ChartC3 = function () {};
        ChartC3.prototype.init = function () {
                //generating chart 
                c3.generate({
                    bindto: '#chart-opt',
                    data: {
                        x: 'x_data_opt',
                        columns: [
                            ['x_data_opt', 
                                @foreach($opt as $row)
                                    '{{ $row->name }}',
                                @endforeach
                            ],
                            ['OPT', 
                                @foreach($opt as $row)
                                    {{ $row->total }},
                                @endforeach
                            ],
                        ],
                        type: 'bar',
                        colors: {
                            OPT: '#0b2e2f',
                        }
                    },
                    axis: {
                        x: {
                            type: 'category' // this needed to load string x value
                        }
                    },
                    legend: {
                        show: false
                    }
                });

                c3.generate({
                    bindto: '#chart-komoditas',
                    data: {
                        x: 'x_data_komoditas',
                        columns: [
                            ['x_data_komoditas', 
                                @foreach($commodity as $row)
                                    '{{ $row->name }}',
                                @endforeach
                            ],
                            ['Komoditas', 
                                @foreach($commodity as $row)
                                    '{{ $row->total }}',
                                @endforeach
                            ],
                        ],
                        type: 'bar',
                        colors: {
                            Komoditas: '#1a7749',
                        }
                    },
                    axis: {
                        x: {
                            type: 'category' // this needed to load string x value
                        }
                    },
                    legend: {
                        show: false
                    }
                });
            },
            $.ChartC3 = new ChartC3, $.ChartC3.Constructor = ChartC3
    }(window.jQuery),

    //initializing 
    function ($) {
        "use strict";
        $.ChartC3.init()
    }(window.jQuery);

</script>
@endsection
