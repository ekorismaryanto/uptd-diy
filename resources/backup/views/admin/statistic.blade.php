@extends('theme.master')

@section('contents')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Statistik</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Statistik</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @foreach (resolve(\App\Repositories\Entities\District::class)->get() as $item)
            
                <div class="col-lg-4 col-12">
                    <div class="small-box bg-white">
                        <div class="inner location">
                            <div class="d-flex justify-content-end">
                                <img src="{{ asset('assets/admin/assets/img/location.png') }}" width="50px">
                            </div>
                            <p style="font-size: 100%; font-weight:normal">{{ $item->name }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight: normal">OPT</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body" style="height:350px">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
            <!-- Default box -->
            {{-- <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Team Member</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <!-- <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button> -->
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 d-flex align-items-center mb-2 mt-2">
                            <img height="80px" src="https://img.pngio.com/circlegreysphereovalbeige-4635958-free-png-library-gray-circle-png-800_800.png">
                            <div class="info-member ml-3">
                                <h5><b>Ardy</b></h5>
                                <p class="text-secondary" ><b>20 task submited</b></p>
                            </div>
                        </div>
                        <div class="col-lg-3 d-flex align-items-center mb-2 mt-2">
                            <img height="80px" src="https://img.pngio.com/circlegreysphereovalbeige-4635958-free-png-library-gray-circle-png-800_800.png">
                            <div class="info-member ml-3">
                                <h5><b>Ardy</b></h5>
                                <p class="text-secondary" ><b>20 task submited</b></p>
                            </div>
                        </div>
                        <div class="col-lg-3 d-flex align-items-center mb-2 mt-2">
                            <img height="80px" src="https://img.pngio.com/circlegreysphereovalbeige-4635958-free-png-library-gray-circle-png-800_800.png">
                            <div class="info-member ml-3">
                                <h5><b>Ardy</b></h5>
                                <p class="text-secondary" ><b>20 task submited</b></p>
                            </div>
                        </div>
                        <div class="col-lg-3 d-flex align-items-center mb-2 mt-2">
                            <img height="80px" src="https://img.pngio.com/circlegreysphereovalbeige-4635958-free-png-library-gray-circle-png-800_800.png">
                            <div class="info-member ml-3">
                                <h5><b>Ardy</b></h5>
                                <p class="text-secondary" ><b>20 task submited</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- /.card -->
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection

@push('styles')
<style>

.col-xs-1-5 {
  width: 19.99995%;
}
</style>
    
@endpush

@push('scripts')
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
  
      // The data for our dataset
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
          label: 'My First dataset',
          backgroundColor: 'rgba(0,255,36,255)',
          borderColor: 'rgba(0,255,36,255)',
          data: [25, 10, 5, 2, 20, 30, 45]
        }]
      },
  
      // Configuration options go here
      options: {
        responsive: true,
        maintainAspectRatio: false
      }
    });
  </script>
@endpush
