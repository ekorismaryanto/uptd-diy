@extends('theme.master')

@php
    use \App\Http\Constants\CategoryDamage;
    use \App\Http\Constants\TypeCommodity;
@endphp

@section('contents')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4>Selamat Datang</h4>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"  style="font-weight: normal;">Home</a></li>
                        <li class="breadcrumb-item active">Welcome</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <marquee>
                                Gunakan menu disamping untuk menggunakan sistem ini
                            </marquee>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
