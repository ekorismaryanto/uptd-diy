@extends('theme.master')

@php
    use \App\Http\Constants\CategoryDamage;
    use \App\Http\Constants\TypeCommodity;
@endphp

@section('contents')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Data master - Komoditas</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data master - Komoditas</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <form action="">
                                <div class="row">
                                    <div class="col-12 col-sm-2">
                                        <div class="form-group">
                                            <div class="input-group input-group" id="reservationdate" data-target-input="nearest">
                                                <input type="text" name="date" class="form-control datepicker datetimepicker-input" data-target="#reservationdate"/>
                                                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <div class="form-group">
                                            <select class="form-control select2" id="district" name="district" >
                                                <option value="" selected="selected">Semua Kabupaten</option>
                                                @foreach (resolve(\App\Repositories\Entities\District::class)->get() as $item)
                                                <option value="{{ $item->id }}" {{ Request::get('district') == $item->id ? 'selected' : '' }} >{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-12 col-sm-2">
                                        <div class="form-group">
                                            <select class="form-control select2 sub-district" id="sub-district" name="sub_district" placeholder="Kecamatan">
                                                <option value="" selected>Kecamatan</option>
                                                @foreach (resolve(\App\Repositories\Entities\SubDistrict::class)->get() as $item)
                                                <option value="{{ $item->id }}" {{ Request::get('sub_district') == $item->id ? 'selected' : '' }} >{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <div class="form-group">
                                            <select class="form-control select2" name="damage" placeholder="Kecamatan">
                                                <option selected="selected" value="">Semua Kerusakan</option>
                                                @foreach (CategoryDamage::labels() as $key => $item)
                                                    <option value="{{ $key }}" {{ Request::get('damage') == $key ? 'selected' : '' }} >{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <div class="form-group">
                                            <select class="form-control select2" name="type" placeholder="Kecamatan">
                                                <option value="" selected="selected">Semua Type</option>
                                                @foreach (TypeCommodity::labels() as $key => $item)
                                                    <option value="{{ $key }}" {{ Request::get('type') == $key ? 'selected' : '' }} >{{ $item }}</option>
                                                @endforeach
                                               
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <div class="form-group">
                                            <div class="input-group input-group">
                                                <input type="text" name="q" value="{{ Request::get('q') }}" id="Cari User" class="form-control pull-right" placeholder="Search">
                                                <div class="input-group-btn">
                                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <button class="btn btn-primary btn-sm"><span class="fa fa-print"></span> Export Excel Semua Data</button>
                                        <button class="btn btn-primary btn-sm"><span class="fa fa-print"></span> Export Excel Data Yang terfilter</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive table-striped no-padding">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>No</th>
                                        <th>User</th>
                                        <th class="text-center">Periode Lapor</th>
                                        <th>Kabupaten</th>
                                        <th>Kecamatan</th>
                                        <th>Komoditas</th>
                                        <th>Type</th>
                                        <th class="text-center">Kategori Kerusakan</th>
                                        <th>Luas Tanah (Ha)</th>
                                        <th>Luas Kerusakan (Ha)</th>
                                        <th>Luas Pengendalian (Ha)</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    @forelse ($reports as $key => $item)
                                        <tr>
                                            <td>{{ $key+=1 }}</td>
                                            <td>{{ $item->user->name }}</td>
                                            <td>{{ date_view($item->date) }}</td>
                                            <td>{{ $item->subDistrict->district->name }}</td>
                                            <td>{{ $item->subDistrict->name }}</td>
                                            <td>{{ $item->commodity->name }}</td>
                                            <td>{{ TypeCommodity::label($item->type_commodity) }}</td>
                                            <td>{{ CategoryDamage::label($item->category_damage) }}</td>
                                            <td>{{$item->surface_area }}</td>
                                            <td>{{$item->area_of_damage }}</td>
                                            <td>{{$item->control_area }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.report.detail', $item->id) }}">
                                                    <button class="btn btn-sm btn-warning"><span class="fa fa-eye"></span> Detail</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="12" class="text-center">Data Empty</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            {!! $reports->links()  !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin-1/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid rgb(206, 204, 204);
        border-radius: 4px;
        padding-bottom: 29px;
        }
    </style>
@endpush

@push('scripts')
<script src="{{ asset('assets/admin-1/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.sub-district').select2({
            allowClear:true,
            placeholder :'Pilih Kecamatan'
        });

        @if(Request::get('sub_district'))
            $('.sub-district').select2({
                    allowClear:true,
                    placeholder: 'Pilih kecamatan',
                    ajax: {
                        url: '{{ route('admin.json.sub-district') }}',
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                q: params.term,
                                district_id: '{{ Request::get('district') }}',
                                page: params.page
                            }
                            return query;
                        },
                        delay: 250,
                        processResults: function (data) {
                            return data;
                        },
                        cache: true
                    }
                })
        @endif
        
        $('#district').on('change', function(){
            $(".sub-district").val('').trigger('change')
            var district_id = $(this).val();
            $('.sub-district').select2({
                placeholder: 'Pilih kecamatan',
                ajax: {
                    url: '{{ route('admin.json.sub-district') }}',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            q: params.term,
                            district_id: district_id,
                            page: params.page
                        }
                        return query;
                    },
                    delay: 250,
                    processResults: function (data) {
                        return data;
                    },
                    cache: true
                }
            })
        });

        $(".datepicker").datepicker({ 
            autoclose: true,
            orientation : 'bottom'
        });

    });
</script>
@endpush
