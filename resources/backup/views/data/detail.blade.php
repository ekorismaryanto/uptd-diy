@extends('theme.master')

@php
    use \App\Http\Constants\CategoryDamage;
    use \App\Http\Constants\TypeCommodity;   
    if (Request::routeIs('admin.data.crops.*')) {
        $route = 'admin.data.crops.';
        $typeCommodity = TypeCommodity::CROPS;
        $label = TypeCommodity::label(1);
    }else if (Request::routeIs('admin.data.holticulture.*')) {
        $route = 'admin.data.holticulture.';
        $typeCommodity = TypeCommodity::HOLTICULTURE;
        $label = TypeCommodity::label(2);
    }else{
        $route = 'admin.data.plantation.';
        $typeCommodity = TypeCommodity::PLANTATION;
        $label = TypeCommodity::label(3);
    }
@endphp

@section('contents')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4>Data - {{ $label }}</h4>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data master - {{ $label }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
<section class="content">
    <div class="card card-default">
        <div class="card-header with-border">
            <h3 class="card-title">Detail</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th style="width: 50%" >Periode Pelaporan</th>
                                <td>{{ date_view($report->date) }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Kabupaten</th>
                                <td>{{  $report->subDistrict->district->name }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Kecamatan</th>
                                <td> {{$report->subDistrict->name}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Komoditas</th>
                                <td>{{ TypeCommodity::label($report->type_commodity) }}</td>
                            </tr>
                            <tr>
                                @php
                                    $opt = $report->optDatas;

                                    foreach ($opt->where('type_opt', 1) as $key => $value) {
                                        $opt1[] = $value->opt->name;
                                    }

                                    foreach ($opt->where('type_opt', 2) as $key => $value) {
                                        $opt2[] = $value->opt->name;
                                    }

                                    foreach ($opt->where('type_opt', 3) as $key => $value) {
                                        $opt3[] = $value->opt->name;
                                    }

                                    foreach ($opt->where('type_opt', 4) as $key => $value) {
                                        $opt4[] = $value->opt->name;
                                    }
                                @endphp
                                <th scope="row">OPT Utama</th>
                                <td>
                                    {{ implode(', ', $opt1)}}.
                                    
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Kerusakan Mutlak</th>
                                <td>
                                    {{ implode(', ', $opt2)}}.
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Kerusakan Tidak Mutlak</th>
                                <td>
                                    {{ implode(', ', $opt3)}}.
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">kategori Kerusakan</th>
                                <td>
                                    {{ implode(', ', $opt4)}}.
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Luas Tanah</th>
                                <td> {{ $report->surface_area }} </td>
                            </tr>
                            <tr>
                                <th scope="row">Luas Kerusakan</th>
                                <td> {{ $report->area_of_damage }} </td>
                            </tr>
                            <tr>
                                <th scope="row">Luas Pengendalian</th>
                                <td> {{ $report->control_area }} </td>
                            </tr>
                        </tbody>
                    </table>                            
                </div>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ route($route.'index') }}" class="btn btn-default pull-right">
                Kembali
            </a>
        </div>
    </div>
</section>
@endsection
@push('styles')
@endpush

@push('scripts')

@endpush
