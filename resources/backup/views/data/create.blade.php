@extends('theme.master')

@php
    use \App\Http\Constants\CategoryDamage;
    use \App\Http\Constants\TypeCommodity;   
    if (Request::routeIs('admin.data.crops.*')) {
        $route = 'admin.data.crops.';
        $typeCommodity = TypeCommodity::CROPS;
        $label = TypeCommodity::label(1);
    }else if (Request::routeIs('admin.data.holticulture.*')) {
        $route = 'admin.data.holticulture.';
        $typeCommodity = TypeCommodity::HOLTICULTURE;
        $label = TypeCommodity::label(2);
    }else{
        $route = 'admin.data.plantation.';
        $typeCommodity = TypeCommodity::PLANTATION;
        $label = TypeCommodity::label(3);
    }
@endphp

@section('contents')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4>Data - {{ $label }}</h4>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data master - {{ $label }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <section class="content d-flex justify-content-center mt-5" >
        <div class="container-fluid mt-1 my-content ">
            <form action="{{ route($route.'store') }}" method="POST">
                @csrf
                <input type="hidden" value="{{ $typeCommodity }}" name="type_commodity">

            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-4 d-flex flex-column align-items-center">
                    <div class="custom-input" data-target-input="nearest" >
                        <input class="form-select" id="reservationdate" name="date" required type="text" placeholder="Bulan/Tahun" data-target="#reservationdate" data-toggle="datetimepicker">
                        <i class="nav-icon fas fa-calendar-alt"></i>
                    </div>
                    <div class="mycustom-select">
                        <select class="form-control select2 district" required id="district" name="district" required data-placeholder="Pilih Kabupaten">
                            <option value="">Pilih Kabupaten</option>
                            @foreach ($districts as $key => $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>  
                    <div class="mycustom-select">
                        <select class="form-control sub-district" required name="sub_district_id" data-placeholder="Pilih Kecamatan">
                        </select>
                    </div>  
                    <div class="mycustom-select">
                        <select class="form-control select2" name="commodity_id" data-placeholder="Pilih Komoditas" required>
                            <option value="">Pilih Komoditas</option>
                            @foreach ($commodities as $key => $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>  
                </div>
                <div class="col-xs-12 col-md-6 col-lg-4 d-flex flex-column align-items-center">
                    <div class="mycustom-select">
                        <select class="form-control opt" name="opt[]" data-placeholder="Pilih kategori OPT Utama" multiple></select>
                    </div>
                    <div class="custom-input">
                        <select class="form-control opt" name="opt_absolute[]" data-placeholder="Pilih Kerusakan Mutlak" multiple></select>
                    </div>
                    <div class="custom-input">
                        <select class="form-control opt" name="opt_non_absolute[]" data-placeholder="Pilih Kerusakan Tidak Mutlak" multiple></select>
                    </div>
                    <div class="custom-input">
                        <select class="form-control opt" name="opt_other[]" data-placeholder="Pilih Kerusakan Lain" multiple></select>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-4 d-flex flex-column align-items-center">
                    <div class="mycustom-select">
                        <select class="select2"  name="category_damage" required data-placeholder="Kategori Kerusakan" style="width: 100%;">
                            <option value="">Pilih Kategori Kerusakan</option>
                            @foreach (CategoryDamage::labels() as $key => $item)
                                <option value="{{ $key }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="custom-input">
                        <input type="number" step="any" class="form-select" required name="surface_area" required placeholder="Luas Tanah (Ha)">
                    </div>
                    <div class="custom-input">
                        <input  type="number" step="any" class="form-select" required name="area_of_damage" placeholder="Luas Kerusakan">
                    </div>
                    <div class="custom-input">
                        <input  type="number" step="any" class="form-select" required name="control_area" placeholder="Luas Pengendalian">
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex justify-content-end ">
                <a href="{{ route($route.'index') }}" class="btn btn-lg btn-default pull-right">
                    Kembali
                </a>
                <button type="submit" class="btn btn-lg btn-success pull-right" style="margin-left: 15px">Simpan</button>
            </div>
            </form>
            
        </div>
    </section>
</div>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
         color: rgb(114, 101, 101) !important;
     }
     input:focus{
        outline: none;
        color: black;
    }
    .select2-container .select2-selection--single {
    background-color: white;
    box-shadow: 0 0 1px rgba(0, 0, 0, 0.125), 0 1px 3px rgba(0, 0, 0, 0.2);
    border-radius: 1.8rem;
    position: relative;
    z-index: 100;
    margin-bottom: 30px;
    color: #6767;
    padding: 1.4rem 2rem 2.4rem;
    border: 1px solid transparent;
    border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
    cursor: pointer;
}

.select2-container--default
    .select2-selection--single
    .select2-selection__arrow {
    height: 26px;
    position: absolute;
    top: calc(2.4rem / 2);
    right: 1px;
    width: 20px;
}

.select2-container--default
    .select2-selection--single
    .select2-selection__arrow
    b {
    border-color: #08fb00 transparent transparent transparent;
    border-style: solid;
    border-width: 6px 6px 0 6px;
    height: 0;
    left: 50%;
    margin-left: -13px;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
}

.select2-container--default
    .select2-selection--single
    .select2-selection__rendered {
    color: rgba(44, 66, 44, 0.822);
}

.select2-container--default.select2-container--open
    .select2-selection--single
    .select2-selection__arrow
    b {
    border-color: transparent transparent #06f157 transparent;
    border-width: 0 6px 6px 6px;
}

.select2-container--default .select2-selection--multiple {
    background-color: white;
    box-shadow: 0 0 1px rgba(0, 0, 0, 0.125), 0 1px 3px rgba(0, 0, 0, 0.2);
    border-radius: 1.8rem;
    position: relative;
    z-index: 100;
    margin-bottom: 30px;
    color: #6767;
    padding: 0.75rem 2rem;
    border: 1px solid transparent;
    border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
    cursor: pointer;
}

.select2-container--default
    .select2-selection--multiple
    .select2-selection__rendered
    li:first-child.select2-search.select2-search--inline
    .select2-search__field::placeholder {
    color: #6767;
}

 </style>
<script>
    $(document).ready(function() {
        $('#reservationdate').datetimepicker({
            format: 'DD-MM-yyyy'
        });
        $('.select2').select2();
        $('.sub-district').select2();
        
        $('#district').select2().on('change', function(){
            var district_id = $(this).val();
            $('.sub-district').select2({
                placeholder: 'Pilih Jabatan',
                ajax: {
                    url: '{{ route('admin.json.sub-district') }}',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            q: params.term,
                            district_id: district_id,
                            page: params.page
                        }
                        return query;
                    },
                    delay: 250,
                    processResults: function (data) {
                        return data;
                    },
                    cache: true
                }
            })
        });

        $('.opt').select2({
            placeholder: 'Pilih OPT',
            ajax: {
                url: '{{ route('admin.json.opt') }}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page
                    }
                    return query;
                },
                delay: 250,
                processResults: function (data) {
                    return data;
                },
                cache: true
            }
        })

        $(".datepicker").datepicker({ 
            autoclose: true,
            orientation : 'bottom'
        });

    });
</script>
@endpush
