@extends('theme.master')

@php
    use \App\Http\Constants\TypeCommodity;    
@endphp

@section('contents')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Data master - Komoditas</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data master - Komoditas</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <form action="">
                            <div class="card-header">
                                <a href="{{ route('admin.datamaster.commodity.create') }}" class="btn btn-xs btn-success">Tambah Data</a>
                                <div class="card-tools mr-1 mt-3 col-md-2">
                                    <div class="input-group input-group-sm" >
                                        <input type="text" name="q" class="form-control pull-right" value="{{ Request::get('q') }}" placeholder="Search">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive table-striped no-padding">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th style="width: 50%">Nama</th>
                                        <th style="width: 0%">Type</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    @forelse ($commodities as $item)
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ TypeCommodity::label($item->type) }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.datamaster.commodity.edit', $item->id) }}">
                                                    <button class="btn btn-primary"> Edit</button>
                                                </a>
                                                <br>
                                                <a href="{{ route('admin.datamaster.commodity.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                    <button class="btn btn-danger"> Hapus</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="2" class="text-center">Data Empty</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            {!! $commodities->links()  !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
