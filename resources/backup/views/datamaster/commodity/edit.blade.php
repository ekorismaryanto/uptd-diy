@extends('theme.master')
@php
use \App\Http\Constants\TypeCommodity;    
@endphp
@section('contents')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Data master - OPT</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data master - OPT</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header with-border">
                    <h3 class="card-title">Input Data OPT</h3>
                </div>
                <form action="{{ route('admin.datamaster.commodity.update', $commodity->id) }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="name" class="form-control" value="{{ $commodity->name }}" placeholder="Input Nama" required>
                                    @error('name')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Tipe Komoditas</label>
                                    <select name="type" class="form-control" id="" required>
                                      
                                        <option value="" disabled selected>Pilih Komoditas</option>
                                        @foreach (TypeCommodity::labels() as $key => $item)
                                            <option value="{{ $key }}" {{ $commodity->type == $key ? 'selected' : ''  }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                    @error('type')
                                        <div style="color: red">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info btn-sm pull-right" style="margin-left: 5px">Simpan</button>
                        <a href="{{ route('admin.datamaster.commodity.index') }}" class="btn btn-sm btn-default pull-right">
                            Kembali
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection

@push('scripts')
@endpush
