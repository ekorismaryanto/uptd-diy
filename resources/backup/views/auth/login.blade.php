<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UPTD BPTD | Dinas Pertanian DIY</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('assets/admin-1/') }}/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin-1/') }}/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin-1/') }}/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin-1/') }}/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin-1') }}/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="{{ asset('assets/toast/toastr.min.css') }}">
    @stack('styles')
    <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" style="
    background-size: cover;
    width: 100%;
    background-repeat: no-repeat;
background-image: url('{{ asset('assets/homepage/assets/img/sawah.jpg') }}')">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>UPTD BPTD</b> DIY</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Silahkan Login</p>
            <form action="{{ route('auth.login.do-login') }}" method="post">
                @csrf
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="nip" placeholder="NIP" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @error('nip')
                        <div style="color: red">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @error('password')
                        <div style="color: red">{{ $message }}</div>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                </div>
            </form>
            <div class="social-auth-links text-center">
                <a href="#">Lupa Password</a><br>
            </div>
        </div>
    </div>
    <!-- /.login-box -->
    
    <!-- jQuery 3 -->
    <script src="{{ asset('assets/admin') }}/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="{{ asset('assets/admin') }}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{ asset('assets/admin') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ asset('assets/admin') }}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="{{ asset('assets/admin') }}/bower_components/fastclick/lib/fastclick.js"></script>
    <script src="{{ asset('assets/admin') }}/dist/js/adminlte.min.js"></script>
    <script src="{{ asset('assets/admin') }}/dist/js/demo.js"></script>
    <script src="{{ asset('assets/toast/toastr.min.js') }}"></script>
    <script>
        toastr.options = {
            "progressBar": true
        };
    </script>
    @stack('toastr')
    @if(session()->has("notice"))
        @php
            $value = Session::get('notice');
        @endphp
        @if (is_array($value))
            <script>
                @foreach ($value as $data)
                    @if ($data['labelClass'] == 'success')
                        toastr["success"]("{{ $data['content'] }}");
                    @endif
                    @if ($data['labelClass'] == 'error')
                        toastr["error"]("{{ $data['content'] }}");
                    @endif
                @endforeach
            </script>
        @endif
        @php
            Session::forget('notice');
        @endphp
    @endif
    @stack('scripts')
</body>
</html>
