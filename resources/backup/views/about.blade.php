<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('assets/admin') }}/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin') }}/assets/style.css">
    <link
      rel="stylesheet"
      href="{{ URL::asset('assets/css/animate.min.css')}}"
    />
    <title>Document</title>
</head>
    <body id="tentang" >
        <div class="container">
            <div class="title text-center">
                <h3>Tentang Kami</h3>
            </div>
            <div class="box-green shadow rounded">
                <div class="row p-5 d-flex align-items-center ">
                    <div class="col-md-4 mb-3">
                        <h3 class="animate__animated animate__fadeInUp" >UPTD</h3>
                        <h3 class="animate__animated animate__fadeInUp" >BPTD DIY</h3>
                    </div>
                    <div class="col-md-8 ">
                        <p class="animate__animated animate__fadeIn" >UPT  Balai Proteksi Tanaman Pertanian  (BPTP) merupakan salah saw Unit Pelaksana Teknis  Dinas
                            UPTD (UPTD) di lingkup Dinas Pertanian dan Ketahanan Pangan Daerah Istimewa Yogyakarta.  Berdasarkan
                            Pergub DIY No.96 Tahun 2018, UPT BPTP mempunyai tugas melaksanakan proteksi  tanaman BPTP DIY
                            pangan, hortikultura dan perkebunan untuk meningkatkan persentase pertanaman  aman  dari 
                            serangan Organisme Pengganggu Tumbuhan (OPT) dan dampak perubahan  iklim  (DPI)</p>
                    </div>
                </div>
            </div>
            <div class="sejarah row">
                <div class="col-md-4 justify-content-center mb-3">
                    <h3 class="animate__animated animate__fadeInUp" >Sejarah</h3>
                    <h3 class="animate__animated animate__fadeInUp" >BPTD DIY</h3>
                </div>
                <div class="col-md-8">
                    <p class="animate__animated animate__fadeIn" >
                        Sejarah Pada awal berdirinya kelembagaan perlindungan tanaman tahun 1987, UPT BPTP DIY merupakan
                        UPT Ditjen Pertanian Tanaman Pangan yang berada di wilayah kerja Balai Proteksi Tanaman Pangan
                        BPTP DIY
                        (BPTP) V Jawa Tengah dengan nama Satuan Tugas (Satgas) BPTP V Propinsi DIY. Kemudian
                        berdasarkan SK Dirjen Tanaman Pangan dan Hortikultura Nomor 1.11.050.96.14 Tahun 1996 terjadi
                        perubahan nama dan i Satgas BPTP V Propinsi DIY menjadi Satgas Balai Proteksi Tanaman Pangan
                        dan Hortikultura (BPTPH) V Propinsi DIY yang masih tetap merupakan wilayah kerja BPTPH V Jawa
                        tengah.
                        <br>
                        <br>
                        Sejalan dengan perkembangan kebijakan otonomi daerah sesuai dengan UU Nomor 22 Tahun 1999
                        dan PP Nomor 25 Tahun 2000, maka dibuat perda DIY no. 5 th 2001 yang mengatur tentang
                        Pembentukan Organisasi Dinas Daerah yang menjelaskan Organisasi Dinas Pertanian. Mengingat
                        BPTPH merupakan Satgas pusat dengan lingkup propinsi, maka BPTPH menjadi wewenang propinsi.
                        Melalui Perda DIY no. 7 th 2002 dijelaskan bahwa BPTPH dibawah Dinas Pertanian. Selanjutnya pada
                        tahun 2008 berdasrkan Perda DIY no. 6 Tahun 2008 yang ditindaklanjuti dengan Pergub DIY no 44
                        Tahun 2008 nama BPTPH berubah menjadi Balai Proteksi Tanaman Pertanin (BPTP). Pada tahun 2018
                        terjadi perubahan Organisasi Perangkat Daerah DIY, dengan Pergub DIY no. 60 dan ditindak lanjuti
                        dengan Pergub 96 tahun 2018 memasukkan perlindungan tanaman perkebunan masuk kedalam
                        tugas fungsi BPTP Dinas Pertanian dan Ketahanan Pangan DIY.</p>
                </div>
            </div>
            <div class="box-green rounded mb-5 shadow">
                <div class="row p-5 d-flex align-items-stratch ">
                    <div class="col-md-6 col-lg-4">
                        <h3 class="animate__animated animate__fadeInUp" >Lab Pengendalian</h3>
                        <h3 class="animate__animated animate__fadeInUp" >Hama Penyakit</h3>
                        <h3 class="animate__animated animate__fadeInUp" >Tanaman (LPHPT)</h3>
                    </div>
                    <div class="col-md-6 col-lg-8 logo_m">
                        <img class="animate__animated animate__fadeInUp" id="mikroskop" src="{{ asset('assets/admin') }}/assets/img/mikroskop.png">
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>