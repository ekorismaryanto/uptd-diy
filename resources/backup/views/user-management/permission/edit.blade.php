@extends('theme.master')

@section('contents')
<section class="content-header">
    <h1>
        User Management
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Permission</a></li>
        <li class="active">Edit Permission</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Data Permission</h3>
        </div>
        <form action="{{ route('admin.user-management.permission.update', $permission->id) }}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="name" class="form-control" value="{{ $permission->name }}" placeholder="Input Nama" required>
                            @error('name')
                                <div style="color: red">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right" style="margin-left: 5px">Simpan</button>
                <a href="{{ route('admin.user-management.permission.index') }}" class="btn btn-default pull-right">
                    Kembali
                </a>
            </div>
        </form>
    </div>
</section>
@endsection
