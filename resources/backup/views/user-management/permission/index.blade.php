@extends('theme.master')

@section('contents')
<section class="content-header">
    <h1>
        User Management
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Permission</a></li>
        <li class="active">Data Permission</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <form action="">
                    <div class="box-header">
                    <a href="{{ route('admin.user-management.permission.create') }}" class="btn btn-xs btn-success">Tambah Data</a>
                        <div class="box-tools">
                            <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                                <input type="text" name="q" class="form-control pull-right" value="{{ Request::get('q') }}" placeholder="Search">
                                
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /.box-header -->
                <div class="box-body table-responsive table-striped no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th style="width: 80%">Nama</th>
                                <th class="text-center">Action</th>
                            </tr>
                            @forelse ($roles as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.user-management.permission.edit', $item->id) }}">
                                            <button class="btn btn-xs btn-primary"><span class="fa fa-pencil"></span> Edit</button>
                                        </a>
                                        <a href="{{ route('admin.user-management.permission.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                            <button class="btn btn-xs btn-danger"><span class="fa fa-trash"></span> Hapus</button>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Data Empty</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    {!! $permissions->links()  !!}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection
