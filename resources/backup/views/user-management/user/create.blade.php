@extends('theme.master')

@section('contents')
<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>User Management</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User Management</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card card-success">
                        <div class="card-header with-border">
                            <h3 class="card-title">Input Data user</h3>
                        </div>
                        <form action="{{ route('admin.user-management.user.store') }}" method="POST">
                            @csrf
                            <div class="card-body" style="font-weight: normal !important">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" name="name" class="form-control" placeholder="Input Nama" required>
                                            @error('name')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>NIP</label>
                                            <input type="text" name="nip" class="form-control" placeholder="Input NIP" required>
                                            @error('nip')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ESELON</label>
                                            <input type="text" name="eselon" class="form-control" placeholder="Input eselon" required>
                                            @error('eselon')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Golongan</label>
                                            <input type="text" name="type" class="form-control" placeholder="Input Golongan" required>
                                            @error('type')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>TMT Golongan:</label>
                                            <input type="text" name="tmt_type" class="form-control pull-right datepicker" id="datepicker" readonly="readonly" style="background-color: white" placeholder="Input Periode">
                                            @error('tmt_type')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Jabatan</label>
                                            <input type="text" name="position" class="form-control" placeholder="Input Jabatan" required>
                                            @error('position')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tempat Kerja/Wilayah Kerja</label>
                                            <input type="text" name="work_location" class="input-sm form-control" placeholder="Input Tempat Kerja/Wilayah Kerja" required>
                                            @error('work_location')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Status Karyawan</label>
                                            <select name="status_employee" class="form-control" id="">
                                                <option value="1">PNS</option>
                                                <option value="0">Bukan PNS</option>
                                            </select>
                                            @error('status_employee')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nomer Hp</label>
                                            <input type="text" name="no_hp" class="form-control" placeholder="Input No Hp" required>
                                            @error('no_hp')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" name="email" class="form-control" placeholder="Input Email" required>
                                            @error('email')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control" placeholder="Input Password" required>
                                            @error('password')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Roles</label>
                                            <select class="form-control select2" name="roles[]" multiple="multiple" data-placeholder="Pilih Role" style="width: 100%;">
                                                @foreach ($roles as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-info pull-right" style="margin-left: 5px">Simpan</button>
                                <a href="{{ route('admin.user-management.user.index') }}" class="btn btn-sm btn-default pull-right">
                                    Kembali
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</section>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/admin-1/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin-1/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .form-group{
            font-size: 14px !important;
        }
        .form-control{
            font-size: 13px !important;
        }
        .form-group label{
            font-weight: normal !important;
        }
       .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #05F750 !important;
            color: black !important;
            border: 1px solid #efd1d1;
            border-radius: 4px;
            cursor: default;
            float: left;
            margin-right: 5px;
            margin-top: 5px;
            padding: 0 5px;
            font-weight: normal;
        }
    </style>
    
@endpush

@push('scripts')
<script src="{{ asset('assets/admin-1/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin-1/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2();

        $(".datepicker").datepicker({ 
			dateFormat: 'dd-M-yy',
			firstDay: 1,
			changeMonth: true,
			changeYear: true,												
			//showButtonPanel: true,
			showOn: 'both', 
			buttonImage: 'application/images/calendar_icon.png',
			buttonImageOnly: true 
			});

    });
</script>
@endpush
