@extends('theme.master')

@section('contents')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>User Management</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User Management</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <form action="">
                            <div class="card-header row">
                                <div class="col mt-3">
                                    <a href="{{ route('admin.user-management.user.create') }}" class="btn btn-xs btn-success">
                                        Tambah Data
                                    </a>
                                </div>
                                <div class="card-tools mr-1 mt-3 col-md-2">
                                    <div class="input-group input-group-sm" >
                                        <input type="text" name="q" class="form-control pull-right" value="{{ Request::get('q') }}" placeholder="Search">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover" style="font-wight:normal">
                                <tbody>
                                <tr class="active">
                                    <th>No</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Eselon</th>
                                    <th>Golongan</th>
                                    <th>TMT Golongan</th>
                                    <th>Jabatan</th>
                                    <th>Tempat Kerja/Wilayah Kerja</th>
                                    <th></th>
                                </tr>
                                @forelse ($users as $key => $item)
                                    <tr>
                                        <td>{{ $key+=1 }}</td>
                                        <td>{{ $item->nip }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->eselon }}</td>
                                        <td>{{ $item->type }}</td>
                                        <td>{{ date_view($item->tmt_tye) }}</td>
                                        <td>{{ $item->position }}</td>
                                        <td>{{ $item->work_location }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.user-management.user.edit', $item->id) }}" style="margin-bottom: 50px">
                                                <button class="btn btn-block btn-xs btn-primary" style="margin-bottom: 4px"> Edit</button>
                                            </a>
                                            <a href="{{ route('admin.user-management.user.delete', $item->id) }}" onclick="return confirm('Anda Yakin Ingin menghapus ?');">
                                                <button class="btn btn-block btn-xs btn-danger"> Hapus</button>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="8">Data Kosong</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            {!! $users->links()  !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

@endsection
