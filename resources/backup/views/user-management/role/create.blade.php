@extends('theme.master')

@section('contents')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4>Role Management</h4>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Role Management</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <form action="{{ route('admin.user-management.role.store') }}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="name" class="input-sm form-control" placeholder="Input Nama Role" required>
                                            @error('name')
                                                <div style="color: red">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-box">
                                            <span><input type="checkbox" id="checkAll"> Permission</span>
                                            <br>
                                            @foreach($permissions as $permission)
                                                <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"> <span>{{ $permission->name }}</span><br>
                                            @endforeach
                                            @if($errors->has('permissions'))
                                                <div class="text-danger">{{ $errors->first('permissions') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-info pull-right" style="margin-left: 5px">Simpan</button>
                                <a href="{{ route('admin.user-management.role.index') }}" class="btn btn-sm btn-default pull-right">
                                    Kembali
                                </a>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
</script>
@endpush