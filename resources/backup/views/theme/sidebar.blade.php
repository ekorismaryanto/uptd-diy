@php
    use \App\Http\Constants\TypeCommodity;    
@endphp
<aside class="main-sidebar bg-green-custom elevation-4">
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex flex-column justify-content-center align-items-center">
            <div class="image mb-3">
                <img src="https://static.vecteezy.com/system/resources/thumbnails/000/439/863/small/Basic_Ui__28186_29.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info text-center">
                <a href="#" class="d-block">{{ logged_in_user()->name }}</a>
                <a href="#" class="d-block">{{ logged_in_user()->nip }}</a>
            </div>
        </div>
        
        <nav class="mt-2 sidebar-custom">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">Main Navigation</li>
                <li class="nav-item">
                    <a href="admin.html" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Overview
                            <!-- <span class="right badge badge-danger">New</span> -->
                        </p>
                    </a>
                </li>
                
                @can('data.tanaman-pangan','data.tanaman-holtikultura','data.perkebunan')
                    <li class="nav-item {{ Request::routeIs('admin.data.*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-chart-pie"></i>
                            <p>
                                Data
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('data.tanaman-pangan')
                                <li class="nav-item {{ Request::routeIs('admin.data.crops.*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.data.crops.index') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{ TypeCommodity::label(1) }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('data.holtikultura')
                                <li class="nav-item {{ Request::routeIs('admin.data.holticulture.*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.data.holticulture.index') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{ TypeCommodity::label(2) }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('data.perkebunan')
                                <li class="nav-item {{ Request::routeIs('admin.data.plantation.*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.data.plantation.index') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{ TypeCommodity::label(3) }}</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                
                @can('statistik')
                    <li class="nav-item {{ Request::routeIs('admin.statistic.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.statistic.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-chart-bar"></i>
                            <p>
                                Statistik
                                <!-- <span class="right badge badge-danger">New</span> -->
                            </p>
                        </a>
                    </li>
                @endcan
                
                @can('report')
                    <li class="nav-item {{ Request::routeIs('admin.report.*') ? 'active' : '' }}">
                        <a href="{{ route('admin.report.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                Reports
                                <!-- <span class="right badge badge-danger">New</span> -->
                            </p>
                        </a>
                    </li>
                @endcan
                
                @can('datamaster.opt','datamaster.komoditas')
                <li class="nav-header">Data Master</li>
                    @can('datamaster.komoditas')
                        <li class="nav-item {{ Request::routeIs('admin.datamaster.commodity.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.datamaster.commodity.index') }}" class="nav-link">
                                <i class="nav-icon fas fa-th"></i>
                                <p>
                                    Komoditas
                                    <!-- <span class="right badge badge-danger">New</span> -->
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('datamaster.opt')
                        <li class="nav-item {{ Request::routeIs('admin.datamaster.distraction.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.datamaster.distraction.index') }}" class="nav-link">
                                <i class="nav-icon fas fa-th"></i>
                                <p>
                                    OPT
                                    <!-- <span class="right badge badge-danger">New</span> -->
                                </p>
                            </a>
                        </li>
                    @endcan
                @endcan
                
                @can('user-management.user','user-management.role')
                    <li class="nav-header">User Management</li>
                    @can('user-management.user')
                        <li class="nav-item {{ Request::routeIs('admin.user-management.user.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.user-management.user.index') }}" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    User/Karyawan
                                    <!-- <span class="right badge badge-danger">New</span> -->
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('user-management.role')
                        <li class="nav-item {{ Request::routeIs('admin.user-management.role.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.user-management.role.index') }}" class="nav-link">
                                <i class="nav-icon fas fa-user-secret"></i>
                                <p>
                                    Role
                                    <!-- <span class="right badge badge-danger">New</span> -->
                                </p>
                            </a>
                        </li>
                    @endcan
                @endcan
            </ul>
        </nav>
    </div>
</aside>