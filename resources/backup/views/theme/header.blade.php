<nav class="main-header navbar navbar-expand navbar-pink navbar-light">

    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">UPTD BPTD Dinas Pertanian DIY</a>
        </li>
    </ul>
    
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        
        <!-- Notifications Dropdown Menu -->
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="https://static.vecteezy.com/system/resources/thumbnails/000/439/863/small/Basic_Ui__28186_29.jpg" class="user-image" alt="User Image">
                <span class="hidden-xs">{{ logged_in_user()->name }}</span>
            </a>
            <ul class="dropdown-menu" style="width: 200px;right:0px;">
                <li class="user-footer">
                    <div class="pull-left">
                        <a href="{{ route('admin.profile') }}" class="btn btn-block btn-primary btn-sm btn-flat">Profile</a>
                    </div>
                    <hr>
                    <div class="pull-right">
                        <a href="{{ route('auth.login.sign-out') }}" class="btn btn-block btn-danger btn-sm btn-flat">Sign out</a>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</nav>