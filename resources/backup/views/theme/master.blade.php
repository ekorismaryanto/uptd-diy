<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UPTD BPTD DIY</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('assets/admin') }}/assets/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin') }}/assets/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin') }}/assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin') }}/assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin') }}/assets/admin.css">
    <link rel="stylesheet" href="{{ asset('assets/toast/toastr.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
  @stack('styles')
  <style>
       .form-group{
            font-size: 14px !important;
        }
        .form-control{
            font-size: 13px !important;
        }
        .form-group label{
            font-weight: normal !important;
        }
  </style>

</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        {{-- header --}}
            @include('theme.header')
        {{-- header --}}
        <!-- Main Sidebar Container -->
            <!-- Sidebar -->
                @include('theme.sidebar')
            {{-- sidebar --}}
            
            <!-- Content Wrapper. Contains page content -->
            @yield('contents')
            <!-- /.content-wrapper -->
       
        </div>
    <script src="{{ asset('assets/admin') }}/assets/plugins/jquery/jquery.min.js"></script>
    <script src="{{ asset('assets/admin') }}/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('assets/admin') }}/assets/dist/js/adminlte.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="{{ asset('assets/admin') }}/assets/plugins/moment/moment.min.js"></script>
    <script src="{{ asset('assets/admin') }}/assets/plugins/inputmask/jquery.inputmask.min.js"></script>
    <script src="{{ asset('assets/admin') }}/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="{{ asset('assets/admin') }}/assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="{{ asset('assets/toast/toastr.min.js') }}"></script>
    <script>
        toastr.options = {
            "progressBar": true
        };
    </script>
    @stack('toastr')
    @if(session()->has("notice"))
        @php
            $value = Session::get('notice');
        @endphp
        @if (is_array($value))
            <script>
                @foreach ($value as $data)
                    @if ($data['labelClass'] == 'success')
                        toastr["success"]("{{ $data['content'] }}");
                    @endif
                    @if ($data['labelClass'] == 'error')
                        toastr["error"]("{{ $data['content'] }}");
                    @endif
                @endforeach
            </script>
        @endif
        @php
            Session::forget('notice');
        @endphp
    @endif
    @stack('scripts')
    <script>
        $('.select2').select2();
    </script>
</body>
</html>
