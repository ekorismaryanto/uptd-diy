<?php
  
namespace App\Http\Imports;

use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\DataReportControlArea;
use App\Repositories\Entities\Opt;
use App\Repositories\Entities\SubDistrict;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Repositories\Entities\DataReportOpt;
use App\Repositories\Entities\ReportCrop;
use App\Repositories\Entities\ReportCropAreaAddedAtact;
use App\Repositories\Entities\ReportCropFlood;
use App\Repositories\Entities\ReportCropsAttactState;
use App\Repositories\Entities\ReportCropsControlArea;
use App\Repositories\Entities\ReportCropsPreviousMonth;
use App\Repositories\Entities\Village;
use Maatwebsite\Excel\Cell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class TanamanPanganBanjirImport implements ToModel, WithHeadingRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = array_values($row);

        if ($data[0] != null) {
            $commodity = Commodity::where('name', $data[6])->first();
            if (!$commodity) {
                $commodity = Commodity::create([
                    'name' => $data[6],
                    'type' => 1
                ]);
            }
    
            $findSubDistrict = SubDistrict::where('name', $data[4])->first();
            $village = Village::where('name', $data[5])->first();
            if (!$village) {
                $village = Village::create([
                    'sub_district_id' => $findSubDistrict->id,
                    'name' => $data[5]
                ]);
            }
    
            $request = [
                'user_id' => logged_in_user()->id,
                'periode' => $data[0].'-'.$data[1].'-01',
                'sub_district_id' => $findSubDistrict->id,
                'village_id' => $village->id,
                'commodity_id' => $commodity->id,
                'varieties' => $data[7],
                'age' => $data[8],
                'planting_area' => $data[9],
                'periode_tb' => $data[10],
                'broadly_alert' => $data[11],
                'previous_month_rest_receding_area' => $data[12],
                'previous_month_rest_receding_description' => $data[13],
                'previous_month_rest_puso_area' => $data[14],
                'previous_month_rest_puso_description' => $data[15],
                'more_area_added' => $data[16],
                'puso_area_added' => $data[17],
                'more_area_state_period' => $data[18],
                'puso_area_state_period' => $data[19],
                'handling_effort' => $data[20],
                'total_handling_effort' => $data[21],
                'coordinate' => $data[22],
            ];
    
            $report = ReportCropFlood::create($request);
        }

        return $report ?? false;
    }

    public function headingRow(): int
    {
        return 4;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    // public function chunkSize(): int
    // {
    //     return 50;
    // }
}