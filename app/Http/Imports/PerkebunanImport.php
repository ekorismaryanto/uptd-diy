<?php
  
namespace App\Http\Imports;

use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\DataReportControlArea;
use App\Repositories\Entities\Opt;
use App\Repositories\Entities\SubDistrict;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Repositories\Entities\DataReportOpt;

class PerkebunanImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row['tahun'] != null) {
            $query =  [
                "description" => $row['pendahuluan'],
                "user_id" => logged_in_user()->id,
                "date" => date('Y-m-d', strtotime($row['tahun'].'-'.$row['bulan'])),
                "type_commodity" => 3,
                'planting_area' => $row['luaspertanaman_ha'],
                "itensity_attack_easy" => $row['ringan'],
                "itensity_attack_hard" => $row['berat'],
                "control_area_apbn" => $row['apbn'],
                "control_area_apbd_1" => $row['apbd_i'],
                "control_area_apbd_2" => $row['apbd_ii'],
                "control_area_apbd_public" => $row['masya_rakat'],
                "price_average" => $row['harga_rata_rata_ton_rp'],
            ];
    
            $query =  DataReport::create($query);
    
            $city = SubDistrict::where('name', $row['kec'])->first();
    
            if ($city) {
                $query->update([
                    'sub_district_id' => $city->id ?? null
                ]);
            }
    
            $commodity = Commodity::where('name', $row['tanaman'])->first();
            if ($commodity) {
                $query->update([
                    'commodity_id' => $commodity->id ?? null
                ]);
            }else{
                $commodity = Commodity::create(['name' => $row['tanaman']]);
                $query->update([
                    'commodity_id' => $commodity->id ?? null
                ]);
            }
    
            $opt = Opt::where('name', $row['opt'])->first();
            if ($opt) {
                DataReportOpt::create([
                    'report_data_id' => $query->id,
                    'opt_id' => $opt->id
                ]);
            }else{
                $opt = Opt::create(['name' => $row['opt']]);
                DataReportOpt::create([
                    'report_data_id' => $query->id,
                    'opt_id' => $opt->id
                ]);
            }
    
            if ($row['cara_pengendalian']) {
                $caraPengendalian = explode(' ', $row['cara_pengendalian']);
    
                foreach ($caraPengendalian as $key => $value) {
                    if ($value == 'KT') {
                        DataReportControlArea::create([
                            'report_data_id' => $query->id,
                            'control_area' =>1
                        ]);
                    }
    
                    if ($value == 'M') {
                        DataReportControlArea::create([
                            'report_data_id' => $query->id,
                            'control_area' =>2
                        ]);
                    }
    
                    if ($value == 'B') {
                        DataReportControlArea::create([
                            'report_data_id' => $query->id,
                            'control_area' =>3
                        ]);
                    }
    
                    if ($value == 'K') {
                        DataReportControlArea::create([
                            'report_data_id' => $query->id,
                            'control_area' =>4
                        ]);
                    }
    
                    if ($value == 'Lainnya') {
                        DataReportControlArea::create([
                            'report_data_id' => $query->id,
                            'control_area' =>5
                        ]);
                    }
                }
            }
    
        }

        return $query ?? false;



    }

    public function headingRow(): int
    {
        return 2;
    }

    // public function chunkSize(): int
    // {
    //     return 50;
    // }
}