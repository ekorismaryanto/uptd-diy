<?php
  
namespace App\Http\Imports;

use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\DataReportControlArea;
use App\Repositories\Entities\Opt;
use App\Repositories\Entities\SubDistrict;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Repositories\Entities\DataReportOpt;
use App\Repositories\Entities\ReportCrop;
use App\Repositories\Entities\ReportCropAreaAddedAtact;
use App\Repositories\Entities\ReportCropsAttactState;
use App\Repositories\Entities\ReportCropsControlArea;
use App\Repositories\Entities\ReportCropsPreviousMonth;
use App\Repositories\Entities\Village;
use Maatwebsite\Excel\Cell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class TanamanPanganImport implements ToModel, WithHeadingRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = array_values($row);

        $findSubDistrict = SubDistrict::where('name', $data[4])->first();
        $commodity = Commodity::where('name', $data[6])->first();
        if (!$commodity) {
            $commodity = Commodity::create([
                'name' => $data[6],
                'type' => 1
            ]);
        }

        $village = Village::where('name', $data[5])->first();
        if (!$village) {
            $village = Village::create([
                'sub_district_id' => $findSubDistrict->id,
                'name' => $data[5]
            ]);
        }


        $opt = Opt::where('name', $data[10])->first();
        if (!$opt) {
            $opt = Opt::create([
                'name' => $data[10]
            ]);
        }


        $request = [
            'user_id' => logged_in_user()->id,
            'periode' => $data[0].'-'.$data[1].'-01',
            'sub_district_id' => $findSubDistrict->id,
            'village_id' => $village->id,
            'commodity_id' => $commodity->id,
            'varieties' => $data[7],
            'age' => $data[8],
            'planting_area' => $data[9],
            'opt_id' => $opt->id,
            'criteria_opt' => $data[11],
            'month' => $data[12],
            'periode_tb' => $data[13]
        ];

        $report = ReportCrop::create($request);

        ReportCropsPreviousMonth::create([
            'report_crop_id' => $report->id,
            'ringan' => $data[14],
            'sedang'=> $data[15],
            'berat'=> $data[16],
            'puso'=> $data[17],
            'retrained_area'=> $data[19],
            'harvest_area'=> $data[20],
        ]);

        ReportCropAreaAddedAtact::create([
            'report_crop_id' => $report->id,
            'ringan' => $data[21],
            'sedang'=> $data[22],
            'berat'=> $data[23],
            'puso'=> $data[24],
        ]);

        ReportCropsControlArea::create([
            'report_crop_id' => $report->id,
            'kimia' => $data[26],
            'hayati'=> $data[27],
            'eradikasi'=> $data[28],
            'others'=> $data[29],
        ]);

        ReportCropsAttactState::create([
            'report_crop_id' => $report->id,
            'ringan' => $data[31],
            'sedang'=> $data[32],
            'berat'=> $data[33],
            'puso'=> $data[34]
        ]);


        return $report;

    }

    public function headingRow(): int
    {
        return 4;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    // public function chunkSize(): int
    // {
    //     return 50;
    // }
}