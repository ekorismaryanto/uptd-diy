<?php
  
namespace App\Http\Imports;

use App\Repositories\Entities\Opt;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
  
class DistractionImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (isset($row['nama'])) {
            return Opt::firstOrCreate([
                'name' => $row['nama'] ?? null
            ]);
        }
    }
}