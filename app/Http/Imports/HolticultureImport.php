<?php
  
namespace App\Http\Imports;

use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\DataReportControlArea;
use App\Repositories\Entities\Opt;
use App\Repositories\Entities\SubDistrict;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Repositories\Entities\DataReportOpt;
use App\Repositories\Entities\ReportCrop;
use App\Repositories\Entities\ReportCropAreaAddedAtact;
use App\Repositories\Entities\ReportHolticulture;
use App\Repositories\Entities\ReportCropsAttactState;
use App\Repositories\Entities\ReportCropsControlArea;
use App\Repositories\Entities\ReportCropsPreviousMonth;
use App\Repositories\Entities\Village;
use Maatwebsite\Excel\Cell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class HolticultureImport implements ToModel, WithHeadingRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = array_values($row);

        if ($data[0] != null) {
            $commodity = Commodity::where('name', $data[4])->first();
            if (!$commodity) {
                $commodity = Commodity::create([
                    'name' => $data[4],
                    'type' => 1
                ]);
            }
    
            $subDistrict = SubDistrict::where('name', $data[2])->first();
            
            $village = Village::where('name', $data[3])->first();
            if (!$village) {
                $subDistrict = SubDistrict::where('name', $data[2])->get();
                $village = Village::create([
                    'sub_district_id' => $subDistrict->id,
                    'name' => $data[3]
                ]);
            }
    
            $opt = Opt::where('name', $data[5])->first();
            if (!$opt) {
                $opt = Opt::create([
                    'name' => $data[5]
                ]);
            }
    
            $request = [
                'user_id' => logged_in_user()->id,
                'periode' => $data[6].'-'.$data[7].'-01',
                'sub_district_id' => $subDistrict->id,
                'village_id' => $village->id,
                'commodity_id' => $commodity->id,
                'opt_id' => $opt->id,
                'periode_tb' => $data[8],
                'planting_area' => $data[9],
                'attact_area_r' => $data[10],
                'attact_area_s' => $data[11],
                'attact_area_b' => $data[12],
                'attact_area_p' => $data[13],
                'attact_area_j' => $data[14],
                'under_control' => $data[15],
                'added_area_r' => $data[16],
                'added_area_s' => $data[17],
                'added_area_b' => $data[18],
                'added_area_p' => $data[19],
                'added_area_j' => $data[20],
                'state_area_r' => $data[21],
                'state_area_s' => $data[22],
                'state_area_b' => $data[23],
                'state_area_p' => $data[24],
                'state_area_j' => $data[25],
                'control_area_pem' => $data[26],
                'control_area_pest' => $data[27],
                'control_area_cl' => $data[28],
                'control_area_aph' => $data[29],
                'control_area_j' => $data[30],
                'threatened_plant' => $data[31],
            ];
    
            $report = ReportHolticulture::create($request);
        }

        
        return $report ?? [];

    }

    public function headingRow(): int
    {
        return 3;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    // public function chunkSize(): int
    // {
    //     return 50;
    // }
}