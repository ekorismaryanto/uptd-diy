<?php
  
namespace App\Http\Imports;

use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\Opt;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
  
class CommodityImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row['jenis_komoditas'] != '') {

            switch ($row['jenis_komoditas']) {
                case 'Tanaman Pangan':
                    $type = 1;
                    break;

                case 'Holtikultura':
                    $type = 2;
                    break;

                case 'Perkebunan':
                    $type = 3;
                    break;
                default:
                    $type = null;
                    break;
            }


            return Commodity::firstOrCreate([
                'name' => $row['nama_komoditas'] ?? null,
                'type' => $type
            ]);
        }
    }
}