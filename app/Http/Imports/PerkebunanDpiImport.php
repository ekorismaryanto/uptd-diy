<?php
  
namespace App\Http\Imports;

use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\DataReportControlArea;
use App\Repositories\Entities\Opt;
use App\Repositories\Entities\SubDistrict;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Repositories\Entities\DataReportOpt;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportCrop;
use App\Repositories\Entities\ReportCropAreaAddedAtact;
use App\Repositories\Entities\ReportCropFlood;
use App\Repositories\Entities\ReportCropsAttactState;
use App\Repositories\Entities\ReportCropsControlArea;
use App\Repositories\Entities\ReportCropsPreviousMonth;
use App\Repositories\Entities\ReportPlantationDpi;
use App\Repositories\Entities\Village;
use Maatwebsite\Excel\Cell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class PerkebunanDpiImport implements ToModel, WithHeadingRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = array_values($row);

        if ($data[0] != null) {
            $commodity = Commodity::where('name', $data[4])->first();
            if (!$commodity) {
                $commodity = Commodity::create([
                    'name' => $data[4],
                    'type' => 3
                ]);
            }
    
            $findDistrict = SubDistrict::where('name', $data[3])->first();
    
            $request = [
                'user_id' => logged_in_user()->id,
                'periode' => $data[0].'-'.$data[1].'-01',
                'sub_district_id' => $findDistrict->id,
                'commodity_id' => $commodity->id,
                'planting_area' => $data[5],
                'age' => $data[6],
                'age_unit' => $data[7],
                'dry_exposed' => $data[8],
                'dry_lightly' => $data[9],
                'dry_handling' => $data[10],
                'flood_hit' => $data[11],
                'light_flood' => $data[12],
                'flood_handling' => $data[13],
                'fire_hit' => $data[14],
                'light_fire' => $data[15],
                'fire_handling' => $data[16],
            ];
    
            $report = ReportPlantationDpi::create($request);
        }

        return $report ?? false;
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    // public function chunkSize(): int
    // {
    //     return 50;
    // }
}