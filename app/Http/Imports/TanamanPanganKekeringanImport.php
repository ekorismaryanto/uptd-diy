<?php
  
namespace App\Http\Imports;

use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\DataReportControlArea;
use App\Repositories\Entities\Opt;
use App\Repositories\Entities\SubDistrict;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Repositories\Entities\DataReportOpt;
use App\Repositories\Entities\ReportCrop;
use App\Repositories\Entities\ReportCropAreaAddedAtact;
use App\Repositories\Entities\ReportCropFlood;
use App\Repositories\Entities\ReportCropsAttactState;
use App\Repositories\Entities\ReportCropsControlArea;
use App\Repositories\Entities\ReportCropsDrought;
use App\Repositories\Entities\ReportCropsPreviousMonth;
use App\Repositories\Entities\Village;
use Maatwebsite\Excel\Cell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class TanamanPanganKekeringanImport implements ToModel, WithHeadingRow, WithCalculatedFormulas
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $data = array_values($row);

        if ($data[0] != null) {
            $commodity = Commodity::where('name', $data[6])->first();
            if (!$commodity) {
                $commodity = Commodity::create([
                    'name' => $data[6],
                    'type' => 1
                ]);
            }
    
            $findSubDistrict = SubDistrict::where('name', $data[4])->first();
            $village = Village::where('name', $data[5])->first();
            if (!$village) {
                $village = Village::create([
                    'sub_district_id' => $findSubDistrict->id ?? null,
                    'name' => $data[5]
                ]);
            }
    
            if ($findSubDistrict) {
                $request = [
                    'user_id' => logged_in_user()->id,
                    'periode' => $data[0].'-'.$data[1].'-01',
                    'sub_district_id' => $findSubDistrict->id ?? null,
                    'village_id' => $village->id,
                    'commodity_id' => $commodity->id,
                    'varieties' => $data[7],
                    'age' => $data[8], 
                    'planting_area' => $data[9], 
                    'broadly_alert' => $data[10],  
                    'previous_periode_low' => $data[11],  
                    'previous_periode_mid' => $data[12],
                    'previous_periode_hight' => $data[13],
                    'previous_periode_puso' => $data[14],
                    'previous_periode_recover' => $data[15],
                    'previous_periode_j' => $data[16],
                    'area_added_periode_low' => $data[17],
                    'area_added_periode_mid' => $data[18],
                    'area_added_periode_hight' => $data[19],
                    'area_added_periode_puso' => $data[20],
                    'area_added_periode_j' => $data[21],
                    'area_state_periode_low' => $data[22],
                    'area_state_periode_mid' => $data[23],
                    'area_state_periode_hight' => $data[24],
                    'area_state_periode_puso' => $data[25],
                    'area_state_periode_j' => $data[26],
                    'handling_effort' => $data[27],
                    'handling_effort_total' => $data[28],
                    'coordinate' => $data[29],
                    'description' => $data[30],
                ];
        
                $report = ReportCropsDrought::create($request);
            }
        }
        
        return $report ?? true;


    }

    public function headingRow(): int
    {
        return 3;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    // public function chunkSize(): int
    // {
    //     return 50;
    // }
}