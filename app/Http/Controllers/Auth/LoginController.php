<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{

    public function index()
    {
        return view('auth.login');
    }

    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'nip' => 'required',
            'password' => 'required'
        ]);

        if(Auth::attempt(['nip' => $request->nip, 'password' => $request->password])) {
            if (logged_in_user()->status == 1) {
                
                notice('success', 'Selamat datang !!');
                
                if (logged_in_user()->can('input-data')) {
                    return redirect()->route('admin.data.data-report.create');
                }

                return redirect()->route('admin.index');
            }else{
                Auth::logout();
                notice('error', 'Akun Anda di Suspend. silahkan hubungi admin untuk info lebih lanjut');

                return back()->with('error', 'Incorrect username or password.');
            }
         
        }

        notice('error', 'NIP atau Password salah !!!');
        
        return back()->with('error', 'Incorrect username or password.');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}