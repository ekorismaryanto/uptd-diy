<?php

namespace App\Http\Controllers;

use App\Http\Constants\TypeCommodity;
use App\Http\Controllers\Controller;
use App\Http\Requests\DataReportRequest;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\News;
use App\Repositories\Entities\District;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function keadaanOpt(Request $request)
    {
        return view('keadaan-opt');
    }

    public function dataPetugas(Request $request)
    {
        return view('data-petugas');
    }

    public function news(Request $request)
    {
        $news = News::latest()->paginate(10);

        return view('news', compact('news'));
    }

    public function newsDetail(Request $request, $slug)
    {
        $news = News::where('slug', $slug)->first();

        return view('news-detail', compact('news'));
    }
}