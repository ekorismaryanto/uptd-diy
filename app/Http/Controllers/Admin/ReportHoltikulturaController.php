<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Exports\ReportExcelExport;
use App\Http\Exports\ReportTanamanPanganExcelExport;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportHolticulture;
use App\Repositories\ReportHoltikulturaEloquent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF1;

class ReportHoltikulturaController extends Controller
{

    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, ReportHoltikulturaEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }
    

    public function index(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, true);

        return view('admin.report.holtikultura.index', compact('reports'));
    }

    public function edit($id)
    {
        $opt = ReportHolticulture::find($id);

        return view('data.hortikultura.opt.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $data['report_data'] = $request->all();
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $request = [
                'periode' => $data['report_data']['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'opt_id' => $data['report_data']['opts'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'attact_area_r' => $data['report_data']['attact_area_r'][$key],
                'attact_area_s' => $data['report_data']['attact_area_s'][$key],
                'attact_area_b' => $data['report_data']['attact_area_b'][$key],
                'attact_area_p' => $data['report_data']['attact_area_p'][$key],
                'under_control' => $data['report_data']['under_control'][$key],
                'added_area_r' => $data['report_data']['added_area_r'][$key],
                'added_area_s' => $data['report_data']['added_area_s'][$key],
                'added_area_b' => $data['report_data']['added_area_b'][$key],
                'added_area_p' => $data['report_data']['added_area_p'][$key],
                'state_area_r' => $data['report_data']['state_area_r'][$key],
                'state_area_s' => $data['report_data']['state_area_s'][$key],
                'state_area_b' => $data['report_data']['state_area_b'][$key],
                'state_area_p' => $data['report_data']['state_area_p'][$key],
                'control_area_pem' => $data['report_data']['control_area_pem'][$key],
                'control_area_pest' => $data['report_data']['control_area_pest'][$key],
                'control_area_cl' => $data['report_data']['control_area_cl'][$key],
                'control_area_aph' => $data['report_data']['control_area_aph'][$key],
                'threatened_plant' => $data['report_data']['threatened_plant'][$key],
            ];

            ReportHolticulture::find($id)->update($request);
        }

        notice('success', 'Data Berhasil diubah');

        return redirect()->route('admin.report.holtikultura.index');
    }

    public function delete($id)
    {
        $opt = ReportHolticulture::where('id', $id)->delete();

        notice('success', 'data berhasil di hapus');
        return redirect()->route('admin.report.holtikultura.index');
    }

    public function detail($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.detail', compact('report'));
    }

    public function detailPrint($id)
    {
        $report = $this->report->find($id);

        return view('export.print', compact('report'));
    }

    public function detailPrintPdf($id)
    {
        $report = $this->report->find($id);

        $pdf = PDF1::loadview('export.print', compact('report'));

        return $pdf->download('report.pdf');
    }
    
    public function print(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);

        return view('admin.report.holtikultura.export.excel', compact('reports'));
    }

    public function xls(Request $request)
    {
        return Excel::download(new ReportTanamanPanganExcelExport($request->all()), 'report.xlsx');
    }

    public function csv(Request $request)
    {
        return Excel::download(new ReportTanamanPanganExcelExport($request->all()), 'report.csv');
    }

    public function pdf(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);
     
        $pdf = PDF1::loadview('admin.report.holtikultura.export.pdf',
        compact('reports'), 
        [], 
        [ 
          'title' => 'Certificate', 
          'format' => 'Legal',
          'orientation' => 'L'
        ]);

        return $pdf->download('report-holtikultura.pdf');
    }

}