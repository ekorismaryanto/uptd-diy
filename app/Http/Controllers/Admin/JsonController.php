<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\Opt;
use App\Repositories\Entities\SubDistrict;
use App\Repositories\Entities\Village;
use Illuminate\Http\Request;

class JsonController extends Controller
{

    public function subDistrict(Request $request)
    {
        $query = SubDistrict::orderBy('id', 'desc');

        if($request->has('q')) {
            $query->where('name', 'like','%'.$request->q.'%');
        }

        if($request->district_id != '') {
            $query->where('district_id', $request->district_id);
        }
    
        $query = $query->get()->take(50); 
    
        $data = [];
        foreach ($query as $key => $row) {  
            $data[$key]['id'] = $row['id'];
            $data[$key]['text'] = $row['name'];
        }
    
        $data = array_values($data);
        $data = ['results' => $data];
    
        return response()->json($data);
    }

    public function opt(Request $request)
    {
        $query = Opt::orderBy('id', 'desc');

        if($request->has('q')) {
            $query->where('name', 'like','%'.$request->q.'%');
        }
    
        $query = $query->get()->take(50); 
    
        $data = [];
        foreach ($query as $key => $row) {  
            $data[$key]['id'] = $row['id'];
            $data[$key]['text'] = $row['name'];
        }
    
        $data = array_values($data);
        $data = ['results' => $data];
    
        return response()->json($data);
    }

    public function commodity(Request $request)
    {
        $query = Commodity::orderBy('id', 'desc');

        if($request->has('q')) {
            $query->where('name', 'like','%'.$request->q.'%');
        }

        if($request->has('type')) {
            $query->where('type', $request->type);
        }
    
        $query = $query->get()->take(50); 
    
        $data = [];
        foreach ($query as $key => $row) {  
            $data[$key]['id'] = $row['id'];
            $data[$key]['text'] = $row['name'];
        }
    
        $data = array_values($data);
        $data = ['results' => $data];
    
        return response()->json($data);
    }

    public function village(Request $request)
    {
        $query = Village::orderBy('id', 'desc');

        if($request->has('q')) {
            $query->where('name', 'like','%'.$request->q.'%');
        }

        if($request->sub_district_id != '') {
            $query->where('sub_district_id', $request->sub_district_id);
        }
    
        $query = $query->get()->take(50); 
    
        $data = [];
        foreach ($query as $key => $row) {  
            $data[$key]['id'] = $row['id'];
            $data[$key]['text'] = $row['name'];
        }
    
        $data = array_values($data);
        $data = ['results' => $data];
    
        return response()->json($data);
    }

}