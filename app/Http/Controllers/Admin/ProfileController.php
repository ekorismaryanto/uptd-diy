<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Repositories\UserEloquent;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class ProfileController extends Controller
{
	protected $role, $user;

    public function __construct(Role $role, UserEloquent $user)
    {
        $this->role = $role;
        $this->user = $user;
    }

    public function profile()
    {
        $roles = $this->role->get();
        $user = $this->user->find(logged_in_user()->id);

        return view('auth.profile', compact('roles','user'));
    }

    public function update(ProfileRequest $request)
    {
        $data = $request->data();

        if($request->hasFile('img')){
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('img')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('img')->storeAs('uploads/profile/',$fileNameToStore, ['disk' => 'public_uploads']);
            $data['user'] = array_merge($data['user'], [
                'photo' => $fileNameToStore
            ]);
        }

        $this->user->update(logged_in_user()->id, $data);
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.profile');
    }
}