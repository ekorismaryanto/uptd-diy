<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\District;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, DataReportEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }

    public function index(Request $request)
    {
        if (!logged_in_user()->can('overview')) {
            return redirect()->route('admin.statistic.index');
        }
        
        $reports = $this->report->fetch($request->all(), true);

        return view('admin.dashboard', compact('reports'));
    }
}