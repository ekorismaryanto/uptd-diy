<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Entities\DataReportOpt;
use App\Repositories\Entities\District;
use Illuminate\Http\Request;

class StatisticController extends Controller
{

    public function index(Request $request)
    {
        return view('admin.statistic.index');
    }

    public function detail(Request $request, $id)
    {
        $district = District::find($id);

        $queryOpt = "
            SELECT opts.name, COUNT(opt_id) as total
            FROM 
            data_report_opts 
            JOIN opts
            ON data_report_opts.opt_id = opts.id
            JOIN data_reports
            ON data_reports.id = data_report_opts.report_data_id
            JOIN sub_districts
            ON sub_districts.id = data_reports.sub_district_id
            where sub_districts.district_id = $id
            GROUP BY opt_id
            limit 10
        ";

        $queryCommodity = "
            SELECT
            commodities.name, count(data_reports.commodity_id) as total
            FROM data_reports
            JOIN commodities ON data_reports.commodity_id = commodities.id
            JOIN sub_districts ON sub_districts.id = data_reports.sub_district_id
            where sub_districts.district_id = $id
            GROUP BY data_reports.commodity_id
            limit 10
        ";

        $queryMember = "
            SELECT 
            users.name,
            COUNT(data_reports.user_id) as total
            FROM data_reports
            JOIN users ON data_reports.user_id = users.id
            JOIN sub_districts ON sub_districts.id = data_reports.sub_district_id
            where sub_districts.district_id = $id
            GROUP BY data_reports.user_id
            limit 10
        ";


		$opt = \DB::select(\DB::raw($queryOpt));
		$commodity = \DB::select(\DB::raw($queryCommodity));
		$member = \DB::select(\DB::raw($queryMember));
        
        return view('admin.statistic.detail', compact('district','opt','commodity','member'));
    }
}