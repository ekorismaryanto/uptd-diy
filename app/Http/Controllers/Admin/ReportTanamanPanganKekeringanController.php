<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Exports\ReportExcelExport;
use App\Http\Exports\ReportTanamanPanganKekeringanExcelExport;
use App\Http\Exports\ReportTanamanPanganExcelExport;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportCropFlood;
use App\Repositories\Entities\ReportCropsDrought;
use App\Repositories\ReportTanamanPanganBanjirEloquent;
use App\Repositories\ReportTanamanPanganEloquent;
use App\Repositories\ReportTanamanPanganKekeringanEloquent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF1;

class ReportTanamanPanganKekeringanController extends Controller
{

    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, ReportTanamanPanganKekeringanEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }
    

    public function index(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, true);

        return view('admin.report.tanaman-pangan.kekeringan.index', compact('reports'));
    }

    public function edit($id)
    {
        $opt = ReportCropsDrought::find($id);

        return view('data.tanaman-pangan.kekeringan.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        foreach ($data['sub_district_id'] as $key => $value) {
            $request = [
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['sub_district_id'][$key],
                'village_id' => $data['village_id'][$key],
                'commodity_id' => $data['commodity_id'][$key],
                'varieties' => $data['varietas'][$key],
                'age' => $data['umur'][$key],
                'planting_area' => $data['planting_area'][$key],
                'broadly_alert' => $data['broadly_alert'][$key],
                'periode_tb' => $data['periode_tb'][$key],
                'previous_periode_low' => $data['previous_periode_low'][$key],
                'previous_periode_mid' => $data['previous_periode_mid'][$key],
                'previous_periode_hight' => $data['previous_periode_hight'][$key],
                'previous_periode_puso' => $data['previous_periode_puso'][$key],
                'previous_periode_recover' => $data['previous_periode_recover'][$key],
                'area_added_periode_low' => $data['area_added_periode_low'][$key],
                'area_added_periode_mid' => $data['area_added_periode_mid'][$key],
                'area_added_periode_hight' => $data['area_added_periode_hight'][$key],
                'area_added_periode_puso' => $data['area_added_periode_puso'][$key],
                'area_state_periode_low' => $data['area_state_periode_low'][$key],
                'area_state_periode_mid' => $data['area_state_periode_mid'][$key],
                'area_state_periode_hight' => $data['area_state_periode_hight'][$key],
                'area_state_periode_puso' => $data['area_state_periode_puso'][$key],
                'handling_effort' => $data['handling_effort'][$key],
                'handling_effort_total' => $data['handling_effort_total'][$key],
                'coordinate' => $data['coordinate'][$key],
                'description' => $data['description'][$key],
            ];

            $dataReportCrop = ReportCropsDrought::where('id', $id)->update($request);
        }

        notice('success', 'Berhasil di Update');

        return redirect()->route('admin.report.tanaman-pangan.kekeringan.index');
    }

    public function delete($id)
    {
        $opt = ReportCropsDrought::where('id', $id)->delete();

        notice('success', 'data berhasil di hapus');
        return redirect()->route('admin.report.tanaman-pangan.kekeringan.index');
    }

    public function detail($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.detail', compact('report'));
    }

    public function detailPrint($id)
    {
        $report = $this->report->find($id);

        return view('export.print', compact('report'));
    }

    public function detailPrintPdf($id)
    {
        $report = $this->report->find($id);

        $pdf = PDF1::loadview('export.print', compact('report'));

        return $pdf->download('report.pdf');
    }
    
    public function print(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);

        return view('admin.report.tanaman-pangan.kekeringan.export.excel', compact('reports'));
    }

    public function xls(Request $request)
    {
        return Excel::download(new ReportTanamanPanganKekeringanExcelExport($request->all()), 'report.xlsx');
    }

    public function csv(Request $request)
    {
        return Excel::download(new ReportTanamanPanganKekeringanExcelExport($request->all()), 'report.csv');
    }

    public function pdf(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);
     
        $pdf = PDF1::loadview('admin.report.tanaman-pangan.kekeringan.export.pdf',
        compact('reports'), 
        [], 
        [ 
          'title' => 'Certificate', 
          'format' => 'Legal',
          'orientation' => 'L'
        ]);

        return $pdf->download('report-tanaman-pangan.pdf');
    }

}