<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{

    protected $permission, $role;
    
    public function __construct(Permission $permission, Role $role)
    {
        $this->permission = $permission;
        $this->role = $role;
    }
    
    public function index(Request $request)
    {
        $query = $this->role->query();

        if ($request->has('q')) {
            $query->where('name', 'like','%'.$request->q.'%');
        }

        $roles = $query->paginate(20);

        return view('user-management.role.index', compact('roles'));
    }

    public function create(Request $request)
    {
        $permissions = $this->permission->get();

        return view('user-management.role.create',compact('permissions'));
    }

    public function store(Request $request)
    {        
        $validated = $request->validate([
            'name' => 'required|unique:roles|max:255',
        ]);

        try {
            $role = $this->role->create([
                'name' => $request->name
            ]);

            if ($role) {
                $role->givePermissionTo($request->permissions);
            }

        } catch (\Throwable $th) {
            //throw $th;
        }
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.user-management.role.index');
    }

    public function edit($id)
    {
        $role = $this->role->find($id);
        $permissions = $this->permission->get();

        return view('user-management.role.edit', compact('permissions','role'));
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' =>'required|unique:permissions,name,'.$id,',id',
        ]);

        $role = $this->role->find($id);

        $role->update([
            'name' => $request->name
        ]);
            
        if ($role) {
            $role->permissions()->sync($request->permissions);
        }

        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.user-management.role.index');
    }

    public function delete($id)
    {
        $role = $this->role->where('id', $id)->delete();

        notice('success','Data Berhasil di Hapus');

        return redirect()->route('admin.user-management.role.index');
    }
}