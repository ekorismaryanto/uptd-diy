<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{

    protected $permission;
    
    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }
    
    public function index(Request $request)
    {
        $permissions = $this->permission->query();

        if ($request->has('q')) {
            $permissions->where('name', 'like','%'.$request->q.'%');
        }

        $permissions = $permissions->paginate(20);

        return view('user-management.permission.index', compact('permissions'));
    }

    public function create(Request $request)
    {
        return view('user-management.permission.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:permissions|max:255',
        ]);

        try {
            $this->permission->create([
                'name' => $request->name
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.user-management.permission.index');
    }

    public function edit($id)
    {
        $permission = $this->permission->find($id);

        return view('user-management.permission.edit', compact('permission'));
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required|unique:permissions,name,'.$id.',id|max:255',
        ]);

        try {
            $this->permission->find($id)->create([
                'name' => $request->name
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.user-management.permission.index');
    }

    public function delete($id)
    {
        $permission = $this->permission->find($id)->delete();

        notice('success','Data Berhasil di Hapus');

        return view('user-management.permission.index');
    }
}