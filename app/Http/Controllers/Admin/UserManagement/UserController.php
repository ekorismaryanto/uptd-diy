<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Repositories\UserEloquent;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
	protected $role, $user;

    public function __construct(Role $role, UserEloquent $user)
    {
        $this->role = $role;
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $users = $this->user->fetch($request->all());

        return view('user-management.user.index', compact('users'));
    }

    public function create()
    {
        $roles = $this->role->get();

        return view('user-management.user.create', compact('roles'));
    }

    public function store(UserRequest $request)
    {
        $data = $request->data();
        if($request->hasFile('img')){
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('img')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('img')->storeAs('uploads/profile/',$fileNameToStore, ['disk' => 'public_uploads']);
            $data['user'] = array_merge($data['user'], [
                'photo' => $fileNameToStore
            ]);
        }

        $this->user->store($data);
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.user-management.user.index');
    }

    public function edit($id)
    {
        $roles = $this->role->get();
        $user = $this->user->find($id);

        return view('user-management.user.edit', compact('roles','user'));
    }

    public function update(UserRequest $request, $id)
    {
        $data = $request->data();

        if($request->hasFile('img')){
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('img')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('img')->storeAs('uploads/profile/',$fileNameToStore, ['disk' => 'public_uploads']);
            $data['user'] = array_merge($data['user'], [
                'photo' => $fileNameToStore
            ]);
        }

        $this->user->update($id, $data);
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.user-management.user.index');
    }
}