<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Imports\HolticultureBanjirImport;
use App\Http\Imports\HolticultureImport;
use App\Http\Imports\HoltikulturaKekeringanImport;
use App\Http\Imports\PerkebunanDpiImport;
use App\Http\Imports\PerkebunanImport;
use App\Http\Imports\PerkebunanLkkImport;
use App\Http\Imports\TanamanPanganBanjirImport;
use App\Http\Imports\TanamanPanganImport;
use App\Http\Imports\TanamanPanganKekeringanImport;
use App\Http\Requests\ProfileRequest;
use App\Repositories\Entities\DataReport;
use App\Repositories\UserEloquent;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;
use Symfony\Component\VarDumper\Cloner\Data;

class ImportController extends Controller
{
	protected $role, $user;

    public function __construct(Role $role, UserEloquent $user)
    {
        $this->role = $role;
        $this->user = $user;
    }

    public function importPerkebunan()
    {
        return view('import.perkebunan');
    }

    public function storeImportPerkebunan(Request $request)
    {

        $file = $request->file('file');

        $nama_file = rand().$file->getClientOriginalName();
        switch ($request->type) {
            case '1':
                $file->move('file-perkebunan',$nama_file);

                $data = Excel::import(new PerkebunanImport, public_path('/file-perkebunan/'.$nama_file));
                notice('success', 'import berhasil');
              
                // return redirect()->route('admin.report.perkbunan.index');
                break;

            case '2':
                $file->move('file-perkebunan-dpi' ,$nama_file);
                $data = Excel::import(new PerkebunanDpiImport, public_path('/file-perkebunan-dpi/'.$nama_file));
                notice('success', 'import berhasil');
                // return redirect()->route('admin.report.perkebunan.banjir.index');
                break;

            case '3':
                $file->move('file-perkebunan-lkk',$nama_file);
                $data = Excel::import(new PerkebunanLkkImport, public_path('/file-perkebunan-lkk/'.$nama_file));
                notice('success', 'import berhasil');
                // return redirect()->route('admin.report.tanaman-pangan.kekeringan.index');
            break;
            
            default:
                # code...
                break;
        }

        return redirect()->route('admin.import.importPerkebunan');
    }

    public function importTanamanPangan()
    {
        return view('import.tanaman-pangan');
    }

    public function storeImportTanamanPangan(Request $request)
    {
        $file = $request->file('file');

        $nama_file = rand().$file->getClientOriginalName();
        switch ($request->type) {
            case '1':
                $file->move('file-tanaman-pangan',$nama_file);
                $data = Excel::import(new TanamanPanganImport, public_path('/file-tanaman-pangan/'.$nama_file));
                notice('success', 'import berhasil');
                return redirect()->route('admin.report.tanaman-pangan.index');
                break;

            case '2':
                $file->move('file-tanaman-pangan-banjir',$nama_file);
                $data = Excel::import(new TanamanPanganBanjirImport, public_path('/file-tanaman-pangan-banjir/'.$nama_file));
                notice('success', 'import berhasil');
                return redirect()->route('admin.report.tanaman-pangan.banjir.index');
                break;

            case '3':
                $file->move('file-tanaman-pangan-kekeringan',$nama_file);
                $data = Excel::import(new TanamanPanganKekeringanImport, public_path('/file-tanaman-pangan-kekeringan/'.$nama_file));
                notice('success', 'import berhasil');
                return redirect()->route('admin.report.tanaman-pangan.kekeringan.index');    
            break;
            
            default:
                # code...
                break;
        }

      
        return redirect()->route('admin.import.tanaman-pangan');
    }

    public function importHoltikultura()
    {
        return view('import.holtikultura');
    }

    public function storeImportHoltikultura(Request $request)
    {
        $file = $request->file('file');

        $nama_file = rand().$file->getClientOriginalName();
        switch ($request->type) {
            case '1':
                $file->move('file-holtikultura',$nama_file);
                $data = Excel::import(new HolticultureImport, public_path('/file-holtikultura/'.$nama_file));
                notice('success', 'import berhasil');
                return redirect()->route('admin.report.holtikultura.index');
                break;

            case '2':
                $file->move('file-holtikultura-banjir',$nama_file);
                $data = Excel::import(new HolticultureBanjirImport, public_path('/file-holtikultura-banjir/'.$nama_file));
                notice('success', 'import berhasil');
                return redirect()->route('admin.report.holtikultura.banjir.index');
                break;

            case '3':
                $file->move('file-holtikultura-kekeringan',$nama_file);
                $data = Excel::import(new HoltikulturaKekeringanImport, public_path('/file-holtikultura-kekeringan/'.$nama_file));
                notice('success', 'import berhasil');
                return redirect()->route('admin.report.holtikultura.kekeringan.index');
                break;
            
            default:
                # code...
                break;
        }
    }


}