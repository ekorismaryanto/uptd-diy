<?php

namespace App\Http\Controllers\Admin\DataMaster;

use App\Http\Controllers\Controller;
use App\Http\Imports\CommodityImport;
use App\Http\Imports\DistractionImport;
use App\Repositories\Entities\Commodity;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CommodityController extends Controller
{

    protected $commodity;
    
    public function __construct(Commodity $commodity)
    {
        $this->commodity = $commodity;
    }
    
    public function index(Request $request)
    {
        $query = $this->commodity->query();

        if ($request->has('q')) {
            $query->where('name', 'like','%'.$request->q.'%');
        }

        if ($request->has('type')) {
            if ($request->type != '') {
                $query->where('type', $request->type);
            }
        }

        $commodities = $query->latest()->paginate(20);

        return view('datamaster.commodity.index', compact('commodities'));
    }

    public function create(Request $request)
    {
        return view('datamaster.commodity.create');
    }

    public function store(Request $request)
    {        
        $validated = $request->validate([
            'name' => 'required|unique:commodities',
        ]);

        $role = $this->commodity->create([
            'name' => $request->name,
            'type' => $request->type
        ]);
        
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.datamaster.commodity.index');
    }

    public function edit($id)
    {
        $commodity = $this->commodity->find($id);

        return view('datamaster.commodity.edit', compact('commodity'));
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required|unique:commodities,name,'.$id.',id',
        ]);

        $role = $this->commodity->where('id', $id)->update([
            'name' => $request->name,
            'type' => $request->type
        ]);

        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.datamaster.commodity.index');
    }

    public function delete($id)
    {
        $role = $this->commodity->where('id', $id)->delete();

        notice('success','Data Berhasil di Hapus');

        return redirect()->route('admin.datamaster.commodity.index');
    }

    public function import(Request $request)
    {
        // $this->validate($request, [
		// 	'file' => 'required|mimes:csv,xls,xlsx'
		// ]);
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file-komoditas',$nama_file);
 
		// import data
        Excel::import(new CommodityImport, public_path('/file-komoditas/'.$nama_file));
        
        notice('success','Data Berhasil di Import');
              
        return back();
    }
}