<?php

namespace App\Http\Controllers\Admin\DataMaster;

use App\Http\Controllers\Controller;
use App\Repositories\Entities\Opt;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Imports\DistractionImport;

class DistractionController extends Controller
{

    protected $opt;
    
    public function __construct(Opt $opt)
    {
        $this->opt = $opt;
    }
    
    public function index(Request $request)
    {
        $query = $this->opt->query();

        if ($request->has('q')) {
            $query->where('name', 'like','%'.$request->q.'%');
        }

        $opts = $query->latest()->paginate(20);

        return view('datamaster.distraction.index', compact('opts'));
    }

    public function create(Request $request)
    {
        return view('datamaster.distraction.create');
    }

    public function store(Request $request)
    {        
        $validated = $request->validate([
            'name' => 'required|unique:opts',
        ]);

        $role = $this->opt->create([
            'name' => $request->name,
        ]);
        
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.datamaster.distraction.index');
    }

    public function edit($id)
    {
        $opt = $this->opt->find($id);

        return view('datamaster.distraction.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required|unique:opts,name,'.$id.',id',
        ]);

        $query = $this->opt->where('id', $id)->update([
            'name' => $request->name
        ]);

        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.datamaster.distraction.index');
    }

    public function delete($id)
    {
        $query = $this->opt->where('id', $id)->delete();

        notice('success','Data Berhasil di Hapus');

        return redirect()->route('admin.datamaster.distraction.index');
    }

    public function import(Request $request)
    {
        // $this->validate($request, [
		// 	'file' => 'required|mimes:csv,xls,xlsx'
		// ]);
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file-opt',$nama_file);
 
		// import data
        Excel::import(new DistractionImport, public_path('/file-opt/'.$nama_file));
        
        notice('success','Data Berhasil di Import');
              
        return back();
    }
}