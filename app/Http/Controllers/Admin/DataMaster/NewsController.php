<?php

namespace App\Http\Controllers\Admin\DataMaster;

use App\Http\Controllers\Controller;
use App\Http\Imports\CommodityImport;
use App\Http\Imports\DistractionImport;
use App\Repositories\Entities\News;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class NewsController extends Controller
{

    protected $news;
    
    public function __construct(News $news)
    {
        $this->news = $news;
    }
    
    public function index(Request $request)
    {
        $query = $this->news->query();

        if ($request->has('q')) {
            $query->where('name', 'like','%'.$request->q.'%');
        }

        $news = $query->latest()->paginate(20);

        return view('datamaster.news.index', compact('news'));
    }

    public function create(Request $request)
    {
        return view('datamaster.news.create');
    }

    public function store(Request $request)
    {       
        $data = [
            'title' => $request->name,
            'created_by' => logged_in_user()->id,
            'text' => $request->text,
            'slug' => Str::slug($request->name, '-')
        ];

        if($request->hasFile('img')){
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('img')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('img')->storeAs('uploads/news/',$fileNameToStore, ['disk' => 'public_uploads']);
            $data['image'] = $fileNameToStore;
        }

        News::create($data);
   
        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.datamaster.news.index');
    }

    public function edit($id)
    {
        $news = $this->news->find($id);

        return view('datamaster.news.edit', compact('news'));
    }

    public function update(Request $request, $id)
    {
        $data = [
            'title' => $request->name,
            'created_by' => logged_in_user( )->id,
            'text' => $request->text
        ];

        if($request->hasFile('img')){
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('img')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('img')->storeAs('uploads/news/',$fileNameToStore, ['disk' => 'public_uploads']);
            $data['image'] = $fileNameToStore;
        }

        News::where('id', $id)->update($data);

        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.datamaster.news.index');
    }

    public function delete($id)
    {
        $role = $this->news->where('id', $id)->delete();

        notice('success','Data Berhasil di Hapus');

        return redirect()->route('admin.datamaster.news.index');
    }

}