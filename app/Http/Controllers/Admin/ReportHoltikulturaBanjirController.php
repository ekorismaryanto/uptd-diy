<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Exports\ReportExcelExport;
use App\Http\Exports\ReportHolticultureBanjirExcelExport;
use App\Http\Exports\ReportTanamanPanganExcelExport;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportHolticultureFlood;
use App\Repositories\ReportHoltikulturaBanjirEloquent;
use App\Repositories\ReportTanamanPanganBanjirEloquent;
use App\Repositories\ReportTanamanPanganEloquent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF1;

class ReportHoltikulturaBanjirController extends Controller
{

    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, ReportHoltikulturaBanjirEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }
    

    public function index(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, true);

        return view('admin.report.holtikultura.banjir.index', compact('reports'));
    }

    public function edit($id)
    {
        $opt = ReportHolticultureFlood::find($id);

        return view('data.hortikultura.banjir.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $data['report_data'] = $request->all();
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $request = [
                'periode' => $data['report_data']['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'varieties' => $data['report_data']['varietas'][$key],
                'age' => $data['report_data']['umur'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'broadly_alert' => $data['report_data']['broadly_alert'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key],
                'previous_month_rest_receding_area' => $data['report_data']['previous_month_rest_receding_area'][$key],
                'previous_month_rest_receding_description' => $data['report_data']['previous_month_rest_receding_description'][$key],
                'previous_month_rest_puso_area' => $data['report_data']['previous_month_rest_puso_area'][$key],
                'previous_month_rest_puso_description' => $data['report_data']['previous_month_rest_puso_description'][$key],
                'more_area_added' => $data['report_data']['more_area_added'][$key],
                'puso_area_added' => $data['report_data']['puso_area_added'][$key],
                'more_area_state_period' => $data['report_data']['more_area_state_period'][$key],
                'puso_area_state_period' => $data['report_data']['puso_area_state_period'][$key],
                'handling_effort' => $data['report_data']['handling_effort'][$key],
                'total_handling_effort' => $data['report_data']['total_handling_effort'][$key],
                'coordinate' => $data['report_data']['coordinate'][$key],
            ];

            $dataReportCrop = ReportHolticultureFlood::find($id)->update($request);
        }

        notice('success', 'Data Berhasil diubah');

        return redirect()->route('admin.report.holtikultura.banjir.index');
    }

    public function delete($id)
    {
        $opt = ReportHolticultureFlood::where('id', $id)->delete();

        notice('success', 'data berhasil di hapus');
        return redirect()->route('admin.report.holtikultura.banjir.index');
    }

    public function detail($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.detail', compact('report'));
    }

    public function detailPrint($id)
    {
        $report = $this->report->find($id);

        return view('export.print', compact('report'));
    }

    public function detailPrintPdf($id)
    {
        $report = $this->report->find($id);

        $pdf = PDF1::loadview('export.print', compact('report'));

        return $pdf->download('report.pdf');
    }
    
    public function print(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);

        return view('admin.report.holtikultura.banjir.export.excel', compact('reports'));
    }

    public function xls(Request $request)
    {
        return Excel::download(new ReportHolticultureBanjirExcelExport($request->all()), 'report.xlsx');
    }

    public function csv(Request $request)
    {
        return Excel::download(new ReportHolticultureBanjirExcelExport($request->all()), 'report.csv');
    }

    public function pdf(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);
     
        $pdf = PDF1::loadview('admin.report.holtikultura.banjir.export.pdf',
        compact('reports'), 
        [], 
        [ 
          'title' => 'Certificate', 
          'format' => 'Legal',
          'orientation' => 'L'
        ]);

        return $pdf->download('report-holtikultura.pdf');
    }

}