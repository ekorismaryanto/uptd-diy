<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Exports\ReportExcelExport;
use App\Http\Exports\ReportTanamanPanganBanjirExcelExport;
use App\Http\Exports\ReportTanamanPanganExcelExport;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportCropFlood;
use App\Repositories\ReportTanamanPanganBanjirEloquent;
use App\Repositories\ReportTanamanPanganEloquent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF1;

class ReportTanamanPanganBanjirController extends Controller
{

    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, ReportTanamanPanganBanjirEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }
    

    public function index(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, true);

        return view('admin.report.tanaman-pangan.banjir.index', compact('reports'));
    }

    public function edit($id)
    {
        $opt = ReportCropFlood::find($id);

        return view('data.tanaman-pangan.banjir.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        foreach ($data['sub_district_id'] as $key => $value) {
            $request = [
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['sub_district_id'][$key],
                'village_id' => $data['village_id'][$key],
                'commodity_id' => $data['commodity_id'][$key],
                'varieties' => $data['varietas'][$key],
                'age' => $data['umur'][$key],
                'planting_area' => $data['planting_area'][$key],
                'broadly_alert' => $data['broadly_alert'][$key],
                'periode_tb' => $data['periode_tb'][$key],
                'previous_month_rest_receding_area' => $data['previous_month_rest_receding_area'][$key],
                'previous_month_rest_receding_description' => $data['previous_month_rest_receding_description'][$key],
                'previous_month_rest_puso_area' => $data['previous_month_rest_puso_area'][$key],
                'previous_month_rest_puso_description' => $data['previous_month_rest_puso_description'][$key],
                'more_area_added' => $data['more_area_added'][$key],
                'puso_area_added' => $data['puso_area_added'][$key],
                'more_area_state_period' => $data['more_area_state_period'][$key],
                'puso_area_state_period' => $data['puso_area_state_period'][$key],
                'handling_effort' => $data['handling_effort'][$key],
                'total_handling_effort' => $data['total_handling_effort'][$key],
                'coordinate' => $data['coordinate'][$key],
            ];

            $dataReportCrop = ReportCropFlood::where('id', $id)->update($request);
        }

        notice('success', 'data berhasil di update');

        return redirect()->route('admin.report.tanaman-pangan.banjir.index');
    }

    public function delete($id)
    {
        $opt = ReportCropFlood::where('id', $id)->delete();

        notice('success', 'data berhasil di hapus');
        return redirect()->route('admin.report.tanaman-pangan.banjir.index');
    }

    public function detail($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.detail', compact('report'));
    }

    public function detailPrint($id)
    {
        $report = $this->report->find($id);

        return view('export.print', compact('report'));
    }

    public function detailPrintPdf($id)
    {
        $report = $this->report->find($id);

        $pdf = PDF1::loadview('export.print', compact('report'));

        return $pdf->download('report.pdf');
    }
    
    public function print(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);

        return view('admin.report.tanaman-pangan.banjir.export.excel', compact('reports'));
    }

    public function xls(Request $request)
    {
        return Excel::download(new ReportTanamanPanganBanjirExcelExport($request->all()), 'report.xlsx');
    }

    public function csv(Request $request)
    {
        return Excel::download(new ReportTanamanPanganBanjirExcelExport($request->all()), 'report.csv');
    }

    public function pdf(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);
     
        $pdf = PDF1::loadview('admin.report.tanaman-pangan.banjir.export.pdf',
        compact('reports'), 
        [], 
        [ 
          'title' => 'Certificate', 
          'format' => 'Legal',
          'orientation' => 'L'
        ]);

        return $pdf->download('report-tanaman-pangan.pdf');
    }

}