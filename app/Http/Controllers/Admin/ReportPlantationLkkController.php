<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Exports\ReportExcelExport;
use App\Http\Exports\ReportPlantationLkkExcelExport;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportPlantationLkk;
use App\Repositories\ReportPlantationLkkEloquent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF1;

class ReportPlantationLkkController extends Controller
{

    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, ReportPlantationLkkEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }
    

    public function index(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, true);

        return view('admin.report.perkebunan.lkk.index', compact('reports'));
    }

    public function edit($id)
    {
        $opt = ReportPlantationLkk::find($id);

        return view('data.perkebunan.lkk.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $data['report_data'] = $request->all();

        foreach ($data['report_data']['sub_district_id'] as $key => $value) {

            $report = ReportPlantationLkk::where('id', $id)->update(
                [
                    "periode" => date('Y-m-d', strtotime($data['report_data']['date'])),
                    "sub_district_id" => $data['report_data']['sub_district_id'][$key],
                    "commodity_id" => $data['report_data']['commodity_id'][$key],
                    "planting_area" => $data['report_data']['planting_area'][$key],
                    "age" => $data['report_data']['age'][$key],
                    "age_unit" => $data['report_data']['age_unit'][$key],
                    "attact_area" => $data['report_data']['attact_area'][$key],
                    "deskripsi_opt" => $data['report_data']['deskripsi_opt'][$key],
                ]
            );
        }

        notice('success', 'data berhasil di update');

        return redirect()->route('admin.report.perkebunan.lkk.index');
    }

    public function detail($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.detail', compact('report'));
    }

    public function detailPrint($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.perkebunan.pdf', compact('report'));
    }

    public function detailPrintPdf($id)
    {
        $report = $this->report->find($id);

        $pdf = PDF1::loadview('admin.report.perkebunan.pdf', compact('report'));

        return $pdf->download('admin.report.perkebunan.pdf');
    }
    
    public function print(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);

        return view('admin.report.perkebunan.lkk.export.pdf', compact('reports'));
    }

    public function xls(Request $request)
    {
        return Excel::download(new ReportPlantationLkkExcelExport($request->all()), 'report.xlsx');
    }

    public function csv(Request $request)
    {
        return Excel::download(new ReportPlantationLkkExcelExport($request->all()), 'report.csv');
    }

    public function pdf(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);
     
        $pdf = PDF1::loadview('admin.report.perkebunan.lkk.export.pdf', compact('reports'));

        return $pdf->download('admin.report.perkebunan.lkk.export.pdf');
    }

    public function delete($id)
    {
        $opt = ReportPlantationLkk::where('id', $id)->delete();

        notice('success', 'data berhasil di hapus');
        return redirect()->route('admin.report.perkebunan.lkk.index');
    }

}