<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Exports\ReportExcelExport;
use App\Http\Exports\ReportTanamanPanganExcelExport;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportCrop;
use App\Repositories\ReportTanamanPanganEloquent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF1;

class ReportTanamanPanganController extends Controller
{

    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, ReportTanamanPanganEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }
    

    public function index(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, true);

        return view('admin.report.tanaman-pangan.index', compact('reports'));
    }

    public function create(Request $request)
    {
        return view('admin.report.tanaman-pangan.create');
    }

    public function edit(Request $request, $id)
    {
        $opt = ReportCrop::find($id);
        
        return view('data.tanaman-pangan.opt.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        foreach ($data['sub_district_id'] as $key => $value) {
            $param['report_crop'] = [
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['sub_district_id'][$key],
                'village_id' => $data['village_id'][$key],
                'commodity_id' => $data['commodity_id'][$key],
                'varieties' => $data['varietas'][$key],
                'age' => $data['umur'][$key],
                'planting_area' => $data['planting_area'][$key],
                'opt_id' => $data['opts'][$key],
                'criteria_opt' => $data['criteria_opt'][$key],
                'periode_tb' => $data['periode_tb'][$key]
            ];

            $param['report_crops_previous_month'] = [
                'ringan' => $data['sisa_serangan_r'][$key],
                'sedang' => $data['sisa_serangan_s'][$key],
                'berat' => $data['sisa_serangan_b'][$key],
                'puso' => $data['sisa_serangan_p'][$key],
                'retrained_area' => $data['sisa_serangan_terkendali'][$key],
                'harvest_area' => $data['sisa_serangan_panen'][$key],
            ];


            $param['report_crops_area_added_atacts'] = [
                'ringan' => $data['tambah_serangan_r'][$key],
                'sedang' => $data['tambah_serangan_s'][$key],
                'berat' => $data['tambah_serangan_b'][$key],
                'puso' => $data['tambah_serangan_p'][$key],
            ];


            $param['report_crops_attact_state'] = [
                'ringan' => $data['keadaa_serangan_r'][$key],
                'sedang' => $data['keadaa_serangan_s'][$key],
                'berat' => $data['keadaa_serangan_b'][$key],
                'puso' => $data['keadaa_serangan_p'][$key],
            ];

            $param['report_crops_control_area'] = [
                'kimia' => $data['pestisida_kimia'][$key],
                'hayati' => $data['pestisida_hayati'][$key],
                'eradikasi' => $data['non_pestisida_eradiksi'][$key],
                'others' => $data['non_pestisida_lain'][$key],
            ];


            $dataReportCrop = ReportCrop::find($id);

            $dataReportCrop->update($param['report_crop']);

            $dataReportCrop->sisaPeriode()->update($param['report_crops_previous_month']);
            $dataReportCrop->luasTambahSerangan()->update($param['report_crops_area_added_atacts']);
            $dataReportCrop->luasPengendalian()->update($param['report_crops_control_area']);
            $dataReportCrop->keadaanSerangan()->update($param['report_crops_attact_state']);
        }

        notice('success', 'Data Berhasil di Update');

        return redirect()->route('admin.report.tanaman-pangan.index');
      
    }

    public function delete($id)
    {
        $opt = ReportCrop::where('id', $id)->delete();

        notice('success', 'data berhasil di hapus');
        return redirect()->route('admin.report.tanaman-pangan.index');
    }

    public function detail($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.detail', compact('report'));
    }

    public function detailPrint($id)
    {
        $report = $this->report->find($id);

        return view('export.print', compact('report'));
    }

    public function detailPrintPdf($id)
    {
        $report = $this->report->find($id);

        $pdf = PDF1::loadview('export.print', compact('report'));

        return $pdf->download('report.pdf');
    }
    
    public function print(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);

        return view('admin.report.tanaman-pangan.export.excel', compact('reports'));
    }

    public function xls(Request $request)
    {
        return Excel::download(new ReportTanamanPanganExcelExport($request->all()), 'report.xlsx');
    }

    public function csv(Request $request)
    {
        return Excel::download(new ReportTanamanPanganExcelExport($request->all()), 'report.csv');
    }

    public function pdf(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);
     
        $pdf = PDF1::loadview('admin.report.tanaman-pangan.export.pdf',
        compact('reports'), 
        [], 
        [ 
          'title' => 'Certificate', 
          'format' => 'Legal',
          'orientation' => 'L'
        ]);

        return $pdf->download('report-tanaman-pangan.pdf');
    }

}