<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Exports\ReportExcelExport;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\District;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF1;

class ReportPlantationController extends Controller
{

    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, DataReportEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }
    

    public function index(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, true);

        return view('admin.report.perkebunan.index', compact('reports'));
    }

    public function edit($id)
    {
        $opt = DataReport::find($id);

        return view('data.perkebunan.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $data['report_data'] = $request->all();

        $areas = array_values($data['report_data']['control_areas']);

        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $report = DataReport::find($id);

            $report->update(
                [
                    "date" => date('Y-m-d', strtotime($data['report_data']['date'])),
                    "sub_district_id" => $data['report_data']['sub_district_id'][$key],
                    "type_commodity" => 3,
                    "commodity_id" => $data['report_data']['commodity_id'][$key],
                    "itensity_attack_easy" => $data['report_data']['itensity_attack_easy'][$key],
                    "itensity_attack_hard" => $data['report_data']['itensity_attack_hard'][$key],
                    "control_area_apbn" => $data['report_data']['itensity_attack_easy'][$key],
                    "control_area_apbd_1" => $data['report_data']['control_area_apbd_1'][$key],
                    "control_area_apbd_2" => $data['report_data']['control_area_apbd_2'][$key],
                    "control_area_apbd_public" => $data['report_data']['control_area_apbd_public'][$key],
                    "price_average" => $data['report_data']['price_average'][$key],
                    "planting_area" => $data['report_data']['planting_area'][$key]
                ]
            );
    
            if ($report) {
                $report->optDatas()->delete();
                $report->optDatas()->create([
                    'opt_id' => $data['report_data']['opts'][$key]
                ]);

                $area = [];
                foreach ($areas[$key] as $key => $row) {
                    $area[] = [
                        'control_area' => $row
                    ];
                }
    
                if (!empty($area)) {
                    $report->controlAreas()->delete();
                    $report->controlAreas()->createMany($area);
                }
            }
        }

        notice('success', 'data berhasil di update');

        return redirect()->route('admin.report.perkebunan.index');
    }

    public function detail($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.detail', compact('report'));
    }

    public function detailPrint($id)
    {
        $report = $this->report->find($id);

        return view('export.print', compact('report'));
    }

    public function detailPrintPdf($id)
    {
        $report = $this->report->find($id);

        $pdf = PDF1::loadview('export.print', compact('report'));

        return $pdf->download('report.pdf');
    }
    
    public function print(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);

        return view('export.report', compact('reports'));
    }

    public function xls(Request $request)
    {
        return Excel::download(new ReportExcelExport($request->all()), 'report.xlsx');
    }

    public function csv(Request $request)
    {
        return Excel::download(new ReportExcelExport($request->all()), 'report.csv');
    }

    public function pdf(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);
     
        $pdf = PDF1::loadview('export.pdf-report', compact('reports'));

        return $pdf->download('report.pdf');
    }

    public function delete($id)
    {
        $opt = DataReport::where('id', $id)->delete();

        notice('success', 'data berhasil di hapus');
        return redirect()->route('admin.report.perkebunan.index');
    }

}