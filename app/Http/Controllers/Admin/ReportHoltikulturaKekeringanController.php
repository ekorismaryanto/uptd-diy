<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Exports\ReportExcelExport;
use App\Http\Exports\ReportHoltikulturaKekeringanExcelExport;
use App\Http\Exports\ReportTanamanPanganExcelExport;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportHolticultureDrought;
use App\Repositories\ReportTanamanPanganBanjirEloquent;
use App\Repositories\ReportTanamanPanganEloquent;
use App\Repositories\ReportHolticultureKekeringanEloquent;
use App\Repositories\ReportHoltikulturaKekeringanEloquent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF1;

class ReportHoltikulturaKekeringanController extends Controller
{

    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, ReportHoltikulturaKekeringanEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }
    

    public function index(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, true);

        return view('admin.report.holtikultura.kekeringan.index', compact('reports'));
    }

    public function edit($id)
    {
        $opt = ReportHolticultureDrought::find($id);

        return view('data.hortikultura.kekeringan.edit', compact('opt'));
    }

    public function update(Request $request, $id)
    {
        $data['report_data'] = $request->all();
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $request = [
                'periode' => $data['report_data']['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'varieties' => $data['report_data']['varietas'][$key],
                'age' => $data['report_data']['umur'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'broadly_alert' => $data['report_data']['broadly_alert'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key],
                'previous_periode_low' => $data['report_data']['previous_periode_low'][$key],
                'previous_periode_mid' => $data['report_data']['previous_periode_mid'][$key],
                'previous_periode_hight' => $data['report_data']['previous_periode_hight'][$key],
                'previous_periode_puso' => $data['report_data']['previous_periode_puso'][$key],
                'previous_periode_recover' => $data['report_data']['previous_periode_recover'][$key],
                'area_added_periode_low' => $data['report_data']['area_added_periode_low'][$key],
                'area_added_periode_mid' => $data['report_data']['area_added_periode_mid'][$key],
                'area_added_periode_hight' => $data['report_data']['area_added_periode_hight'][$key],
                'area_added_periode_puso' => $data['report_data']['area_added_periode_puso'][$key],
                'area_state_periode_low' => $data['report_data']['area_state_periode_low'][$key],
                'area_state_periode_mid' => $data['report_data']['area_state_periode_mid'][$key],
                'area_state_periode_hight' => $data['report_data']['area_state_periode_hight'][$key],
                'area_state_periode_puso' => $data['report_data']['area_state_periode_puso'][$key],
                'handling_effort' => $data['report_data']['handling_effort'][$key],
                'handling_effort_total' => $data['report_data']['handling_effort_total'][$key],
                'coordinate' => $data['report_data']['coordinate'][$key],
                'description' => $data['report_data']['description'][$key],
            ];

            $dataReportCrop = ReportHolticultureDrought::find($id)->update($request);
        }


        notice('success', 'Data Berhasil diubah');

        return redirect()->route('admin.report.holtikultura.kekeringan.index');
    }

    public function delete($id)
    {
        $opt = ReportHolticultureDrought::where('id', $id)->delete();

        notice('success', 'data berhasil di hapus');
        return redirect()->route('admin.report.holtikultura.kekeringan.index');
    }

    public function detail($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.detail', compact('report'));
    }

    public function detailPrint($id)
    {
        $report = $this->report->find($id);

        return view('export.print', compact('report'));
    }

    public function detailPrintPdf($id)
    {
        $report = $this->report->find($id);

        $pdf = PDF1::loadview('export.print', compact('report'));

        return $pdf->download('report.pdf');
    }
    
    public function print(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);

        return view('admin.report.holtikultura.kekeringan.export.excel', compact('reports'));
    }

    public function xls(Request $request)
    {
        return Excel::download(new ReportHoltikulturaKekeringanExcelExport($request->all()), 'report.xlsx');
    }

    public function csv(Request $request)
    {
        return Excel::download(new ReportHoltikulturaKekeringanExcelExport($request->all()), 'report.csv');
    }

    public function pdf(Request $request)
    {
        $reports = $this->report->fetch($request->all(), false, false);
     
        $pdf = PDF1::loadview('admin.report.holtikultura.kekeringan.export.pdf',
        compact('reports'), 
        [], 
        [ 
          'title' => 'Certificate', 
          'format' => 'Legal',
          'orientation' => 'L'
        ]);

        return $pdf->download('report-holtikultura.pdf');
    }

}