<?php

namespace App\Http\Controllers\Admin\Data;

use App\Http\Constants\TypeCommodity;
use App\Http\Controllers\Controller;
use App\Http\Requests\DataReportOptRequest;
use App\Http\Requests\DataReportRequest;
use App\Repositories\DataReportEloquent;
use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\District;
use App\Repositories\Entities\ReportCrop;
use App\Repositories\Entities\ReportCropFlood;
use App\Repositories\Entities\ReportCropsDrought;
use App\Repositories\Entities\ReportHolticulture;
use App\Repositories\Entities\ReportHolticultureDrought;
use App\Repositories\Entities\ReportHolticultureFlood;
use App\Repositories\Entities\ReportPlantationDpi;
use App\Repositories\Entities\ReportPlantationLkk;
use Illuminate\Http\Request;
use Session;

class DataReportController extends Controller
{
    protected $commodity, $district, $report;
    
    public function __construct(Commodity $commodity, District $district, DataReportEloquent $report)
    {
        $this->commodity = $commodity;
        $this->district = $district;
        $this->report = $report;
    }

    public function create(Request $request)
    {
        Session::forget('form_1');
        Session::forget('form_2');

        return view('data.input-data');
    }

    public function createStep1(Request $request)
    {
        Session::forget('form_2');
        $request->session()->put('form_1', $request->all());

        $districts = $this->district->get();
        $commodities = $this->commodity->get();
        switch ($request->input('type_commodity')) {
            case '1':

                switch ($request->input('type_opt_pangan')) {
                    case '1':
                        return view('data.tanaman-pangan.opt.create', compact('districts','commodities'));
                        break;
                    case '2':
                        return view('data.tanaman-pangan.banjir.create', compact('districts','commodities'));
                        break;
                    case '3':
                        return view('data.tanaman-pangan.kekeringan.create', compact('districts','commodities'));
                        break;
                }
                break;

            case '2':
                switch ($request->input('type_opt_hortikultura')) {
                    case '1':
                        return view('data.hortikultura.opt.create', compact('districts','commodities'));
                        break;
                    case '2':
                        return view('data.hortikultura.banjir.create', compact('districts','commodities'));
                        break;
                    case '3':
                        return view('data.hortikultura.kekeringan.create', compact('districts','commodities'));
                        break;
                }
                break;

            case '3':
                switch ($request->input('type_opt_perkebunan')) {
                    case '1':
                        return view('data.perkebunan.create-opt', compact('districts','commodities'));
                        break;
                    case '2':
                        return view('data.perkebunan.dpi.create', compact('districts','commodities'));
                        break;
                    case '3':
                        return view('data.perkebunan.lkk.create', compact('districts','commodities'));
                        break;
                }
                break;
            
            default:
                # code...
                break;
        }

    }

    public function preview(Request $request)
    {
        $request->session()->put('form_2', $request->all());

        $data1 = $request->session()->get('form_1');
        $data2['report_data'] = $request->session()->get('form_2');

        $data = array_merge($data1, $data2);


        switch ($data['type_commodity']) {
            case '1':

                switch ($data['type_opt_pangan']) {
                    case '1':
                        return view('data.tanaman-pangan.opt.preview', compact('data'));
                        break;
                    case '2':
                        return view('data.tanaman-pangan.banjir.preview', compact('data'));
                        break;
                    case '3':
                        return view('data.tanaman-pangan.kekeringan.preview', compact('data'));
                        break;
                }
                break;

            case '2':
                switch ($data['type_opt_hortikultura']) {
                    case '1':
                        return view('data.hortikultura.opt.preview', compact('districts','commodities','data'));
                        break;
                    case '2':
                        return view('data.hortikultura.banjir.preview', compact('districts','commodities','data'));
                        break;
                    case '3':
                        return view('data.hortikultura.kekeringan.preview', compact('districts','commodities','data'));
                        break;
                }
                break;
            case '3':
                switch ($data['type_opt_perkebunan']) {
                    case '1':
                        return view('data.perkebunan.preview', compact('districts','commodities', 'data'));
                        break;
                    case '2':
                        return view('data.perkebunan.dpi.preview', compact('districts','commodities','data'));
                        break;
                    case '3':
                        return view('data.perkebunan.lkk.preview', compact('districts','commodities','data'));
                        break;
                }
                break;
            
            default:
                # code...
                break;
        }



        return view('data.perkebunan.preview', compact('data'));
    }

    public function store(Request $request)
    {
        $data1 = $request->session()->get('form_1');
        $data2['report_data'] = $request->session()->get('form_2');

        $data = array_merge($data1, $data2);

        switch ($data['type_commodity']) {
            case '1':

                switch ($data['type_opt_pangan']) {
                    case '1':
                        $this->tanamanPangan($data);
                        break;
                    case '2':
                        $this->tanamanPanganBanjir($data);
                    break;
                    case '3':
                        $this->tanamanPanganKekeringan($data);
                        break;
                }
                break;

            case '2':
                switch ($data['type_opt_hortikultura']) {
                    case '1':
                        $this->hortikultura($data);
                        break;
                    case '2':
                        $this->hortikulturaBanjir($data);

                        break;
                    case '3':
                        $this->hortikulturaKekeringan($data);
                        break;
                }
                break;
            case '3':
                switch ($data['type_opt_perkebunan']) {
                    case '1':
                        $this->perkebunan($data);
                        break;
                    case '2':
                        $this->perkebunanDpi($data);
                        break;
                    case '3':
                        $this->perkebunanLkk($data);
                        break;
                }
                break;
            
            default:
                # code...
                break;
        }


        Session::forget('form_1');
        Session::forget('form_2');

        notice('success','Data Berhasil di Simpan');

        return redirect()->route('admin.index');
    }

    private function perkebunan($data)
    {
        $areas = array_values($data['report_data']['control_areas']);

        foreach ($data['report_data']['sub_district_id'] as $key => $value) {

            $report = DataReport::create(
                [
                    "description" => $data['description'],
                    "user_id" => logged_in_user()->id,
                    "date" => $data['date'].'-01',
                    "sub_district_id" => $data['report_data']['sub_district_id'][$key],
                    "type_commodity" => 3,
                    "commodity_id" => $data['report_data']['commodity_id'][$key],
                    "itensity_attack_easy" => $data['report_data']['itensity_attack_easy'][$key],
                    "itensity_attack_hard" => $data['report_data']['itensity_attack_hard'][$key],
                    "control_area_apbn" => $data['report_data']['itensity_attack_easy'][$key],
                    "control_area_apbd_1" => $data['report_data']['control_area_apbd_1'][$key],
                    "control_area_apbd_2" => $data['report_data']['control_area_apbd_2'][$key],
                    "control_area_apbd_public" => $data['report_data']['control_area_apbd_public'][$key],
                    "price_average" => $data['report_data']['price_average'][$key],
                    "planting_area" => $data['report_data']['planting_area'][$key]
                ]
            );
    
            if ($report) {
                
                $report->optDatas()->create([
                    'opt_id' => $data['report_data']['opts'][$key]
                ]);

                $area = [];
                foreach ($areas[$key] as $key => $row) {
                    $area[] = [
                        'control_area' => $row
                    ];
                }
    
                if (!empty($area)) {
                    $report->controlAreas()->createMany($area);
                }
            }
        }
    }

    private function perkebunanDpi($data)
    {
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {

            $report = ReportPlantationDpi::create(
                [
                    "description" => $data['description'],
                    "user_id" => logged_in_user()->id,
                    "periode" => date('Y-m-d', strtotime($data['date'])),
                    "sub_district_id" => $data['report_data']['sub_district_id'][$key],
                    "commodity_id" => $data['report_data']['commodity_id'][$key],
                    "planting_area" => $data['report_data']['planting_area'][$key],
                    "age" => $data['report_data']['age'][$key],
                    "age_unit" => $data['report_data']['age_unit'][$key],
                    "dry_exposed" => $data['report_data']['dry_exposed'][$key],
                    "dry_lightly" => $data['report_data']['dry_lightly'][$key],
                    "dry_handling" => $data['report_data']['dry_handling'][$key],
                    "flood_hit" => $data['report_data']['flood_hit'][$key],
                    "light_flood" => $data['report_data']['light_flood'][$key],
                    "flood_handling" => $data['report_data']['flood_handling'][$key],
                    "fire_hit" => $data['report_data']['fire_hit'][$key],
                    "light_fire" => $data['report_data']['light_fire'][$key],
                    "fire_handling" => $data['report_data']['fire_handling'][$key],
                ]
            );
        }
    }

    private function perkebunanLkk($data)
    {
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {

            $report = ReportPlantationLkk::create(
                [
                    "description" => $data['description'],
                    "user_id" => logged_in_user()->id,
                    "periode" => date('Y-m-d', strtotime($data['date'])),
                    "sub_district_id" => $data['report_data']['sub_district_id'][$key],
                    "commodity_id" => $data['report_data']['commodity_id'][$key],
                    "planting_area" => $data['report_data']['planting_area'][$key],
                    "age" => $data['report_data']['age'][$key],
                    "age_unit" => $data['report_data']['age_unit'][$key],
                    "attact_area" => $data['report_data']['attact_area'][$key],
                    "deskripsi_opt" => $data['report_data']['deskripsi_opt'][$key],
                ]
            );
        }
    }


    private function tanamanPangan($data)
    {
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $data['report_crop'] = [
                'user_id' => logged_in_user()->id,
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'varieties' => $data['report_data']['varietas'][$key],
                'age' => $data['report_data']['umur'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'opt_id' => $data['report_data']['opts'][$key],
                'criteria_opt' => $data['report_data']['criteria_opt'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key]
            ];

            $data['report_crops_previous_month'] = [
                'ringan' => $data['report_data']['sisa_serangan_r'][$key],
                'sedang' => $data['report_data']['sisa_serangan_s'][$key],
                'berat' => $data['report_data']['sisa_serangan_b'][$key],
                'puso' => $data['report_data']['sisa_serangan_p'][$key],
                'retrained_area' => $data['report_data']['sisa_serangan_terkendali'][$key],
                'harvest_area' => $data['report_data']['sisa_serangan_panen'][$key],
            ];


            $data['report_crops_area_added_atacts'] = [
                'ringan' => $data['report_data']['tambah_serangan_r'][$key],
                'sedang' => $data['report_data']['tambah_serangan_s'][$key],
                'berat' => $data['report_data']['tambah_serangan_b'][$key],
                'puso' => $data['report_data']['tambah_serangan_p'][$key],
            ];


            $data['report_crops_attact_state'] = [
                'ringan' => $data['report_data']['keadaa_serangan_r'][$key],
                'sedang' => $data['report_data']['keadaa_serangan_s'][$key],
                'berat' => $data['report_data']['keadaa_serangan_b'][$key],
                'puso' => $data['report_data']['keadaa_serangan_p'][$key],
            ];

            $data['report_crops_control_area'] = [
                'kimia' => $data['report_data']['pestisida_kimia'][$key],
                'hayati' => $data['report_data']['pestisida_hayati'][$key],
                'eradikasi' => $data['report_data']['non_pestisida_eradiksi'][$key],
                'others' => $data['report_data']['non_pestisida_lain'][$key],
            ];


            $dataReportCrop = ReportCrop::create($data['report_crop']);

            $dataReportCrop->sisaPeriode()->create($data['report_crops_previous_month']);
            $dataReportCrop->luasTambahSerangan()->create($data['report_crops_area_added_atacts']);
            $dataReportCrop->luasPengendalian()->create($data['report_crops_control_area']);
            $dataReportCrop->keadaanSerangan()->create($data['report_crops_attact_state']);
        }

        return true;
    }

    private function tanamanPanganBanjir($data)
    {
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $request = [
                'user_id' => logged_in_user()->id,
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'varieties' => $data['report_data']['varietas'][$key],
                'age' => $data['report_data']['umur'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'broadly_alert' => $data['report_data']['broadly_alert'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key],
                'previous_month_rest_receding_area' => $data['report_data']['previous_month_rest_receding_area'][$key],
                'previous_month_rest_receding_description' => $data['report_data']['previous_month_rest_receding_description'][$key],
                'previous_month_rest_puso_area' => $data['report_data']['previous_month_rest_puso_area'][$key],
                'previous_month_rest_puso_description' => $data['report_data']['previous_month_rest_puso_description'][$key],
                'more_area_added' => $data['report_data']['more_area_added'][$key],
                'puso_area_added' => $data['report_data']['puso_area_added'][$key],
                'more_area_state_period' => $data['report_data']['more_area_state_period'][$key],
                'puso_area_state_period' => $data['report_data']['puso_area_state_period'][$key],
                'handling_effort' => $data['report_data']['handling_effort'][$key],
                'total_handling_effort' => $data['report_data']['total_handling_effort'][$key],
                'coordinate' => $data['report_data']['coordinate'][$key],
            ];

            $dataReportCrop = ReportCropFlood::create($request);
        }

        return true;
    }

    private function tanamanPanganKekeringan($data)
    {
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $request = [
                'user_id' => logged_in_user()->id,
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'varieties' => $data['report_data']['varietas'][$key],
                'age' => $data['report_data']['umur'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'broadly_alert' => $data['report_data']['broadly_alert'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key],
                'previous_periode_low' => $data['report_data']['previous_periode_low'][$key],
                'previous_periode_mid' => $data['report_data']['previous_periode_mid'][$key],
                'previous_periode_hight' => $data['report_data']['previous_periode_hight'][$key],
                'previous_periode_puso' => $data['report_data']['previous_periode_puso'][$key],
                'previous_periode_recover' => $data['report_data']['previous_periode_recover'][$key],
                'area_added_periode_low' => $data['report_data']['area_added_periode_low'][$key],
                'area_added_periode_mid' => $data['report_data']['area_added_periode_mid'][$key],
                'area_added_periode_hight' => $data['report_data']['area_added_periode_hight'][$key],
                'area_added_periode_puso' => $data['report_data']['area_added_periode_puso'][$key],
                'area_state_periode_low' => $data['report_data']['area_state_periode_low'][$key],
                'area_state_periode_mid' => $data['report_data']['area_state_periode_mid'][$key],
                'area_state_periode_hight' => $data['report_data']['area_state_periode_hight'][$key],
                'area_state_periode_puso' => $data['report_data']['area_state_periode_puso'][$key],
                'handling_effort' => $data['report_data']['handling_effort'][$key],
                'handling_effort_total' => $data['report_data']['handling_effort_total'][$key],
                'coordinate' => $data['report_data']['coordinate'][$key],
                'description' => $data['report_data']['description'][$key],
            ];

            $dataReportCrop = ReportCropsDrought::create($request);
        }

        return true;
    }

    private function hortikultura($data)
    {
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $request = [
                'user_id' => logged_in_user()->id,
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'opt_id' => $data['report_data']['opts'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'attact_area_r' => $data['report_data']['attact_area_r'][$key],
                'attact_area_s' => $data['report_data']['attact_area_s'][$key],
                'attact_area_b' => $data['report_data']['attact_area_b'][$key],
                'attact_area_p' => $data['report_data']['attact_area_p'][$key],
                'under_control' => $data['report_data']['under_control'][$key],
                'added_area_r' => $data['report_data']['added_area_r'][$key],
                'added_area_s' => $data['report_data']['added_area_s'][$key],
                'added_area_b' => $data['report_data']['added_area_b'][$key],
                'added_area_p' => $data['report_data']['added_area_p'][$key],
                'state_area_r' => $data['report_data']['state_area_r'][$key],
                'state_area_s' => $data['report_data']['state_area_s'][$key],
                'state_area_b' => $data['report_data']['state_area_b'][$key],
                'state_area_p' => $data['report_data']['state_area_p'][$key],
                'control_area_pem' => $data['report_data']['control_area_pem'][$key],
                'control_area_pest' => $data['report_data']['control_area_pest'][$key],
                'control_area_cl' => $data['report_data']['control_area_cl'][$key],
                'control_area_aph' => $data['report_data']['control_area_aph'][$key],
                'threatened_plant' => $data['report_data']['threatened_plant'][$key],
            ];

            ReportHolticulture::create($request);
        }

        return true;
    }

    private function hortikulturaBanjir($data)
    {
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $request = [
                'user_id' => logged_in_user()->id,
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'varieties' => $data['report_data']['varietas'][$key],
                'age' => $data['report_data']['umur'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'broadly_alert' => $data['report_data']['broadly_alert'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key],
                'previous_month_rest_receding_area' => $data['report_data']['previous_month_rest_receding_area'][$key],
                'previous_month_rest_receding_description' => $data['report_data']['previous_month_rest_receding_description'][$key],
                'previous_month_rest_puso_area' => $data['report_data']['previous_month_rest_puso_area'][$key],
                'previous_month_rest_puso_description' => $data['report_data']['previous_month_rest_puso_description'][$key],
                'more_area_added' => $data['report_data']['more_area_added'][$key],
                'puso_area_added' => $data['report_data']['puso_area_added'][$key],
                'more_area_state_period' => $data['report_data']['more_area_state_period'][$key],
                'puso_area_state_period' => $data['report_data']['puso_area_state_period'][$key],
                'handling_effort' => $data['report_data']['handling_effort'][$key],
                'total_handling_effort' => $data['report_data']['total_handling_effort'][$key],
                'coordinate' => $data['report_data']['coordinate'][$key],
            ];

            $dataReportCrop = ReportHolticultureFlood::create($request);
        }

        return true;
    }

    private function hortikulturaKekeringan($data)
    {
        foreach ($data['report_data']['sub_district_id'] as $key => $value) {
            $request = [
                'user_id' => logged_in_user()->id,
                'periode' => $data['date'].'-01',
                'sub_district_id' => $data['report_data']['sub_district_id'][$key],
                'village_id' => $data['report_data']['village_id'][$key],
                'commodity_id' => $data['report_data']['commodity_id'][$key],
                'varieties' => $data['report_data']['varietas'][$key],
                'age' => $data['report_data']['umur'][$key],
                'planting_area' => $data['report_data']['planting_area'][$key],
                'broadly_alert' => $data['report_data']['broadly_alert'][$key],
                'periode_tb' => $data['report_data']['periode_tb'][$key],
                'previous_periode_low' => $data['report_data']['previous_periode_low'][$key],
                'previous_periode_mid' => $data['report_data']['previous_periode_mid'][$key],
                'previous_periode_hight' => $data['report_data']['previous_periode_hight'][$key],
                'previous_periode_puso' => $data['report_data']['previous_periode_puso'][$key],
                'previous_periode_recover' => $data['report_data']['previous_periode_recover'][$key],
                'area_added_periode_low' => $data['report_data']['area_added_periode_low'][$key],
                'area_added_periode_mid' => $data['report_data']['area_added_periode_mid'][$key],
                'area_added_periode_hight' => $data['report_data']['area_added_periode_hight'][$key],
                'area_added_periode_puso' => $data['report_data']['area_added_periode_puso'][$key],
                'area_state_periode_low' => $data['report_data']['area_state_periode_low'][$key],
                'area_state_periode_mid' => $data['report_data']['area_state_periode_mid'][$key],
                'area_state_periode_hight' => $data['report_data']['area_state_periode_hight'][$key],
                'area_state_periode_puso' => $data['report_data']['area_state_periode_puso'][$key],
                'handling_effort' => $data['report_data']['handling_effort'][$key],
                'handling_effort_total' => $data['report_data']['handling_effort_total'][$key],
                'coordinate' => $data['report_data']['coordinate'][$key],
                'description' => $data['report_data']['description'][$key],
            ];

            $dataReportCrop = ReportHolticultureDrought::create($request);
        }

        return true;
    }

}