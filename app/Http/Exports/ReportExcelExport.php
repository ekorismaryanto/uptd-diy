<?php
  
namespace App\Http\Exports;

use App\Repositories\Entities\Commodity;
use App\Repositories\Entities\Opt;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;

class ReportExcelExport implements FromView
{
    protected $param;

    public function __construct($param)
    {
        $this->param = $param;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function view(): View
    {
        $reports = resolve(\App\Repositories\DataReportEloquent::class)->fetch($this->param);

        return view('export.report',[
            'reports' => $reports
        ]);
    }
}