<?php

namespace App\Http\Constants;

class CategoryDamage
{
    const LOW = 1;
    const MEDIUM = 2;
    const HIGH = 3;
    const PUSO = 4;

    public static function label($id = false): string
    {
        if ($id == false) {
            return '';
        }

        return static::labels()[$id];
    }

    public static function labels(): array
    {
        return [
            self::LOW => "Ringan",
            self::MEDIUM => "Sedang",
            self::HIGH => "Berat",
            self::PUSO => "Puso"
        ];
    }
}

