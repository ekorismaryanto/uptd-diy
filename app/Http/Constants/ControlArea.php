<?php

namespace App\Http\Constants;

class ControlArea
{
    const KT = 1;
    const M = 2;
    const B = 3;
    const K = 4;
    const OTHER = 5;

    public static function label($id = false): string
    {
        if ($id == false) {
            return '';
        }

        return static::labels()[$id];
    }

    public static function labels(): array
    {
        return [
            self::KT => "KT",
            self::M => "M",
            self::B => "B",
            self::K => "K",
            self::OTHER => "Lainnya"
        ];
    }

    public static function labelDescriptions(): array
    {
        return [
            self::KT => "KT - Kultur Teknis",
            self::M => "M - Mekanis",
            self::B => "B - Biologis",
            self::K => "K - Kimiawi",
            self::OTHER => "Lainnya"
        ];
    }
}

