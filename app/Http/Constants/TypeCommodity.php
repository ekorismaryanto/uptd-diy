<?php

namespace App\Http\Constants;

class TypeCommodity
{
    const CROPS = 1;
    const HOLTICULTURE = 2;
    const PLANTATION = 3;

    public static function label($id = false): string
    {
        if ($id == false) {
            return '';
        }

        return static::labels()[$id];
    }

    public static function labels(): array
    {
        return [
            self::CROPS => "Tanaman Pangan",
            self::HOLTICULTURE => "Hortikultura",
            self::PLANTATION => "Perkebunan"
        ];
    }
}

