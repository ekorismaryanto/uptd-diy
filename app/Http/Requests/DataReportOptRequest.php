<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Constants\UserType;

class DataReportOptRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Transform request
     *
     * @return array
     */
    public function data()
    {
        $result = [];

        $opts = array_values($this->opts);
        $control_areas = array_values($this->control_areas);

        $data = [];
        foreach ($this->sub_district_id as $key => $request) {
            $data[] = [
                "sub_district_id" => $request,
                "commodity_id" => $this->commodity_id[$key],
                "itensity_attack_easy" => $this->itensity_attack_easy[$key],
                "itensity_attack_hard" => $this->itensity_attack_hard[$key],
                "control_area_apbn" => $this->itensity_attack_easy[$key],
                "control_area_apbd_1" => $this->control_area_apbd_1[$key],
                "control_area_apbd_2" => $this->control_area_apbd_2[$key],
                "control_area_apbd_public" => $this->control_area_apbd_public[$key],
                "price_average" => $this->price_average[$key],
                "planting_area" => $this->planting_area[$key],
                "areas" => $control_areas[$key],
                "opts" => $opts[$key]
            ];
        }

        return $data;
    }
}