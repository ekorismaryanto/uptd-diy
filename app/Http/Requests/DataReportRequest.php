<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Constants\UserType;

class DataReportRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Transform request
     *
     * @return array
     */
    public function data()
    {
        $data['report'] = [
            'date' => date('Y-m-d', strtotime($this->date)),
            'user_id' => logged_in_user()->id,
            'commodity_id' => $this->commodity_id,
            'type_commodity' => $this->type_commodity,
            'sub_district_id' => $this->sub_district_id,
            'category_damage' => $this->category_damage,
            'surface_area' => $this->surface_area,
            'area_of_damage' => $this->area_of_damage,
            'control_area' => $this->control_area,
        ];
        
        $data['report_opt'] = [];
       
        if ($this->has('opt')) {
            foreach ($this->opt as $value) {
                $data['report_opt'][] = [
                    'type_opt' => 1,
                    'opt_id' => $value
                ];
            }
        }

        if ($this->has('opt_absolute')) {
            foreach ($this->opt_absolute as $value) {
                $data['report_opt'][] = [
                    'type_opt' => 2,
                    'opt_id' => $value
                ];
            }
        }

        if ($this->has('opt_non_absolute')) {
            foreach ($this->opt_non_absolute as $value) {
                $data['report_opt'][] = [
                    'type_opt' => 3,
                    'opt_id' => $value
                ];
            }
        }

        if ($this->has('opt_other')) {
            foreach ($this->opt_other as $value) {
                $data['report_opt'][] = [
                    'type_opt' => 4,
                    'opt_id' => $value
                ];
            }
        }

        return $data;
    }
}