<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Modules\User\Constants\UserType;

class ProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|unique:users,email,'.logged_in_user()->id.',id',
            'nip' => 'required',
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Transform request
     *
     * @return array
     */
    public function data()
    {
        $data['user'] = [
            'name' => $this->name,
            'nip' => $this->nip,
            'eselon' => $this->eselon,
            'type' => $this->type,
            'tmt_type' => date('Y-m-d', strtotime($this->tmt_type)),
            'position' => $this->position,
            'work_location' => $this->work_location,
            'status_employee' => $this->status_employee,
            'no_hp' => $this->no_hp,
            'email' => $this->email
        ]; 

        if ($this->password) {
            $data['user'] = array_merge($data['user'], [
                'password' => Hash::make($this->password)
            ]);
        }

        return $data;
    }
}