<?php


if (!function_exists('notice')) {
    function notice($labelClass, $content)
    {
        // Session::forget('notice');
        $notices = Session::get('notice');
        if (!is_array($notices))
            $notices = [];

        array_push($notices, [
            'labelClass' => $labelClass,
            'content' => $content
        ]);

        Session::put('notice', $notices);
    }
}

if (!function_exists('date_view')) {
    function date_view($value, $format = '%B %Y')
    {
        if (empty($value) || $value == '-') {
            return '-';
        }

        setlocale(LC_TIME, 'id_ID.utf8');
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($value)->formatLocalized($format);
    }
}


if(!function_exists("logged_in_user")){
    function logged_in_user() {
        return \Auth::user();
    }
}