<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportCropsAttactState extends Model
{    
    protected $table = 'report_crops_attact_state';

	protected $guarded = [
        'id',
    ];

}