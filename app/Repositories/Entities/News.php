<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{    
	protected $guarded = [
        'id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by','id');
    }

    public function getImageUrlAttribute()
    {
        
    }

}