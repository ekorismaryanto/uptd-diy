<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportCropAreaAddedAtact extends Model
{    

    protected $table = 'report_crops_area_added_atacts';

	protected $guarded = [
        'id',
    ];

}