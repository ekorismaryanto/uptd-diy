<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportCrop extends Model
{    
	protected $guarded = [
        'id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function commodity()
    {
        return $this->belongsTo(Commodity::class, 'commodity_id','id');
    }

    public function opt()
    {
        return $this->belongsTo(Opt::class, 'opt_id','id');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'village_id','id');
    }

    public function subDistrict()
    {
        return $this->belongsTo(SubDistrict::class, 'sub_district_id','id');
    }

    public function sisaPeriode()
    {
        return $this->hasOne(ReportCropsPreviousMonth::class, 'report_crop_id');
    }


    public function luasTambahSerangan()
    {
        return $this->hasOne(ReportCropAreaAddedAtact::class,'report_crop_id');
    }


    public function luasPengendalian()
    {
        return $this->hasOne(ReportCropsControlArea::class,'report_crop_id');
    }


    public function keadaanSerangan()
    {
        return $this->hasOne(ReportCropsAttactState::class,'report_crop_id');
    }

}