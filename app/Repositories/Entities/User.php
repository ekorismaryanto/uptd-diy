<?php

namespace App\Repositories\Entities;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    protected $connection = 'mysql';
    protected $table = 'users';

    use Notifiable,  HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $guarded = ['id'];

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $appends = [
        "profile_picture_url"
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'active' => 'boolean',
        'activated' => 'boolean'
    ];

    public function district()
    {
        return $this->belongsTo(District::class,'work_location', 'id');
    }

    public function getProfilePictureUrlAttribute()
    {
        return true;
    }
}
