<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataReportOpt extends Model
{    
	protected $guarded = [
        'id',
    ];

    public function opt()
    {
        return $this->belongsTo(Opt::class,'opt_id','id');
    }

    public function report()
    {
        return $this->belongsTo(DataReport::class,'report_data_id','id');
    }


}