<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataReportControlArea extends Model
{    
	protected $guarded = [
        'id',
    ];

    public function dataReport()
    {
        return $this->belongsTo(DataReport::class,'data_report_id','id');
    }


}