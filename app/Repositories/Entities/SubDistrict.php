<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubDistrict extends Model
{    
	protected $guarded = [
        'id',
    ];

    public function district()
    {
        return $this->belongsTo(District::class,'district_id','id');
    }

}