<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportCropsPreviousMonth extends Model
{    
    protected $table = 'report_crops_previous_month';

	protected $guarded = [
        'id',
    ];

}