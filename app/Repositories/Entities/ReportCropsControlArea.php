<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportCropsControlArea extends Model
{    
    protected $table = 'report_crops_control_area';

	protected $guarded = [
        'id',
    ];

}