<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Village extends Model
{    
	protected $guarded = [
        'id',
    ];

    public function subDistrict()
    {
        return $this->belongsTo(SubDistrict::class,'sub_district_id','id');
    }

}