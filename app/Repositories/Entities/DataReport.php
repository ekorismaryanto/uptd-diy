<?php

namespace App\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataReport extends Model
{    
	protected $guarded = [
        'id',
    ];

    public function optData()
    {
        return $this->hasOne(DataReportOpt::class, 'report_data_id');
    }

    public function optDatas()
    {
        return $this->hasMany(DataReportOpt::class, 'report_data_id');
    }

    public function controlAreas()
    {
        return $this->hasMany(DataReportControlArea::class, 'report_data_id');
    }

    public function subDistrict()
    {
        return $this->belongsTo(SubDistrict::class,'sub_district_id','id');
    }

    public function commodity()
    {
        return $this->belongsTo(Commodity::class,'commodity_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

}