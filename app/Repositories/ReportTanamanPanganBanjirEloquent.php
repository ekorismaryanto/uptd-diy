<?php

namespace App\Repositories;

use App\Repositories\Entities\DataReport;
use App\Repositories\Entities\ReportCropFlood;
use App\Repositories\Entities\User;

class ReportTanamanPanganBanjirEloquent {

    public function find($id)
    {
        return ReportCropFlood::find($id);
    }

    public function fetch($param = [], $user = '', $paginate = false)
    {
        $query = ReportCropFlood::query()->latest();

        $cekRole = logged_in_user()->roles()->where('name', 'karyawan')->first();

        if ($cekRole) {
            $query->where('user_id', logged_in_user()->id);
        }

        if (isset($param['date'])) {
            $query->whereDate('date', 'like', date('Y-m', strtotime($param['date'])).'%');
        }

        if (isset($param['district'])) {
            $query->whereHas('subDistrict', function($q) use($param){
                $q->where('district_id', $param['district']);
            });
        }

        if (isset($param['q'])) {
            $query->whereHas('commodity', function($q) use($param){
                $q->where('name', 'like','%'.$param['q'].'%');
            });
        }

        if (isset($param['sub_district'])) {
            $query->whereHas('subDistrict', function($q) use($param){
                $q->where('id', $param['sub_district']);
            });
        }

        if (isset($paginate)) {
            return $query->paginate(30);    
        }

        return $query->get();

    }


    public function store($data)
    {
        foreach ($data['report_data'] as $key => $value) {

            $report = DataReport::create(
                [
                    "description" => $data['description'],
                    "user_id" => logged_in_user()->id,
                    "date" => date('Y-m-d', strtotime($data['date'])),
                    "sub_district_id" => $value['sub_district_id'],
                    "type_commodity" => 3,
                    "commodity_id" => $value['commodity_id'],
                    "itensity_attack_easy" => $value['itensity_attack_easy'],
                    "itensity_attack_hard" => $value['itensity_attack_hard'],
                    "control_area_apbn" => $value['itensity_attack_easy'],
                    "control_area_apbd_1" => $value['control_area_apbd_1'],
                    "control_area_apbd_2" => $value['control_area_apbd_2'],
                    "control_area_apbd_public" => $value['control_area_apbd_public'],
                    "price_average" => $value['price_average'],
                    "planting_area" => $value['planting_area']
                ]
            );
    
            if ($report) {
                
                $opt = [];
                foreach ($value['opts'] as $key => $row) {
                    $opt[] = [
                        'opt_id' => $row
                    ];
                }

                if (!empty($opt)) {
                    $report->optDatas()->createMany($opt);
                }

                $area = [];
                foreach ($value['areas'] as $key => $row) {
                    $area[] = [
                        'control_area' => $row
                    ];
                }
    
                if (!empty($area)) {
                    $report->controlAreas()->createMany($area);
                }
            }
        }

        return $report;
    }

    public function update($data, $id)
    {
        $report = DataReport::find($id);

        $report->update($data['report']);

        if ($report) {
            $report->optDatas()->delete();
            $report->optDatas()->createMany($data['report_opt']);
        }

        return $report;
    }
}