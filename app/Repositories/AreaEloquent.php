<?php

namespace App\Repositories;

use App\Repositories\Entities\District;
use App\Repositories\Entities\SubDistrict;
use App\Repositories\Entities\Village;
use Illuminate\Support\Facades\DB;

class AreaEloquent {
    
    public function seedArea()
    {
        $dbKabupaten = DB::table('kabupaten')->where('id_prov', 34)->get();

        foreach ($dbKabupaten as $key => $kab) {

           $district = District::create([
                'name' => $kab->nama,
           ]);

            $dbKecamatan = DB::table('kecamatan')->where('id_kab', $kab->id_kab)->get();

            foreach ($dbKecamatan as $key => $kec) {
                $sub = SubDistrict::create([
                    'district_id' => $district->id,
                    'name' => $kec->nama,
                ]);

                $dbDesa = DB::table('kelurahan')->where('id_kec', $kec->id_kec)->get();

                foreach ($dbDesa as $key => $desa) {
                    Village::create([
                        'sub_district_id' => $sub->id,
                        'name' => $desa->nama
                    ]);
                }
            }
        }
    }
}