<?php

namespace App\Repositories;

use App\Repositories\Entities\User;

class UserEloquent {

    public function fetch(array $request)
    {
        $query = User::query();

        if (isset($request['q'])) {
            $query->where('name', 'like', '%'.$request['q'].'%');
        }

        return $query->latest()->paginate(20);
    }

    public function find($id)
    {
        return User::find($id);
    }

    public function store(array $request)
    {
        $user = User::create($request['user']); 
        if ($user) {
            if (!empty($request['roles'])) {
                $user->assignRole($request['roles']);
            }
        }

        return $user;
    }


    public function update(int $id, array $data)
    {

        $user = User::findOrFail($id);

        if (isset($data['user'])) {
            $user->update($data['user']);
        }

        if (!empty($data['roles'])) {
            $user->syncRoles($data['roles']);
        }
        return $user;
    }


}