<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Cara instalasi project

untuk menginstall website ini caranya adalah

- clone data ke server, pastikan menggunakan php v.7.2
- copy file .env-example menjadi .env
- setting environment database (database bisa memakain command `**php artisan migrate**` atau mengimport dari file db yang ada di folder db)
- setelah terseting environment nya, jalankan perintah `**composer install**`.
- tunggu sampe prosees instal selesai, pastikan connect internet
- setelah semua aman berhasil di install, lakukan perintah ini
- `**php artisan key:generate**`
- `**php artisan storage:link**`
- composer du

Selesai

untuk dokumentasi bisa dilihat disini
https://laravel.com/docs/7.x/installation
